#pragma once

#include <gmock/gmock.h>

#include "urf/middleware/sockets/ISocket.hpp"

namespace urf {
namespace middleware {
namespace sockets {

class SocketMock : public ISocket {
 public:
    ~SocketMock() override = default;

    MOCK_METHOD(bool, publish_impl, (const std::string&, const messages::Message&));

    MOCK_METHOD(bool, open, (), (override));
    MOCK_METHOD(bool, close, (), (override));
    MOCK_METHOD(bool, isOpen, (), (override));

    MOCK_METHOD(std::optional<messages::Message>, pull, (), (override));
    MOCK_METHOD(std::optional<messages::Message>,
                pull,
                (const std::chrono::milliseconds&),
                (override));
    MOCK_METHOD(bool, push, (const messages::Message&, bool), (override));
    MOCK_METHOD(bool, onPush, (const std::function<void(const messages::Message&)>&), (override));

    MOCK_METHOD(std::vector<messages::Message>, request, (const messages::Message&), (override));
    MOCK_METHOD(std::vector<messages::Message>,
                request,
                (const messages::Message&, const std::chrono::milliseconds&),
                (override));

    MOCK_METHOD(std::future<std::vector<messages::Message>>,
                requestAsync,
                (const messages::Message&),
                (override));
    MOCK_METHOD(std::future<std::vector<messages::Message>>,
                requestAsync,
                (const messages::Message&, const std::chrono::milliseconds&),
                (override));

    MOCK_METHOD(std::optional<messages::Message>, receiveRequest, (), (override));
    MOCK_METHOD(std::optional<messages::Message>,
                receiveRequest,
                (const std::chrono::milliseconds&),
                (override));
    MOCK_METHOD(bool, respond, (const messages::Message&), (override));

    MOCK_METHOD(bool,
                subscribe,
                (const std::string&, const std::chrono::milliseconds&),
                (override));
    MOCK_METHOD(uint32_t, subscriptionsCount, (const std::string&), (override));
    MOCK_METHOD(bool, unsubscribe, (const std::string&), (override));

    MOCK_METHOD(bool, keepUpdateHistory, (bool), (override));
    MOCK_METHOD(std::optional<messages::Message>, receiveUpdate, (const std::string&), (override));
    MOCK_METHOD(std::optional<messages::Message>,
                receiveUpdate,
                (const std::string&, const std::chrono::milliseconds&),
                (override));
    MOCK_METHOD(bool,
                onUpdate,
                (const std::string&,
                 const std::function<void(const std::string& topic, const messages::Message&)>&),
                (override));

    virtual bool publish(const std::string& topic,
                         const messages::Message& msg,
                         bool requiresAck = false) override {
        return publish_impl(topic, msg);
    }
    MOCK_METHOD(std::vector<std::string>, availablePartnerTopics, (), (override));

    MOCK_METHOD(bool, addTopic, (const std::string&), (override));
    MOCK_METHOD(bool, removeTopic, (const std::string&), (override));

    MOCK_METHOD(bool, automaticallyDeserializeBody, (bool), (override));
};

} // namespace sockets
} // namespace middleware
} // namespace urf