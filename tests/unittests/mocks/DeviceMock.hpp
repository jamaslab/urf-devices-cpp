#pragma once

#include <gmock/gmock.h>

#include "urf/devices/Device.hpp"

namespace urf {
namespace devices {

class DeviceMock : public common::components::IComponent {
 public:
    ~DeviceMock() override = default;

    MOCK_METHOD(bool, resetFault, (), (noexcept));
    MOCK_METHOD(bool, switchOn, (), (noexcept));
    MOCK_METHOD(bool, shutdown, (), (noexcept));
    MOCK_METHOD(bool, disable, (), (noexcept));
    MOCK_METHOD(bool, enable, (), (noexcept));
    MOCK_METHOD(bool, reconfigure, (), (noexcept));
    MOCK_METHOD(bool, fault, (), (noexcept));
    MOCK_CONST_METHOD0(currentState, ComponentStates());
    MOCK_CONST_METHOD0(currentTransition, ComponentStateTransitions());

    MOCK_CONST_METHOD0(componentName, std::string());
    MOCK_CONST_METHOD0(componentClass, std::vector<std::string>());

    MOCK_METHOD(common::properties::PropertyNode, settings, (), (const));
};

} // namespace devices
} // namespace urf