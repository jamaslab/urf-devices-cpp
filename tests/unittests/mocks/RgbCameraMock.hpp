#pragma once

#include <gmock/gmock.h>

#include "urf/devices/sensors/cameras/rgb/RgbCamera.hpp"

namespace urf {
namespace devices {
namespace sensors {
namespace cameras {

class RgbCameraMock : public RgbCamera {
 public:
    RgbCameraMock();
    ~RgbCameraMock() override = default;

    MOCK_METHOD0(getFrame, algorithms::compression::VideoFrame());

    MOCK_METHOD0(resetFaultImpl, bool());
    MOCK_METHOD0(switchOnImpl, bool());
    MOCK_METHOD0(shutdownImpl, bool());
    MOCK_METHOD0(disableImpl, bool());
    MOCK_METHOD0(enableImpl, bool());
    MOCK_METHOD0(faultImpl, bool());
};

} // namespace cameras
} // namespace sensors
} // namespace devices
} // namespace urf
