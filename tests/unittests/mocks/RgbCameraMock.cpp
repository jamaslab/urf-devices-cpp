#include "./RgbCameraMock.hpp"

namespace urf {
namespace devices {
namespace sensors {
namespace cameras {

RgbCameraMock::RgbCameraMock()
    : Device("RgbCameraMock", {"RgbCamera"})
    , RgbCamera("RgbCameraMock") { }

} // namespace cameras
} // namespace sensors
} // namespace devices
} // namespace urf