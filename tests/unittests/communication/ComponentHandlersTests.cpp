#include <gtest/gtest.h>

#include <urf/common/logger/Logger.hpp>

#include "urf/devices/accesscontrol/AccessControl.hpp"
#include "urf/devices/communication/handlers/ComponentHandlers.hpp"
#include "mocks/DeviceMock.hpp"

using namespace urf::devices;
using namespace urf::middleware;
using namespace urf::common::properties;

namespace {
PropertyNode buildSettings() {
    PropertyNode node;

    auto matrix = std::make_shared<ObservableProperty<Eigen::MatrixXf>>();
    auto setting = std::make_shared<ObservableSetting<float>>();
    auto ranged = std::make_shared<ObservableSettingRanged<uint32_t>>();

    auto subnode = std::make_shared<PropertyNode>();
    auto subsetting = std::make_shared<ObservableSetting<float>>();

    subnode->insert("subsetting", subsetting);

    node.insert("matrix", matrix);
    node.insert("setting", setting);
    node.insert("ranged", ranged);
    node.insert("node", subnode);
    return node;
}

}

TEST(AcquireSlotHandlerTest, missingUsername) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto handler = communication::AcquireSlotHandler(accessControl);

    auto request = messages::Message();
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("Missing user in acquire request", response["error"]);
}

TEST(AcquireSlotHandlerTest, missingRole) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto handler = communication::AcquireSlotHandler(accessControl);

    auto request = messages::Message();
    request["user"] = "user";
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("Missing role in acquire request", response["error"]);
}

TEST(AcquireSlotHandlerTests, invalidRole) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto handler = communication::AcquireSlotHandler(accessControl);

    auto request = messages::Message();
    request["user"] = "user";
    request["role"] = "invalid";
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("Invalid role invalid", response["error"]);
}

TEST(AcquireSlotHandlerTests, rejectedAcquireByAccessControl) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(0);
    auto handler = communication::AcquireSlotHandler(accessControl);

    auto request = messages::Message();
    request["user"] = "user";
    request["role"] = "user";
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("Could not acquire a slot", response["error"]);
}

TEST(AcquireSlotHandlerTests, correctlyAcquire) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto handler = communication::AcquireSlotHandler(accessControl);

    auto request = messages::Message();
    request["user"] = "user";
    request["role"] = "user";
    auto response = handler(request);

    EXPECT_EQ(true, response["retval"]);
    EXPECT_NE(0, response["id"]);
}

TEST(ReleaseSlotHandlerTests, correctlyRelease) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);
    
    auto handler = communication::ReleaseSlotHandler(accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    auto response = handler(request);

    EXPECT_EQ(true, response["retval"]);
}

TEST(ReleaseSlotHandlerTests, notAcquiredRelease) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto handler = communication::ReleaseSlotHandler(accessControl);
    auto request = messages::Message();
    request.header().writerId(0);
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("Could not release slot", response["error"]);
}

TEST(ResetFaultHandlerTests, noWriteAccess) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, resetFault()).Times(0);

    auto handler = communication::ResetFaultHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(0);
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("No write access", response["error"]);
}

TEST(ResetFaultHandlerTests, resetFaultFails) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, resetFault()).WillOnce(testing::Return(false));

    auto handler = communication::ResetFaultHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("Device could not be reset", response["error"]);
}

TEST(ResetFaultHandlerTests, correctlyResetFault) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, resetFault()).WillOnce(testing::Return(true));

    auto handler = communication::ResetFaultHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    auto response = handler(request);

    EXPECT_EQ(true, response["retval"]);
}

TEST(SwitchOnHandlerTests, noWriteAccess) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, switchOn()).Times(0);

    auto handler = communication::SwitchOnHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(0);
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("No write access", response["error"]);
}

TEST(SwitchOnHandlerTests, switchOnFails) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, switchOn()).WillOnce(testing::Return(false));

    auto handler = communication::SwitchOnHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("Device could not be switched on", response["error"]);
}

TEST(SwitchOnHandlerTests, correctlySwitchOn) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, switchOn()).WillOnce(testing::Return(true));

    auto handler = communication::SwitchOnHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    auto response = handler(request);

    EXPECT_EQ(true, response["retval"]);
}

TEST(ShutdownHandlerTests, noWriteAccess) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, shutdown()).Times(0);

    auto handler = communication::ShutdownHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(0);
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("No write access", response["error"]);
}

TEST(ShutdownHandlerTests, shutdownFails) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, shutdown()).WillOnce(testing::Return(false));

    auto handler = communication::ShutdownHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("Device could not be shutdown", response["error"]);
}

TEST(ShutdownHandlerTests, correctlyShutdown) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, shutdown()).WillOnce(testing::Return(true));

    auto handler = communication::ShutdownHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    auto response = handler(request);

    EXPECT_EQ(true, response["retval"]);
}

TEST(DisableHandlerTests, noWriteAccess) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, disable()).Times(0);

    auto handler = communication::DisableHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(0);
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("No write access", response["error"]);
}

TEST(DisableHandlerTests, disabledFails) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, disable()).WillOnce(testing::Return(false));

    auto handler = communication::DisableHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("Device could not be disabled", response["error"]);
}

TEST(DisableHandlerTests, correctlyDisabled) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, disable()).WillOnce(testing::Return(true));

    auto handler = communication::DisableHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    auto response = handler(request);

    EXPECT_EQ(true, response["retval"]);
}

TEST(EnableHandlerTests, noWriteAccess) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, enable()).Times(0);

    auto handler = communication::EnableHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(0);
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("No write access", response["error"]);
}

TEST(EnabledHandlerTests, enabledFails) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, enable()).WillOnce(testing::Return(false));

    auto handler = communication::EnableHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("Device could not be enabled", response["error"]);
}

TEST(EnableHandlerTests, correctlyEnabled) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, enable()).WillOnce(testing::Return(true));

    auto handler = communication::EnableHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    auto response = handler(request);

    EXPECT_EQ(true, response["retval"]);
}

TEST(ReconfigureHandlerTests, noWriteAccess) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, reconfigure()).Times(0);

    auto handler = communication::ReconfigureHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(0);
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("No write access", response["error"]);
}

TEST(ReconfigureHandlerTests, reconfigureFails) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, reconfigure()).WillOnce(testing::Return(false));

    auto handler = communication::ReconfigureHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("Device could not be reconfigured", response["error"]);
}

TEST(ReconfigureHandlerTests, correctlyReconfigured) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, reconfigure()).WillOnce(testing::Return(true));

    auto handler = communication::ReconfigureHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    auto response = handler(request);

    EXPECT_EQ(true, response["retval"]);
}

TEST(FaultHandlerTests, noWriteAccess) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, fault()).Times(0);

    auto handler = communication::FaultHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(0);
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("No write access", response["error"]);
}

TEST(FaultHandlerTests, faultFails) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, fault()).WillOnce(testing::Return(false));

    auto handler = communication::FaultHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("Device could not be faulted", response["error"]);
}

TEST(FaultHandlerTests, correctlyFaulted) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);
    auto component = std::make_shared<DeviceMock>();

    EXPECT_CALL(*component, fault()).WillOnce(testing::Return(true));

    auto handler = communication::FaultHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    auto response = handler(request);

    EXPECT_EQ(true, response["retval"]);
}

TEST(GetHandlerTests, noReadAccess) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto component = std::make_shared<DeviceMock>();

    auto handler = communication::GetHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(0);
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("No read access", response["error"]);
}

TEST(GetHandlerTests, missingNodeName) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);
    auto component = std::make_shared<DeviceMock>();

    auto handler = communication::GetHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("Missing node name", response["error"]);
}

TEST(GetHandlerTests, returnAccessControlInfoOnServerRequest) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);
    auto component = std::make_shared<DeviceMock>();
    auto handler = communication::GetHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    request["node"] = "server";

    auto response = handler(request);

    EXPECT_EQ(true, response["retval"]);
    ASSERT_NO_THROW(response["data"]["access_control"]["user"]);
    EXPECT_EQ(id.value(), response["data"]["access_control"]["user"]);
}

TEST(GetHandlerTests, unknownNodeName) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);
    auto settings= buildSettings();

    auto component = std::make_shared<DeviceMock>();
    EXPECT_CALL(*component, settings()).WillOnce(testing::Return(settings));

    auto handler = communication::GetHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    request["node"] = "unknown";

    auto response = handler(request);

    EXPECT_EQ(false, response["retval"]);
    EXPECT_EQ("Invalid node name: unknown", response["error"]);
}

TEST(GetHandlerTests, correctlyGetAllSettings) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);
    auto settings= buildSettings();

    auto component = std::make_shared<DeviceMock>();
    EXPECT_CALL(*component, settings()).WillOnce(testing::Return(settings));

    auto handler = communication::GetHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    request["node"] = "/";

    auto response = handler(request);

    EXPECT_EQ(true, response["retval"]);
}

TEST(GetHandlerTests, correctlyGetSubsettings) {
    auto accessControl = std::make_shared<accesscontrol::AccessControl>(5);
    auto id = accessControl->acquireSlot("user", accesscontrol::Roles::USER);

    auto settings= buildSettings();
    auto component = std::make_shared<DeviceMock>();
    EXPECT_CALL(*component, settings()).Times(2).WillRepeatedly(testing::Return(settings));

    auto handler = communication::GetHandler(component, accessControl);
    auto request = messages::Message();
    request.header().writerId(id.value());
    request["node"] = "node/subsetting";

    auto response = handler(request);

    EXPECT_EQ(true, response["retval"]);
}
