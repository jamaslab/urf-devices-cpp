#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <urf/middleware/sockets/Client.hpp>
#include <urf/middleware/sockets/Server.hpp>

#include "urf/devices/communication/DeviceServer.hpp"

#include "./mocks/DeviceMock.hpp"

using namespace urf::devices::communication;
using namespace urf::common::properties;

class DeviceServerShould : public ::testing::Test {
 protected:
    DeviceServerShould() = default;
    virtual ~DeviceServerShould() = default;

    void SetUp() override {
        _state = std::make_shared<ObservableProperty<urf::common::components::StatePair>>();

        auto deviceSettingsNode = std::make_shared<PropertyNode>();
        deviceSettingsNode->insert("state", _state);
        _settings.insert("component", deviceSettingsNode);

        _device = std::make_shared<urf::devices::DeviceMock>();
        EXPECT_CALL(*_device, settings()).WillRepeatedly(testing::Return(_settings));

        _server =
            std::make_shared<urf::middleware::sockets::Server>("test_server", "proc://test_server");
        _sut = std::make_unique<DeviceServer>(_server, _device);
    }

    void TearDown() override {
        _sut.reset();
        _server.reset();
        _device.reset();
    }

    PropertyNode _settings;
    std::shared_ptr<ObservableProperty<urf::common::components::StatePair>> _state;
    std::shared_ptr<urf::devices::DeviceMock> _device;
    std::shared_ptr<urf::middleware::sockets::Server> _server;
    std::unique_ptr<urf::middleware::sockets::Client> _client;
    std::unique_ptr<DeviceServer> _sut;
};

TEST_F(DeviceServerShould, correctlyOpenCloseSequence) {
    ASSERT_FALSE(_sut->close());
    ASSERT_TRUE(_sut->open());
    ASSERT_FALSE(_sut->open());
    ASSERT_TRUE(_sut->close());
    ASSERT_FALSE(_sut->close());

    ASSERT_FALSE(_sut->close());
    ASSERT_TRUE(_sut->open());
    ASSERT_FALSE(_sut->open());
    ASSERT_TRUE(_sut->close());
    ASSERT_FALSE(_sut->close());
}

TEST_F(DeviceServerShould, correctlyReceiveStateUpdates) {
    ASSERT_TRUE(_sut->open());

    _client = std::make_unique<urf::middleware::sockets::Client>("proc://test_server");
    ASSERT_TRUE(_client->open());
    ASSERT_TRUE(_client->subscribe("component"));

    _state->setValue({urf::common::components::ComponentStates::Enabled,
                      urf::common::components::ComponentStateTransitions::NoTransition});

    auto update = _client->receiveUpdate("component", std::chrono::milliseconds(100));

    ASSERT_TRUE(update);

    ASSERT_EQ(urf::common::components::ComponentStates::Enabled,
              update.value()["state"].get<urf::common::components::StatePair>().first);

    ASSERT_TRUE(_client->close());
    ASSERT_TRUE(_sut->close());
}

TEST_F(DeviceServerShould, missingCmdFieldInRequest) {
    ASSERT_TRUE(_sut->open());

    _client = std::make_unique<urf::middleware::sockets::Client>("proc://test_server");
    ASSERT_TRUE(_client->open());
    
    urf::middleware::messages::Message msg;
    msg["cmds"] = "test";

    auto response = _client->request(msg, std::chrono::milliseconds(100));

    ASSERT_EQ(1, response.size());
    ASSERT_EQ(false, response[0]["retval"]);
    ASSERT_EQ("Missing cmd field in request", response[0]["error"]);

    ASSERT_TRUE(_client->close());
    ASSERT_TRUE(_sut->close());
}

TEST_F(DeviceServerShould, invalidCmdFieldInRequest) {
    ASSERT_TRUE(_sut->open());

    _client = std::make_unique<urf::middleware::sockets::Client>("proc://test_server");
    ASSERT_TRUE(_client->open());
    
    urf::middleware::messages::Message msg;
    msg["cmd"] = "test";

    auto response = _client->request(msg, std::chrono::milliseconds(100));

    ASSERT_EQ(1, response.size());
    ASSERT_EQ(false, response[0]["retval"]);
    ASSERT_EQ("Invalid request", response[0]["error"]);

    ASSERT_TRUE(_client->close());
    ASSERT_TRUE(_sut->close());
}

TEST_F(DeviceServerShould, correctlyHandleCommonComponentRequests) {
    ASSERT_TRUE(_sut->open());

    _client = std::make_unique<urf::middleware::sockets::Client>("proc://test_server");
    ASSERT_TRUE(_client->open());
    
    urf::middleware::messages::Message msg;
    msg["cmd"] = "acquire";
    msg["role"] = "expert";
    msg["user"] = "test";

    auto response = _client->request(msg, std::chrono::milliseconds(100));

    ASSERT_EQ(1, response.size());
    ASSERT_EQ(true, response[0]["retval"]);
    msg.header().writerId(response[0]["id"]);

    EXPECT_CALL(*_device, resetFault()).Times(1).WillOnce(testing::Return(true));
    msg["cmd"] = "reset";
    response = _client->request(msg, std::chrono::milliseconds(100));
    ASSERT_EQ(1, response.size());
    ASSERT_EQ(true, response[0]["retval"]);

    EXPECT_CALL(*_device, switchOn()).Times(1).WillOnce(testing::Return(true));
    msg["cmd"] = "switchon";
    response = _client->request(msg, std::chrono::milliseconds(100));
    ASSERT_EQ(1, response.size());
    ASSERT_EQ(true, response[0]["retval"]);

    EXPECT_CALL(*_device, shutdown()).Times(1).WillOnce(testing::Return(true));
    msg["cmd"] = "shutdown";
    response = _client->request(msg, std::chrono::milliseconds(100));
    ASSERT_EQ(1, response.size());
    ASSERT_EQ(true, response[0]["retval"]);

    EXPECT_CALL(*_device, disable()).Times(1).WillOnce(testing::Return(true));
    msg["cmd"] = "disable";
    response = _client->request(msg, std::chrono::milliseconds(100));
    ASSERT_EQ(1, response.size());
    ASSERT_EQ(true, response[0]["retval"]);

    EXPECT_CALL(*_device, enable()).Times(1).WillOnce(testing::Return(true));
    msg["cmd"] = "enable";
    response = _client->request(msg, std::chrono::milliseconds(100));
    ASSERT_EQ(1, response.size());
    ASSERT_EQ(true, response[0]["retval"]);

    EXPECT_CALL(*_device, reconfigure()).Times(1).WillOnce(testing::Return(true));
    msg["cmd"] = "reconfigure";
    response = _client->request(msg, std::chrono::milliseconds(100));
    ASSERT_EQ(1, response.size());
    ASSERT_EQ(true, response[0]["retval"]);

    EXPECT_CALL(*_device, fault()).Times(1).WillOnce(testing::Return(true));
    msg["cmd"] = "fault";
    response = _client->request(msg, std::chrono::milliseconds(100));
    ASSERT_EQ(1, response.size());
    ASSERT_EQ(true, response[0]["retval"]);

    msg["cmd"] = "release";
    response = _client->request(msg, std::chrono::milliseconds(100));
    ASSERT_EQ(1, response.size());
    ASSERT_EQ(true, response[0]["retval"]);

    ASSERT_TRUE(_client->close());
    ASSERT_TRUE(_sut->close());
}

TEST_F(DeviceServerShould, dontWaitForResponse) {
    ASSERT_TRUE(_sut->open());

    _client = std::make_unique<urf::middleware::sockets::Client>("proc://test_server");
    ASSERT_TRUE(_client->open());
    
    urf::middleware::messages::Message msg;
    msg["cmd"] = "acquire";
    msg["role"] = "user";
    msg["user"] = "test";

    auto response = _client->request(msg, std::chrono::milliseconds(0));
    ASSERT_EQ(0, response.size());

    msg["cmd"] = "acquire";
    msg["role"] = "expert";
    msg["user"] = "test2";
    response = _client->request(msg, std::chrono::milliseconds(100));

    ASSERT_EQ(1, response.size());
    ASSERT_EQ(true, response[0]["retval"]);
    msg.header().writerId(response[0]["id"]);

    EXPECT_CALL(*_device, resetFault()).Times(1).WillOnce(testing::Return(true));
    msg["cmd"] = "reset";
    response = _client->request(msg, std::chrono::milliseconds(100));
    ASSERT_EQ(1, response.size());
    ASSERT_EQ(true, response[0]["retval"]);

    ASSERT_TRUE(_client->close());
    ASSERT_TRUE(_sut->close());
}

