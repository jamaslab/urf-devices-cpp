#include <gtest/gtest.h>

#include "urf/devices/communication/handlers/RgbCameraHandlers.hpp"
#include "urf/devices/accesscontrol/AccessControl.hpp"

#include "mocks/RgbCameraMock.hpp"
#include "mocks/SocketMock.hpp"

using namespace urf::devices::accesscontrol;
using namespace urf::devices::communication;
using namespace urf::devices::sensors::cameras;
using namespace urf::middleware::sockets;
using namespace urf::middleware::messages;

class RgbCameraHandlersShould : public ::testing::Test {
 protected:
    std::shared_ptr<SocketMock> socket;
    std::shared_ptr<RgbCameraMock> device;
    std::shared_ptr<AccessControl> accessControl;

    void SetUp() override {
        socket = std::make_shared<SocketMock>();
        device = std::make_shared<RgbCameraMock>();
        accessControl = std::make_shared<AccessControl>(5);
    }

    void TearDown() override {
        socket.reset();
        device.reset();
        accessControl.reset();
    }
};

TEST_F(RgbCameraHandlersShould, startStreamNoReadAccess) {
    StartRgbStreamHandler handler(socket, device, accessControl);

    Message request;
    request.header().writerId(1);
    auto response = handler(request);

    EXPECT_FALSE(response["retval"].get<bool>());
    EXPECT_EQ(response["error"].get<std::string>(), "No read access");
}

TEST_F(RgbCameraHandlersShould, startStreamNotEnabled) {
    StartRgbStreamHandler handler(socket, device, accessControl);
    auto id = accessControl->acquireSlot("test", Roles::EXPERT);

    Message request;
    request.header().writerId(id.value());
    auto response = handler(request);

    EXPECT_FALSE(response["retval"].get<bool>());
    EXPECT_EQ(response["error"].get<std::string>(), "Camera is not in enabled state");
}