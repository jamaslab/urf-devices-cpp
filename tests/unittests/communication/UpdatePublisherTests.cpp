#include <gtest/gtest.h>

#include <iostream>

#include "mocks/SocketMock.hpp"

#include "urf/devices/communication/handlers/UpdateAllHandler.hpp"
#include "urf/devices/communication/handlers/UpdateAnyHandler.hpp"

using namespace urf::devices::communication;

TEST(UpdateAnyHandlerTests, updateImmediatelyOnValueChange) {
    auto socket = std::make_shared<urf::middleware::sockets::SocketMock>();
    auto property = std::make_shared<urf::common::properties::ObservableProperty<int>>();
    auto publisher = UpdateAnyHandler(socket, "test", property);

    std::mutex mtx;
    std::condition_variable cv;
    urf::middleware::messages::Message msg;
    bool called = false;
    EXPECT_CALL(*socket, publish_impl("test", testing::_))
        .WillRepeatedly(testing::Invoke([&mtx, &cv, &called, &msg](auto, auto m) {
            std::lock_guard<std::mutex> lock(mtx);
            called = true;
            cv.notify_one();
            msg = m;
            return true;
        }));

    property->setValue(1);

    std::unique_lock<std::mutex> lock(mtx);
    ASSERT_TRUE(cv.wait_for(lock, std::chrono::seconds(1), [&called]() { return called; }));
    ASSERT_EQ(1, msg["test"]);
}

TEST(UpdateAnyHandlerTests, updateImmediatelyPropertyNode) {
    auto socket = std::make_shared<urf::middleware::sockets::SocketMock>();
    auto node = std::make_shared<urf::common::properties::PropertyNode>();
    auto property1 = std::make_shared<urf::common::properties::ObservableProperty<int>>();
    auto property2 = std::make_shared<urf::common::properties::ObservableProperty<int>>();

    node->insert("property1", property1);
    node->insert("property2", property2);

    auto publisher = UpdateAnyHandler(socket, "test", node);

    std::mutex mtx;
    std::condition_variable cv;
    std::vector<urf::middleware::messages::Message> msgs;
    int called = 0;
    EXPECT_CALL(*socket, publish_impl("test", testing::_))
        .WillRepeatedly(testing::Invoke([&mtx, &cv, &called, &msgs](auto, auto m) {
            std::lock_guard<std::mutex> lock(mtx);
            called++;
            cv.notify_one();
            msgs.push_back(m);
            return true;
        }));

    property1->setValue(1);
    property2->setValue(2);

    std::unique_lock<std::mutex> lock(mtx);
    ASSERT_TRUE(cv.wait_for(lock, std::chrono::seconds(1), [&called]() { return called == 2; }));

    ASSERT_EQ(1, msgs[0]["property1"]);
    ASSERT_EQ(2, msgs[1]["property2"]);
}

TEST(UpdateAnyHandlerTests, updateDeferredPropertyNode) {
    auto socket = std::make_shared<urf::middleware::sockets::SocketMock>();
    auto node = std::make_shared<urf::common::properties::PropertyNode>();
    auto property1 = std::make_shared<urf::common::properties::ObservableProperty<int>>();
    auto property2 = std::make_shared<urf::common::properties::ObservableProperty<int>>();

    node->insert("property1", property1);
    node->insert("property2", property2);

    auto publisher = UpdateAnyHandler(socket, "test", node, std::chrono::milliseconds(100));

    std::mutex mtx;
    std::condition_variable cv;
    urf::middleware::messages::Message msg;
    bool called = false;
    EXPECT_CALL(*socket, publish_impl("test", testing::_))
        .WillRepeatedly(testing::Invoke([&mtx, &cv, &called, &msg](auto, auto m) {
            std::lock_guard<std::mutex> lock(mtx);
            called = true;
            cv.notify_one();
            msg = m;
            return true;
        }));

    property1->setValue(1);
    property2->setValue(2);

    std::unique_lock<std::mutex> lock(mtx);
    ASSERT_TRUE(cv.wait_for(lock, std::chrono::seconds(1), [&called]() { return called; }));

    ASSERT_EQ(1, msg["property1"]);
    ASSERT_EQ(2, msg["property2"]);
}

TEST(UpdateAllHandler, updatePropertyNode) {
    auto socket = std::make_shared<urf::middleware::sockets::SocketMock>();
    auto node = std::make_shared<urf::common::properties::PropertyNode>();
    auto property1 = std::make_shared<urf::common::properties::ObservableProperty<int>>();
    auto property2 = std::make_shared<urf::common::properties::ObservableProperty<int>>();

    node->insert("property1", property1);
    node->insert("property2", property2);

    auto publisher = UpdateAllHandler(socket, "test", node);

    std::mutex mtx;
    std::condition_variable cv;
    urf::middleware::messages::Message msg;
    int called = 0;
    EXPECT_CALL(*socket, publish_impl("test", testing::_))
        .WillRepeatedly(testing::Invoke([&mtx, &cv, &called, &msg](auto, auto m) {
            std::lock_guard<std::mutex> lock(mtx);
            called++;
            cv.notify_one();
            msg = m;
            return true;
        }));

    property1->setValue(1);
    std::unique_lock<std::mutex> lock(mtx);
    ASSERT_FALSE(
        cv.wait_for(lock, std::chrono::milliseconds(100), [&called]() { return called == 1; }));
    property2->setValue(2);
    ASSERT_TRUE(cv.wait_for(lock, std::chrono::seconds(1), [&called]() { return called == 1; }));
    ASSERT_EQ(1, msg["property1"]);
    ASSERT_EQ(2, msg["property2"]);
}

TEST(UpdateAllHandler, updateSingleProperty) {
    auto socket = std::make_shared<urf::middleware::sockets::SocketMock>();
    auto property = std::make_shared<urf::common::properties::ObservableProperty<int>>();
    auto publisher = UpdateAllHandler(socket, "test", property);

    std::mutex mtx;
    std::condition_variable cv;
    urf::middleware::messages::Message msg;
    bool called = false;
    EXPECT_CALL(*socket, publish_impl("test", testing::_))
        .WillRepeatedly(testing::Invoke([&mtx, &cv, &called, &msg](auto, auto m) {
            std::lock_guard<std::mutex> lock(mtx);
            called = true;
            cv.notify_one();
            msg = m;
            return true;
        }));

    property->setValue(1);

    std::unique_lock<std::mutex> lock(mtx);
    ASSERT_TRUE(cv.wait_for(lock, std::chrono::seconds(1), [&called]() { return called; }));
    
    ASSERT_EQ(1, msg["test"]);
}
