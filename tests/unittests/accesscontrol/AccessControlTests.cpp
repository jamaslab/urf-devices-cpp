#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <urf/devices/accesscontrol/AccessControl.hpp>

using namespace urf::devices::accesscontrol;

TEST(AccessControlTests, acquireReleaseSlot) {
    auto accessControl = std::make_unique<AccessControl>(5);
    auto id = accessControl->acquireSlot("user", Roles::USER);
    EXPECT_TRUE(id);
    EXPECT_EQ(true, accessControl->hasReadAccess("user"));
    EXPECT_EQ(true, accessControl->hasWriteAccess("user"));
    EXPECT_EQ(true, accessControl->hasReadAccess(id.value()));
    EXPECT_EQ(true, accessControl->hasWriteAccess(id.value()));

    EXPECT_EQ(false, accessControl->releaseSlot(id.value()+1));
    EXPECT_EQ(true, accessControl->releaseSlot(id.value()));
    EXPECT_EQ(false, accessControl->hasReadAccess("user"));
    EXPECT_EQ(false, accessControl->hasWriteAccess("user"));
    EXPECT_EQ(false, accessControl->hasReadAccess(id.value()));
    EXPECT_EQ(false, accessControl->hasWriteAccess(id.value()));
}

TEST(AccessControlTests, acquireAsViewer) {
    auto accessControl = std::make_unique<AccessControl>(5);
    auto id = accessControl->acquireSlot("user", Roles::VIEWER);
    EXPECT_TRUE(id);
    EXPECT_EQ(true, accessControl->hasReadAccess("user"));
    EXPECT_EQ(false, accessControl->hasWriteAccess("user"));

    EXPECT_EQ(true, accessControl->releaseSlot(id.value()));
    EXPECT_EQ(false, accessControl->hasReadAccess("user"));
    EXPECT_EQ(false, accessControl->hasWriteAccess("user"));
}

TEST(AccessControlTests, loseWriteAccessWhenDowngradingRole) {
    auto accessControl = std::make_unique<AccessControl>(5);
    auto id = accessControl->acquireSlot("user", Roles::USER);
    EXPECT_TRUE(id);
    EXPECT_EQ(true, accessControl->hasReadAccess("user"));
    EXPECT_EQ(true, accessControl->hasWriteAccess("user"));

    accessControl->acquireSlot("user", Roles::VIEWER);
    EXPECT_EQ(true, accessControl->hasReadAccess("user"));
    EXPECT_EQ(false, accessControl->hasWriteAccess("user"));

    EXPECT_EQ(true, accessControl->releaseSlot(id.value()));
    EXPECT_EQ(false, accessControl->hasReadAccess("user"));
    EXPECT_EQ(false, accessControl->hasWriteAccess("user"));
}

TEST(AccessControlTests, gainWriteAccessWhenUpgradingRole) {
    auto accessControl = std::make_unique<AccessControl>(5);
    auto id = accessControl->acquireSlot("user", Roles::VIEWER);
    EXPECT_TRUE(id);
    EXPECT_EQ(true, accessControl->hasReadAccess("user"));
    EXPECT_EQ(false, accessControl->hasWriteAccess("user"));

    accessControl->acquireSlot("user", Roles::USER);
    EXPECT_EQ(true, accessControl->hasReadAccess("user"));
    EXPECT_EQ(true, accessControl->hasWriteAccess("user"));

    EXPECT_EQ(true, accessControl->releaseSlot(id.value()));
    EXPECT_EQ(false, accessControl->hasReadAccess("user"));
    EXPECT_EQ(false, accessControl->hasWriteAccess("user"));
}

TEST(AccessControlTests, getWriteAccessFromOtherUser) {
    auto accessControl = std::make_unique<AccessControl>(5);
    accessControl->acquireSlot("user", Roles::USER);
    EXPECT_EQ(true, accessControl->hasReadAccess("user"));
    EXPECT_EQ(true, accessControl->hasWriteAccess("user"));

    auto id = accessControl->acquireSlot("user2", Roles::EXPERT);
    EXPECT_EQ(true, accessControl->hasReadAccess("user2"));
    EXPECT_EQ(true, accessControl->hasWriteAccess("user2"));
    EXPECT_EQ(true, accessControl->hasReadAccess("user"));
    EXPECT_EQ(false, accessControl->hasWriteAccess("user"));

    accessControl->releaseSlot(id.value());
    EXPECT_EQ(true, accessControl->hasReadAccess("user"));
    EXPECT_EQ(false, accessControl->hasWriteAccess("user"));
    EXPECT_EQ(false, accessControl->hasReadAccess("user2"));
    EXPECT_EQ(false, accessControl->hasWriteAccess("user2"));
}

TEST(AccessControlTests, noMoreSlotsAvailable) {
    auto accessControl = std::make_unique<AccessControl>(1);
    auto id = accessControl->acquireSlot("user", Roles::USER);
    EXPECT_TRUE(id);
    EXPECT_EQ(true, accessControl->hasReadAccess("user"));
    EXPECT_EQ(true, accessControl->hasWriteAccess("user"));

    auto id2 = accessControl->acquireSlot("user2", Roles::USER);
    EXPECT_FALSE(id2);
    EXPECT_EQ(false, accessControl->hasReadAccess("user2"));
    EXPECT_EQ(false, accessControl->hasWriteAccess("user2"));

    EXPECT_EQ(true, accessControl->releaseSlot(id.value()));
    EXPECT_EQ(false, accessControl->hasReadAccess("user"));
    EXPECT_EQ(false, accessControl->hasWriteAccess("user"));
}

TEST(AccessControlTests, releaseSlotNotAcquired) {
    auto accessControl = std::make_unique<AccessControl>(1);
    EXPECT_EQ(false, accessControl->releaseSlot(1));
}

TEST(AccessControlTests, releaseSlotTwice) {
    auto accessControl = std::make_unique<AccessControl>(1);
    auto id = accessControl->acquireSlot("user", Roles::USER);
    EXPECT_TRUE(id);
    EXPECT_EQ(true, accessControl->hasReadAccess("user"));
    EXPECT_EQ(true, accessControl->hasWriteAccess("user"));

    EXPECT_EQ(true, accessControl->releaseSlot(id.value()));
    EXPECT_EQ(false, accessControl->hasReadAccess("user"));
    EXPECT_EQ(false, accessControl->hasWriteAccess("user"));

    EXPECT_EQ(false, accessControl->releaseSlot(id.value()));
}

TEST(AccessControlTests, releaseSlotTwiceWithOtherUser) {
    auto accessControl = std::make_unique<AccessControl>(2);
    auto id = accessControl->acquireSlot("user", Roles::USER);
    EXPECT_TRUE(id);
    EXPECT_EQ(true, accessControl->hasReadAccess("user"));
    EXPECT_EQ(true, accessControl->hasWriteAccess("user"));

    auto id2 = accessControl->acquireSlot("user2", Roles::USER);
    EXPECT_TRUE(id2);
    EXPECT_EQ(true, accessControl->hasReadAccess("user2"));
    EXPECT_EQ(false, accessControl->hasWriteAccess("user2"));

    EXPECT_EQ(true, accessControl->releaseSlot(id.value()));
    EXPECT_EQ(false, accessControl->hasReadAccess("user"));
    EXPECT_EQ(false, accessControl->hasWriteAccess("user"));
    EXPECT_EQ(true, accessControl->hasReadAccess("user2"));
    EXPECT_EQ(false, accessControl->hasWriteAccess("user2"));

    EXPECT_EQ(true, accessControl->releaseSlot(id2.value()));
    EXPECT_EQ(false, accessControl->hasReadAccess("user"));
    EXPECT_EQ(false, accessControl->hasWriteAccess("user"));
    EXPECT_EQ(false, accessControl->hasReadAccess("user2"));
    EXPECT_EQ(false, accessControl->hasWriteAccess("user2"));

    EXPECT_EQ(false, accessControl->releaseSlot(id.value()));
    EXPECT_EQ(false, accessControl->releaseSlot(id2.value()));
}
