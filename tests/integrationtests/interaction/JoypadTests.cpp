#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <urf/devices/interaction/Joypad.hpp>

#include <iostream>
#include <memory>

using urf::devices::interaction::Joypad;

TEST(Joypad, correctlyListAllJoypads) {
    ASSERT_GT(static_cast<int>(Joypad::availableJoypads().size()), 0);
}

TEST(Joypad, invalidJoypadId) {
    Joypad joypad(1);
    ASSERT_FALSE(joypad.switchOn());
}

TEST(Joypad, switchOnShutdownSequence) {
    Joypad j(0);
    ASSERT_TRUE(j.switchOn());
    ASSERT_TRUE(j.shutdown());

    ASSERT_TRUE(j.switchOn());
    ASSERT_TRUE(j.shutdown());
}

TEST(Joypad, enableDisableSequence) {
    Joypad j(0);
    ASSERT_TRUE(j.enable());
    ASSERT_TRUE(j.disable());

    ASSERT_TRUE(j.enable());
    ASSERT_TRUE(j.disable());
}

TEST(Joypad, faultSequence) {
    Joypad j(0);
    ASSERT_TRUE(j.switchOn());
    ASSERT_TRUE(j.fault());
    ASSERT_TRUE(j.shutdown());

    ASSERT_TRUE(j.switchOn());
    ASSERT_TRUE(j.enable());
    ASSERT_TRUE(j.fault());
    ASSERT_TRUE(j.shutdown());

    ASSERT_TRUE(j.switchOn());
    ASSERT_TRUE(j.enable());
    ASSERT_TRUE(j.shutdown());
}

TEST(Joypad, correctlySetupProperties) {
    Joypad j(0);
    ASSERT_TRUE(j.switchOn());
    ASSERT_GT(j.get<int>("configuration", "buttonsCount").value(), 0);
    ASSERT_GT(j.get<int>("configuration", "axesCount").value(), 0);

    std::cout << j.get<std::string>("configuration", "name").value() << std::endl;
    ASSERT_TRUE(j.shutdown());
}
