#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <urf/devices/robots/robotbase/MiniBot.hpp>

#include <memory>

using urf::devices::robots::robotbase::MiniBot;

const std::string PORT = "COM6";

TEST(MiniBot, switchonShutdownTest) {
    MiniBot b(PORT);

    ASSERT_TRUE(b.switchOn());
    ASSERT_TRUE(b.shutdown());

    ASSERT_TRUE(b.switchOn());
    ASSERT_TRUE(b.shutdown());
}

TEST(MiniBot, enableDisableTest) {
    MiniBot b(PORT);

    ASSERT_TRUE(b.switchOn());
    ASSERT_TRUE(b.enable());

    ASSERT_TRUE(b.disable());
    ASSERT_TRUE(b.enable());
    ASSERT_TRUE(b.disable());
}