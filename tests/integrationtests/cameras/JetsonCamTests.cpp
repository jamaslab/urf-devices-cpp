#include <iostream>
#include <opencv2/imgcodecs.hpp>

#include <urf/devices/sensors/cameras/rgb/CameraServer.hpp>
#include <urf/devices/sensors/cameras/rgb/jetson/JetsonCam.hpp>

#include <urf/algorithms/compression/jpeg/JpegVideoEncoder.hpp>

#include <urf/middleware/sockets/Server.hpp>

using urf::devices::sensors::cameras::JetsonCam;
using urf::devices::sensors::cameras::CameraServer;


int main() {
    // auto server = std::make_shared<urf::middleware::sockets::Server>("cameraServer", "tcp://*:8080");
    auto camera = std::make_shared<JetsonCam>(0);

    if (!camera->enable()) {
        std::cout << "Could not enable" << std::endl;
        return -1;
    }

    while(true) {
        auto frame= camera->getFrame();
        std::cout << "Frame: " << frame.size() << std::endl;
    }

    // CameraServer cameraServer(server, camera);

    // if (!cameraServer.open()) {
    //      return -1;
    // }

    // while(true) {
    //     std::this_thread::sleep_for(std::chrono::seconds(1));
    // }

    return 0;
}
