#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>
#include <opencv2/imgcodecs.hpp>
#include <thread>

#include <urf/devices/sensors/cameras/rgbd/IntelRealsense.hpp>

using namespace urf::devices;
using namespace urf::common::properties;
using urf::devices::sensors::cameras::IntelRealsense;

class IntelRealsenseIntegrationTest : public ::testing::Test {
 protected:
    IntelRealsenseIntegrationTest()
        : camera_(new IntelRealsense(0)) { }

    void SetUp() override { }

    void TearDown() override { }

    std::unique_ptr<IntelRealsense> camera_;
};

TEST_F(IntelRealsenseIntegrationTest, switchOnShutdownSequence) {
    ASSERT_TRUE(camera_->switchOn());
    ASSERT_FALSE(camera_->switchOn());
    ASSERT_TRUE(camera_->shutdown());
    ASSERT_FALSE(camera_->shutdown());

    ASSERT_TRUE(camera_->switchOn());
    ASSERT_FALSE(camera_->switchOn());
    ASSERT_TRUE(camera_->shutdown());
    ASSERT_FALSE(camera_->shutdown());
}

TEST_F(IntelRealsenseIntegrationTest, enableDisableSequenceSequence) {
    ASSERT_TRUE(camera_->switchOn());

    ASSERT_TRUE(camera_->enable());
    ASSERT_FALSE(camera_->enable());
    ASSERT_TRUE(camera_->reconfigure());
    ASSERT_TRUE(camera_->disable());
    ASSERT_FALSE(camera_->disable());
    ASSERT_FALSE(camera_->reconfigure());

    ASSERT_TRUE(camera_->enable());
    ASSERT_FALSE(camera_->enable());
    ASSERT_TRUE(camera_->reconfigure());
    ASSERT_TRUE(camera_->disable());
    ASSERT_FALSE(camera_->disable());
    ASSERT_FALSE(camera_->reconfigure());

    ASSERT_TRUE(camera_->shutdown());
}

TEST_F(IntelRealsenseIntegrationTest, faultSequence) {
    ASSERT_TRUE(camera_->switchOn());
    ASSERT_TRUE(camera_->fault());
    ASSERT_TRUE(camera_->shutdown());

    ASSERT_TRUE(camera_->switchOn());
    ASSERT_TRUE(camera_->enable());
    ASSERT_TRUE(camera_->fault());
    ASSERT_TRUE(camera_->shutdown());

    ASSERT_TRUE(camera_->switchOn());
    ASSERT_TRUE(camera_->enable());
    ASSERT_TRUE(camera_->shutdown());
}

TEST_F(IntelRealsenseIntegrationTest, correctlyGetColorFrames) {
    ASSERT_TRUE(camera_->switchOn());

    ASSERT_TRUE(camera_->set("camera_settings", "color_active", true));

    ASSERT_TRUE(camera_->enable());
    auto availableColorProfiles = std::dynamic_pointer_cast<
        ObservableSettingList<urf::devices::sensors::cameras::StreamProfile>>(
        camera_->settings()["camera_settings"]["color_resolution"]);

    for (auto profile : availableColorProfiles->getList()) {
        std::cout << "Setting camera to " << nlohmann::json(profile).dump() << std::endl;
        ASSERT_TRUE(camera_->set("camera_settings", "color_resolution", profile));
        ASSERT_TRUE(camera_->reconfigure());
        urf::devices::sensors::cameras::RgbdFrame frame;
        ASSERT_EQ(camera_
                      ->get<urf::devices::sensors::cameras::StreamProfile>("camera_settings",
                                                                           "color_resolution")
                      .value(),
                  profile);

        frame = camera_->getFrame();
        ASSERT_TRUE(frame.colorFrame);
        ASSERT_FALSE(frame.colorFrame.value().empty());
        ASSERT_EQ(frame.colorFrame.value().cols, profile.resolution.width);
        ASSERT_EQ(frame.colorFrame.value().rows, profile.resolution.height);
    }

    ASSERT_TRUE(camera_->shutdown());
}

TEST_F(IntelRealsenseIntegrationTest, correctlyGetDepthFrames) {
    ASSERT_TRUE(camera_->switchOn());
    ASSERT_TRUE(camera_->set("camera_settings", "color_active", false));
    ASSERT_TRUE(camera_->set("camera_settings", "depth_active", true));

    ASSERT_TRUE(camera_->enable());
    auto availableColorProfiles = std::dynamic_pointer_cast<
        ObservableSettingList<urf::devices::sensors::cameras::StreamProfile>>(
        camera_->settings()["camera_settings"]["depth_resolution"]);

    for (auto profile : availableColorProfiles->getList()) {
        std::cout << "Setting camera to " << nlohmann::json(profile).dump() << std::endl;
        ASSERT_TRUE(availableColorProfiles->setRequestedValue(profile));
        ASSERT_TRUE(camera_->reconfigure());

        urf::devices::sensors::cameras::RgbdFrame frame;

        frame = camera_->getFrame();
        ASSERT_TRUE(frame.depthFrame);
        ASSERT_FALSE(frame.depthFrame.value().empty());
        ASSERT_EQ(frame.depthFrame.value().cols, profile.resolution.width);
        ASSERT_EQ(frame.depthFrame.value().rows, profile.resolution.height);
    }

    ASSERT_TRUE(camera_->shutdown());
}

TEST_F(IntelRealsenseIntegrationTest, correctlyGetPointCloud) {
    ASSERT_TRUE(camera_->switchOn());
    ASSERT_TRUE(camera_->set("camera_settings", "pointcloud_active", true));

    ASSERT_TRUE(camera_->enable());
    for (int i = 0; i < 10; i++) {
        auto frame = camera_->getFrame();
        ASSERT_TRUE(frame.pointCloud);

        ASSERT_GT(frame.pointCloud.value()->size(), 0);
    }
    ASSERT_TRUE(camera_->shutdown());
}

TEST_F(IntelRealsenseIntegrationTest, correctlyAlignFrames) {
    ASSERT_TRUE(camera_->switchOn());
    ASSERT_TRUE(camera_->set("camera_settings", "color_active", true));
    ASSERT_TRUE(camera_->set("camera_settings", "depth_active", true));
    ASSERT_TRUE(camera_->set("camera_settings", "align_depth", true));

    ASSERT_TRUE(camera_->enable());

    urf::devices::sensors::cameras::RgbdFrame frame;

    for (int i=0; i< 10; i++) {
        auto frame = camera_->getFrame();
        ASSERT_TRUE(frame.depthFrame);
        ASSERT_TRUE(frame.colorFrame);
    }

    ASSERT_TRUE(camera_->shutdown());
}