#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>
#include <opencv2/imgcodecs.hpp>
#include <thread>

#include <urf/devices/sensors/cameras/rgb/RicohTheta.hpp>

using namespace urf::devices;
using namespace urf::common::properties;
using urf::devices::sensors::cameras::RicohTheta;

class RicohThetaIntegrationTest : public ::testing::Test {
 protected:
    RicohThetaIntegrationTest()
        : camera_(new RicohTheta(0)) { }

    void SetUp() override { }

    void TearDown() override { }

    std::unique_ptr<RicohTheta> camera_;
};

TEST_F(RicohThetaIntegrationTest, switchOnShutdownSequence) {
    ASSERT_TRUE(camera_->switchOn());
    ASSERT_FALSE(camera_->switchOn());
    ASSERT_TRUE(camera_->shutdown());
    ASSERT_FALSE(camera_->shutdown());

    ASSERT_TRUE(camera_->switchOn());
    ASSERT_FALSE(camera_->switchOn());
    ASSERT_TRUE(camera_->shutdown());
    ASSERT_FALSE(camera_->shutdown());
}

TEST_F(RicohThetaIntegrationTest, enableDisableSequenceSequence) {
    ASSERT_TRUE(camera_->switchOn());

    ASSERT_TRUE(camera_->enable());
    ASSERT_FALSE(camera_->enable());
    ASSERT_TRUE(camera_->reconfigure());
    ASSERT_TRUE(camera_->disable());
    ASSERT_FALSE(camera_->disable());
    ASSERT_FALSE(camera_->reconfigure());

    ASSERT_TRUE(camera_->enable());
    ASSERT_FALSE(camera_->enable());
    ASSERT_TRUE(camera_->reconfigure());
    ASSERT_TRUE(camera_->disable());
    ASSERT_FALSE(camera_->disable());
    ASSERT_FALSE(camera_->reconfigure());

    ASSERT_TRUE(camera_->shutdown());
}

TEST_F(RicohThetaIntegrationTest, faultSequence) {
    ASSERT_TRUE(camera_->switchOn());
    ASSERT_TRUE(camera_->fault());
    ASSERT_TRUE(camera_->shutdown());

    ASSERT_TRUE(camera_->switchOn());
    ASSERT_TRUE(camera_->enable());
    ASSERT_TRUE(camera_->fault());
    ASSERT_TRUE(camera_->shutdown());

    ASSERT_TRUE(camera_->switchOn());
    ASSERT_TRUE(camera_->enable());
    ASSERT_TRUE(camera_->shutdown());
}

TEST_F(RicohThetaIntegrationTest, correctlyGetFrames) {
    ASSERT_TRUE(camera_->switchOn());
    ASSERT_TRUE(camera_->enable());
    auto availableProfiles = std::dynamic_pointer_cast<
        ObservableSettingList<urf::devices::sensors::cameras::StreamProfile>>(
        camera_->settings()["camera_settings"]["resolution"]);

    for (auto profile : availableProfiles->getList()) {
        availableProfiles->setRequestedValue(profile);
        ASSERT_TRUE(camera_->reconfigure());
        cv::Mat frame;
        for (int i = 0; i < 100; i++) {
            auto start = std::chrono::high_resolution_clock::now();
            frame = camera_->getFrame();
            ASSERT_FALSE(frame.empty());
            ASSERT_EQ(frame.cols, profile.resolution.width);
            ASSERT_EQ(frame.rows, profile.resolution.height);
            auto end = std::chrono::high_resolution_clock::now();
            std::cout
                << 1000.0f / std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
                << std::endl;
        }
    }

    ASSERT_TRUE(camera_->shutdown());
}
