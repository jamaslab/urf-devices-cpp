#include <gtest/gtest.h>
#include <gmock/gmock.h>

class IntegrationTestEnvironment : public ::testing::Environment {
    explicit IntegrationTestEnvironment(int argc, char **argv) {
    }
};

int main(int argc, char **argv) {
    ::testing::InitGoogleMock(&argc, argv);
    testing::AddGlobalTestEnvironment(new IntegrationTestEnvironment(argc, argv));

    return RUN_ALL_TESTS();
}
