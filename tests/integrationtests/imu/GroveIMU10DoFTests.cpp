#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <urf/devices/communication/i2c/I2CBus.hpp>
#include <urf/devices/sensors/imu/GroveIMU10DoF.hpp>

#include <memory>

using urf::devices::communication::I2CBus;
using urf::devices::sensors::imu::GroveIMU10DoF;

const std::string BUS_NAME("/dev/i2c-1");

TEST(GroveIMU10Dof, enableDisableTest) {
    auto i2cbus = std::make_shared<I2CBus>(BUS_NAME);
    ASSERT_TRUE(i2cbus->open());

    GroveIMU10DoF imu(i2cbus);
    int received = 0;
    imu.settings()["measurements"]["a"]->onAnyValueChange([&received](auto, auto) { received++; });

    ASSERT_TRUE(imu.enable());
    std::this_thread::sleep_for(std::chrono::seconds(2));
    ASSERT_TRUE(imu.disable());
    ASSERT_GT(received, 0);
    ASSERT_TRUE(i2cbus->close());
}