import os
from conans import ConanFile, CMake, tools


def get_file(filename):
    f = open(filename, "r")
    return f.read()


class UrfDevicesCppConan(ConanFile):
    _default_windows_import_paths = [
        "../{cf.settings.os}/{cf.settings.build_type}/bin/{cf.settings.build_type}"]

    name = "urf_devices_cpp"
    version = "0.0.1"
    license = "MIT"
    author = "Giacomo Lunghi"
    url = "https://github.com/Jamaslab/urf_devices_cpp"
    description = "Unified Robotic Framework Devices"
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "build_it": [True, False],
        "cameras": [True, False],
        "imus": [True, False],
        "with_realsense": [True, False],
        "rgbdcameras": [True, False],
        "robotbases": [True, False],
        "shared": [True, False]
    }
    import_paths = []
    default_options = {
        "build_it": False,
        "cameras": True,
        "imus": True,
        "with_realsense": True,
        "rgbdcameras": True,
        "robotbases": True,
        "shared": False
    }
    requires = ("urf_middleware_cpp/0.10.0@uji-cirtesu-irslab+urobf+urf-middleware-cpp/stable",
                "urf_algorithms_cpp/0.4.0@uji-cirtesu-irslab+urobf+urf-algorithms-cpp/stable",
                "urf_common_cpp/1.10.0@uji-cirtesu-irslab+urobf+urf-common-cpp/stable")
    build_requires = ("gtest/1.10.0", "cmake/3.25.0")
    generators = "cmake", "cmake_find_package", "virtualenv"
    exports_sources = ["environment/*", "src/*", "tests/*",
                       "CMakeLists.txt", "LICENSE", "Version.txt", "README.md"]
    
    @property
    def default_user(self):
        return "uji-cirtesu-irslab+urobf+urf-devices-cpp"

    @property
    def default_channel(self):
        return "stable"

    def configure(self):
        self.options["urf_algorithms_cpp"].shared = True
        self.options["urf_middleware_cpp"].shared = False

        self.options["urf_algorithms_cpp"].video_compression = self.options.rgbdcameras or self.options.cameras
        self.options["urf_algorithms_cpp"].pointcloud_compression = self.options.rgbdcameras

        if self.options.rgbdcameras and self.options.with_realsense:
            self.requires("librealsense/2.49.0@uji-cirtesu-irslab+urobf+urf-externals/stable")
            self.options["librealsense"].shared = False

        if tools.os_info.is_linux and self.options.cameras:
            self.requires("libuvc_theta/1.0.0@uji-cirtesu-irslab+urobf+urf-externals/stable")
            self.options["libuvc_theta"].shared = False

        if self.options.robotbases:
            self.requires("libmodbus/3.1.6")

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = os.path.join(
            self.recipe_folder, 'package/')
        cmake.definitions["CMAKE_MODULE_PATH"] = self.install_folder.replace(
            "\\", "/")
        cmake.definitions["CMAKE_BUILD_TYPE"] = self.settings.build_type
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared
        if not self.options.shared:
            cmake.definitions['CMAKE_POSITION_INDEPENDENT_CODE'] = True

        cmake.definitions["BUILD_CAMERAS"] = self.options.cameras
        cmake.definitions["BUILD_IMUS"] = self.options.imus
        cmake.definitions["BUILD_RGBDCAMERAS"] = self.options.rgbdcameras
        cmake.definitions["BUILD_ROBOTBASES"] = self.options.robotbases
        cmake.definitions["BUILD_REALSENSE"] = self.options.rgbdcameras and self.options.with_realsense
        cmake.definitions["BUILD_IT"] = self.options.build_it

        cmake.configure(
            build_folder='build/%s' % str(self.settings.os) + '/' + str(self.settings.build_type))
        cmake.build()
        cmake.test(output_on_failure=True)
        cmake.install()

    def imports(self):
        if tools.os_info.is_windows:
            import_paths = getattr(self, 'import_paths') + \
                self._default_windows_import_paths
            for ipath in import_paths:
                self.copy("*.dll", str(ipath.format(cf=self)), "bin")
                self.copy("*.dll", str(ipath.format(cf=self)), "lib")
                self.copy("*.dylib", str(ipath.format(cf=self)), "lib")

    def package(self):
        self.copy("*.hpp", dst=".", src=os.path.join(self.recipe_folder,
                                                     'package/'), keep_path=True)
        self.copy("*.h", dst=".", src=os.path.join(self.recipe_folder,
                                                   'package/'), keep_path=True)
        if tools.os_info.is_linux:
            self.copy(
                "*.so", dst=".", src=os.path.join(self.recipe_folder, 'package/'), keep_path=True)
            self.copy(
                "*.a", dst=".", src=os.path.join(self.recipe_folder, 'package/'), keep_path=True)
        elif tools.os_info.is_windows:
            self.copy(
                "*.dll", dst=".", src=os.path.join(self.recipe_folder, 'package/'), keep_path=True)
            self.copy(
                "*.lib", dst=".", src=os.path.join(self.recipe_folder, 'package/'), keep_path=True)

    def package_info(self):
        self.cpp_info.libs = ['urf_devices']
        self.cpp_info.includedirs = ['include/']
        self.cpp_info.libdirs = ['lib']
