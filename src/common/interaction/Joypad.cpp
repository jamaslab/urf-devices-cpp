#include "urf/devices/interaction/Joypad.hpp"

#include <fcntl.h>
#include <linux/joystick.h>
#include <sys/stat.h>
#include <unistd.h>

#ifdef __GNUC__
#    if __GNUC_PREREQ(8, 0)
#        include <filesystem>
namespace fs = std::filesystem;
#    else
#        include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
#    endif
#else
#    include <filesystem>
namespace fs = std::filesystem;
#endif

#include <urf/common/logger/Logger.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("Joypad");
}
namespace urf {
namespace devices {
namespace interaction {

Joypad::Joypad(int joypadNumber)
    : Device("Joypad", {"interactionDevice"})
    , fd_(-1)
    , stopThread_(false)
    , joypadEventThread_()
    , buttonsCount_(new common::properties::ObservableSetting<int>)
    , axesCount_(new common::properties::ObservableSetting<int>)
    , joypadName_(new common::properties::ObservableSetting<std::string>)
    , joypadId_(new common::properties::ObservableSetting<int>)
    , axesPosition_(new common::properties::ObservableProperty<nlohmann::json>)
    , buttonsPressed_(new common::properties::ObservableProperty<nlohmann::json>)
    , configurations_(new common::properties::PropertyNode)
    , measurements_(new common::properties::PropertyNode) {

    joypadId_->setRequestedValue(joypadNumber);
    configurations_->insert("id", joypadId_);
    configurations_->insert("name", joypadName_);
    configurations_->insert("axesCount", axesCount_);
    configurations_->insert("buttonsCount", buttonsCount_);

    settings_.insert("configuration", configurations_);

    measurements_->insert("axes", axesPosition_);
    measurements_->insert("buttons", buttonsPressed_);

    settings_.insert("measurements", measurements_);
}

bool Joypad::resetFaultImpl() {
    stopThread_ = true;
    if (joypadEventThread_.joinable())
        joypadEventThread_.join();

    if (fd_ != -1) {
        ::close(fd_);
        fd_ = -1;
    }

    return true;
}

bool Joypad::switchOnImpl() {
    auto const joypads = availableJoypads();
    if (std::find(joypads.begin(), joypads.end(), joypadId_->getRequestedValue()) ==
        joypads.end()) {
        LOGGER.warn("Joypad {} not connected", joypadId_->getRequestedValue());
        return false;
    }

    std::string filename = "/dev/input/js" + std::to_string(joypadId_->getRequestedValue());
    fd_ = ::open(filename.c_str(), O_RDONLY | O_NONBLOCK);
    if (fd_ == -1) {
        LOGGER.warn("Failed to open file descriptor");
        return false;
    }

    joypadId_->setValue(joypadId_->getRequestedValue());

    axesCount_->setValue(getAxesCount());
    buttonsCount_->setValue(getButtonsCount());
    joypadName_->setValue(getJoypadName());

    return true;
}

bool Joypad::shutdownImpl() {
    stopThread_ = true;
    if (joypadEventThread_.joinable())
        joypadEventThread_.join();

    if (fd_ != -1) {
        ::close(fd_);
        fd_ = -1;
    }

    return true;
}

bool Joypad::disableImpl() {
    stopThread_ = true;

    if (joypadEventThread_.joinable())
        joypadEventThread_.join();

    return true;
}

bool Joypad::enableImpl() {
    stopThread_ = true;

    joypadEventThread_ = std::thread(&Joypad::loop, this);

    while (stopThread_) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    return true;
}

bool Joypad::faultImpl() {
    stopThread_ = true;

    return true;
}

void Joypad::loop() {
    stopThread_ = false;
    auto event = std::make_unique<JoypadEvent>();
    while (!stopThread_) {
        int bytes = ::read(fd_, event.get(), sizeof(JoypadEvent));
        if (bytes == -1) {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            continue;
        }

        if ((event->type == 1) || (event->type == 129)) {
            auto json = buttonsPressed_->getValue();
            json[event->number] = event->value;
            buttonsPressed_->setValue(json);
        } else if ((event->type == 2) || (event->type == 130)) {
            auto json = axesPosition_->getValue();
            json[event->number] = event->value;
            axesPosition_->setValue(json);
        }
    }
}

size_t Joypad::getButtonsCount() {
    __u8 buttons;
    if (ioctl(fd_, JSIOCGBUTTONS, &buttons) == -1)
        return 0;

    return buttons;
}

size_t Joypad::getAxesCount() {
    __u8 axes;

    if (ioctl(fd_, JSIOCGAXES, &axes) == -1)
        return 0;

    return axes;
}

std::string Joypad::getJoypadName() {
    char name[128];
    if (ioctl(fd_, JSIOCGNAME(sizeof(name)), name) < 0)
        return "Unknown";
    return std::string(name);
}

std::vector<int> Joypad::availableJoypads() {
    std::vector<int> retval;
    const std::string path = "/dev/input";
    for (const auto& entry : fs::directory_iterator(path)) {
        if (entry.path().filename().string().find("js") == 0) {
            retval.push_back(std::stoi(entry.path().filename().string().substr(2)));
        }
    }

    return retval;
}

} // namespace interaction
} // namespace devices
} // namespace urf
