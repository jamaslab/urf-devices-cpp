#include "urf/devices/Device.hpp"

#include <urf/common/logger/Logger.hpp>
#include <urf/common/properties/ObservablePropertyFactory.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("Device");
}

namespace urf {
namespace devices {

Device::Device(const std::string& deviceName, const std::vector<std::string>& deviceClass)
    : state_(new common::properties::ObservableProperty<
             common::components::StatePair>)
    , settings_()
    , componentName_(new common::properties::ObservableProperty<std::string>)
    , componentClass_(new common::properties::ObservableProperty<std::vector<std::string>>) {
    componentName_->setValue(deviceName);
    componentClass_->setValue(deviceClass);

    state_->setValue({ComponentStates::ReadyToSwitchOn, ComponentStateTransitions::NoTransition});

    auto deviceSettingsNode = std::make_shared<common::properties::PropertyNode>();
    deviceSettingsNode->insert("state", state_);
    deviceSettingsNode->insert("name", componentName_);
    deviceSettingsNode->insert("class", componentClass_);

    settings_.insert("component", deviceSettingsNode);
}

bool Device::resetFault() noexcept {
    LOGGER.trace("resetFault");
    if (!startTransition(ComponentStateTransitions::ResetFault)) {
        LOGGER.warn("Can't reset fault in current state");
        return false;
    }

    if (!resetFaultImpl()) {
        return false;
    }

    transitionCompleted();
    return true;
}

bool Device::switchOn() noexcept {
    LOGGER.trace("switchOn()");
    if (!startTransition(ComponentStateTransitions::SwitchOn)) {
        LOGGER.warn("Can't switch on in current state");
        return false;
    }

    if (!switchOnImpl()) {
        fault();
        return false;
    }

    transitionCompleted();
    return true;
}
bool Device::shutdown() noexcept {
    LOGGER.trace("shutdown()");
    if ((currentState() == ComponentStates::Enabled) && (!disable())) {
        return false;
    }

    if (!startTransition(ComponentStateTransitions::Shutdown)) {
        LOGGER.warn("Can't shutdown in current state");
        return false;
    }

    if (!shutdownImpl()) {
        fault();
        return false;
    }

    transitionCompleted();
    return true;
}

bool Device::disable() noexcept {
    LOGGER.trace("disable()");
    if (!startTransition(ComponentStateTransitions::Disable)) {
        LOGGER.warn("Can't disable in current state");
        return false;
    }

    if (!disableImpl()) {
        fault();
        return false;
    }

    transitionCompleted();
    return true;
}

bool Device::enable() noexcept {
    LOGGER.trace("enable()");
    if ((currentState() == ComponentStates::ReadyToSwitchOn) && (!switchOn())) {
        return false;
    }

    if (!startTransition(ComponentStateTransitions::Enable)) {
        LOGGER.warn("Can't enable in current state");
        return false;
    }

    if (!enableImpl()) {
        fault();
        return false;
    }

    transitionCompleted();
    return true;
}

bool Device::reconfigure() noexcept {
    LOGGER.trace("reconfigure()");
    if (!disable()) {
        return false;
    }

    if (!enable()) {
        return false;
    }

    return true;
}

bool Device::fault() noexcept {
    LOGGER.trace("fault()");
    failTransition();

    faultImpl();

    return true;
}

ComponentStates Device::currentState() const {
    return state_->getValue().first;
}

ComponentStateTransitions Device::currentTransition() const {
    return state_->getValue().second;
}

std::string Device::componentName() const {
    return componentName_->getValue();
}

std::vector<std::string> Device::componentClass() const {
    return componentClass_->getValue();
}

bool Device::startTransition(ComponentStateTransitions transition) {
    if (currentTransition() != ComponentStateTransitions::NoTransition) {
        return false;
    }

    if ((transition == ComponentStateTransitions::ResetFault) &&
        (currentState() != ComponentStates::Fault)) {
        return false;
    } else if ((transition == ComponentStateTransitions::SwitchOn) &&
               (currentState() != ComponentStates::ReadyToSwitchOn)) {
        return false;
    } else if ((transition == ComponentStateTransitions::Enable) &&
               (currentState() != ComponentStates::ReadyToSwitchOn) &&
               (currentState() != ComponentStates::SwitchedOn)) {
        return false;
    } else if ((transition == ComponentStateTransitions::Disable) &&
               (currentState() != ComponentStates::Enabled)) {
        return false;
    } else if ((transition == ComponentStateTransitions::Shutdown) &&
               (currentState() == ComponentStates::ReadyToSwitchOn)) {
        return false;
    }

    state_->setValue({currentState(), transition});

    return true;
}

bool Device::failTransition() {
    state_->setValue({ComponentStates::Fault, ComponentStateTransitions::NoTransition});

    return true;
}

bool Device::transitionCompleted() {
    ComponentStates outState;
    if (currentTransition() == ComponentStateTransitions::SwitchOn) {
        outState = ComponentStates::SwitchedOn;
    } else if (currentTransition() == ComponentStateTransitions::Init) {
        outState = ComponentStates::ReadyToSwitchOn;
    } else if (currentTransition() == ComponentStateTransitions::Shutdown) {
        outState = ComponentStates::ReadyToSwitchOn;
    } else if (currentTransition() == ComponentStateTransitions::Enable) {
        outState = ComponentStates::Enabled;
    } else if (currentTransition() == ComponentStateTransitions::Disable) {
        outState = ComponentStates::SwitchedOn;
    } else if (currentTransition() == ComponentStateTransitions::ResetFault) {
        outState = ComponentStates::ReadyToSwitchOn;
    }

    state_->setValue({outState, ComponentStateTransitions::NoTransition});
    return true;
}

common::properties::PropertyNode Device::settings() const {
    return settings_;
}

} // namespace devices
} // namespace urf

