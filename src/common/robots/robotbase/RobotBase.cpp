#include "urf/devices/robots/robotbase/RobotBase.hpp"

namespace urf {
namespace devices {
namespace robots {
namespace robotbase {

RobotBase::RobotBase(const std::string& deviceName)
    : Device(deviceName, { "RobotBase" })
    , setpoints_(new common::properties::PropertyNode)
    , measurements_(new common::properties::PropertyNode)
    , configuration_(new common::properties::PropertyNode) {

    settings_.insert("setpoints", setpoints_);
    settings_.insert("measurements", measurements_);
    settings_.insert("configuration", configuration_);
}

} // namespace robotbase
} // namespace robots
} // namespace devices
} // namespace urf
