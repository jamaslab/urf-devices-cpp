#include "urf/devices/robots/robotbase/MiniBot.hpp"

#include <urf/algorithms/control/robotbase/RobotBaseOmnidirectionalKinematics.hpp>
#include <urf/common/logger/Logger.hpp>

#include <algorithm>

namespace {
auto LOGGER = urf::common::getLoggerInstance("MiniBot");
}

namespace urf {
namespace devices {
namespace robots {
namespace robotbase {

const uint16_t INPUT_REGISTERS_OFFSETS = 7;
const uint16_t HOLING_REGISTERS_OFFSETS = 5;

MiniBot::MiniBot(const std::string& modbusPort)
    : RobotBase("MiniBot")
    , modbusPort_(modbusPort)
    , device_(nullptr)
    , cartesianVelocitySetpoint_(
          new common::properties::ObservableSetting<std::vector<float>>())
    , movementSafetyTimeout_(new common::properties::ObservableSettingRanged<float>({0.1f, 2.0f}))
    , cartesianVelocity_(new common::properties::ObservableProperty<std::vector<float>>())
    , wheelRadius_(new common::properties::ObservableProperty<float>)
    , wheelsPosition_(new common::properties::ObservableProperty<std::vector<float>>)
    , lastSetpointChange_(std::chrono::high_resolution_clock::now())
    , kinematics_()
    , stopControlThread_(false)
    , controlThread_()
    , motorStatus_()
    , motorSetpoint_()
    , motorsDirection_({-1, -1, 1, 1}) {
    LOGGER.trace("CTor");

    setpoints_->insert("velocity", cartesianVelocitySetpoint_);

    configuration_->insert("movementSafetyTimeout", movementSafetyTimeout_);
    configuration_->insert("wheelRadius", wheelRadius_);
    configuration_->insert("wheelsPosition", wheelsPosition_);

    measurements_->insert("velocity", cartesianVelocity_);

    cartesianVelocitySetpoint_->setValue({.0f, .0f, .0f});
    cartesianVelocitySetpoint_->setRequestedValue({.0f, .0f, .0f});

    movementSafetyTimeout_->setValue(0.5f);
    movementSafetyTimeout_->setRequestedValue(0.5f);

    wheelRadius_->setValue(0.10f);
    wheelsPosition_->setValue({0.08f, 0.125f});

    kinematics_.reset(new algorithms::control::RobotBaseOmnidirectionalKinematics(
        wheelRadius_->getValue(), wheelsPosition_->getValue()[0], wheelsPosition_->getValue()[1]));
}

MiniBot::~MiniBot() {
    shutdown();
}

bool MiniBot::setCartesianVelocity(const std::array<float, 3>& velocities) {
    lastSetpointChange_ = std::chrono::high_resolution_clock::now();
    return cartesianVelocitySetpoint_->setRequestedValue({velocities[0], velocities[1], velocities[2]});
}

bool MiniBot::resetFaultImpl() {
    return true;
}

bool MiniBot::switchOnImpl() {
    device_ = modbus_new_rtu(modbusPort_.c_str(), 115200, 'N', 8, 1);
    if (!device_) {
        LOGGER.warn("Could not create modbus object");
        return false;
    }

    if (modbus_connect(device_) == -1) {
        LOGGER.warn("Could not open modbus port: {}", std::string(modbus_strerror(errno)));
        modbus_free(device_);
        return false;
    }

    if (modbus_set_slave(device_, 0x02) == -1) {
        LOGGER.warn("Could not set slave: {}", std::string(modbus_strerror(errno)));
        modbus_close(device_);
        modbus_free(device_);
        return false;
    }

    if (modbus_set_response_timeout(device_, 1, 0) == -1) {
        LOGGER.warn("Could not set timeout: {}", std::string(modbus_strerror(errno)));
        modbus_close(device_);
        modbus_free(device_);
        return false;
    }

    uint16_t motorsCount = 0;
    if (modbus_read_input_registers(device_, 0, 1, &motorsCount) == -1) {
        LOGGER.warn("Could not read motors count: {}", std::string(modbus_strerror(errno)));
        modbus_close(device_);
        modbus_free(device_);
        return false;
    }

    if (motorsCount == 0) {
        LOGGER.warn("No motors found");
        return false;
    }
    LOGGER.info("Motors found: {}", motorsCount);

    return true;
}

bool MiniBot::shutdownImpl() {
    if (device_) {
        modbus_close(device_);
        modbus_free(device_);
    }

    return true;
}

bool MiniBot::disableImpl() {
    stopControlThread_ = true;
    controlThread_.join();

    return true;
}

bool MiniBot::enableImpl() {
    movementSafetyTimeout_->setValue(movementSafetyTimeout_->getRequestedValue());
    for (size_t i=0; i < motorSetpoint_.size(); i++) {
        motorSetpoint_[i].minPosition = 0;
        motorSetpoint_[i].maxPosition = 0;
        motorSetpoint_[i].speed = 0;
        motorSetpoint_[i].enable = 1;
        motorSetpoint_[i].position = 0;
    }

    stopControlThread_ = true;
    controlThread_ = std::thread(&MiniBot::controlLoop, this);
    // Let's wait for the thread to actually start
    while (stopControlThread_) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    };

    return true;
}

bool MiniBot::faultImpl() {
    if (controlThread_.joinable()) {
        stopControlThread_ = true;
        controlThread_.join();
    }

    return true;
}

void MiniBot::controlLoop() {
    stopControlThread_ = false;
    std::vector<float> nextVelocities({.0, .0, .0, .0});

    while (!stopControlThread_) {
        auto start = std::chrono::high_resolution_clock::now();
        // Read motor status from input registers
        if (modbus_read_input_registers(
                device_, 1, 28, reinterpret_cast<uint16_t*>(motorStatus_.data())) == -1) {
            LOGGER.warn("Could not read motor status: {}", std::string(modbus_strerror(errno)));
            fault();
            return;
        }

        updateStatus();

        // Write motor control to holding registers
        if (modbus_write_registers(device_, 1, 20, reinterpret_cast<uint16_t*>(motorSetpoint_.data())) == -1) {
            LOGGER.warn("Could not write modbus registers: {}",
                        std::string(modbus_strerror(errno)));
            fault();
            return;
        }

        // Calculate the next velocity setpoint. If no new velocity was provided, don't move
        if (std::chrono::duration_cast<std::chrono::milliseconds>(start -
                                                                  lastSetpointChange_.load())
                .count() > movementSafetyTimeout_->getValue() * 1000) {
                    nextVelocities.assign(4, 0);
        } else {
            auto velocitySetpoint = cartesianVelocitySetpoint_->getRequestedValue();
            if (velocitySetpoint.size() != 3) {
                nextVelocities.assign(4, 0);
            } else {
                nextVelocities = kinematics_->getWheelsVelocity(velocitySetpoint[0], velocitySetpoint[1], velocitySetpoint[2]);
            }
        }

        auto maxAbs = *std::max_element(nextVelocities.begin(), nextVelocities.end(), [](auto const& a, auto const& b) {
            return fabs(a) < fabs(b);
        });

        // One of the wheels velocities is above the limit and needs to be scaled down with all the others
        float scalingFactor = 1, motorFactor = 200;
        if (fabs(maxAbs) > 1024) {
            scalingFactor = 1024.0f/fabs(maxAbs);
        }

        for (size_t i=0; i < motorSetpoint_.size(); i++) {
            auto calculatedSpeed = motorsDirection_[i] * nextVelocities[i] * scalingFactor * motorFactor;
            if (calculatedSpeed < 0) {
                motorSetpoint_[i].speed = 1024 + static_cast<uint16_t>(fabs(calculatedSpeed));
            } else {
                motorSetpoint_[i].speed = static_cast<uint16_t>(calculatedSpeed);
            }
        }
    }

    // Before leaving the control loop, stop the motors
    for (size_t i=0; i < motorSetpoint_.size(); i++) {
        motorSetpoint_[i].enable = 0;
        motorSetpoint_[i].speed = 0;
    }

    if (modbus_write_registers(device_, 1, 20, reinterpret_cast<uint16_t*>(motorSetpoint_.data())) == -1) {
        LOGGER.warn("Could not write modbus registers: {}",
                    std::string(modbus_strerror(errno)));
        fault();
    }
}

void MiniBot::updateStatus() {
    std::vector<float> wheelsVelocity({.0f, .0f, .0f, .0f});
    float motorFactor = 200;
    for (size_t i=0; i < motorStatus_.size(); i++) {
        if (motorStatus_[i].speed > 1024) {
            wheelsVelocity[i] = static_cast<float>(-motorStatus_[i].speed + 1024);
        } else {
            wheelsVelocity[i] = static_cast<float>(motorStatus_[i].speed);
        }
        wheelsVelocity[i] = wheelsVelocity[i]*motorsDirection_[i]/motorFactor;
    }

    cartesianVelocity_->setValue(kinematics_->getCartesianVelocity(wheelsVelocity));
}

} // namespace robotbase
} // namespace robots
} // namespace devices
} // namespace urf
