#include "urf/devices/sensors/cameras/rgb/RgbCamera.hpp"

#include <urf/common/properties/ObservablePropertyFactory.hpp>

namespace urf {
namespace devices {
namespace sensors {
namespace cameras {

StreamProfile::StreamProfile(const algorithms::compression::VideoResolution& res, uint32_t f)
    : resolution(res)
    , fps(f)
    , focalPoint(std::nullopt)
    , principalPoint(std::nullopt) { }

bool StreamProfile::operator!=(const StreamProfile& b) const {
    return (!(resolution == b.resolution) || (fps != b.fps));
}

bool StreamProfile::operator==(const StreamProfile& b) const {
    return !((*this) != b);
}

bool StreamProfile::operator<(const StreamProfile& b) const {
    return (resolution.width < b.resolution.width) && (resolution.height < b.resolution.height);
}

bool StreamProfile::operator>(const StreamProfile& b) const {
    return !((*this) < b);
}

void to_json(nlohmann::json& j, const StreamProfile& p) {
    j["width"] = p.resolution.width;
    j["height"] = p.resolution.height;
    j["fps"] = p.fps;

    if (p.focalPoint) {
        j["f"] = p.focalPoint.value();
    }

    if (p.principalPoint) {
        j["c"] = p.principalPoint.value();
    }
}

void from_json(const nlohmann::json& j, StreamProfile& p) {
    p.resolution =
        algorithms::compression::VideoResolution(j["width"].get<int>(), j["height"].get<int>());
    p.fps = j["fps"].get<uint32_t>();

    if (j.count("f") == 1)
        p.focalPoint = j["f"].get<std::pair<float, float>>();
    else
        p.focalPoint = std::nullopt;

    if (j.count("c") == 1)
        p.principalPoint = j["c"].get<std::pair<float, float>>();
    else
        p.principalPoint = std::nullopt;
}

RgbCamera::RgbCamera(const std::string& deviceName)
    : Device(deviceName, {"RgbCamera"})
    , configuration_(new common::properties::PropertyNode) {
    settings_.insert("configuration", configuration_);
}

} // namespace cameras
} // namespace sensors
} // namespace devices
} // namespace urf

namespace urf {
namespace common {
namespace properties {

// REGISTER_PROPERTY_DATATYPE("cvsize", cv::Size)
REGISTER_PROPERTY_DATATYPE(streamprofile, urf::devices::sensors::cameras::StreamProfile)

} // namespace properties
} // namespace common
} // namespace urf
