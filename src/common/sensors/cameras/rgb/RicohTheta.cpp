#include "urf/devices/sensors/cameras/rgb/RicohTheta.hpp"

#include <urf/algorithms/compression/VideoCompressionFactory.hpp>
#include <urf/common/containers/ThreadSafeQueue.hpp>
#include <urf/common/logger/Logger.hpp>

#include <condition_variable>
#include <mutex>

namespace {
auto LOGGER = urf::common::getLoggerInstance("RicohTheta");

constexpr uint32_t USBVID_RICOH = 0x05ca;
constexpr uint32_t USBPID_THETAV_UVC = 0x2712;
constexpr uint32_t USBPID_THETAZ1_UVC = 0x2715;

class streamCtx {
 public:
    std::unique_ptr<urf::algorithms::compression::IVideoDecoder> decoder;
    urf::common::containers::ThreadSafeQueue<urf::algorithms::compression::VideoFrame> framesQueue;
    std::atomic<bool> failed;

    streamCtx()
        : decoder()
        , framesQueue()
        , failed(false) {
        decoder = urf::algorithms::compression::VideoCompressionFactory::getDecoder("x264");
    }
};

void cb(uvc_theta_frame_t* frame, void* ptr) {
    auto ctx = reinterpret_cast<streamCtx*>(ptr);

    if (!ctx->decoder->addBytes(
            std::vector<uint8_t>(reinterpret_cast<uint8_t*>(frame->data),
                                 reinterpret_cast<uint8_t*>(frame->data) + frame->data_bytes))) {
        ctx->failed = true;
    }

    auto fr = ctx->decoder->getFrame();
    if (!fr.empty()) {
        ctx->framesQueue.push(std::move(fr));
    }

    return;
}

} // namespace

namespace urf {
namespace devices {
namespace sensors {
namespace cameras {

RicohTheta::RicohTheta(uint8_t cameraId)
    : Device("RicohTheta", {"RgbCamera", "PanoramicCamera"})
    , RgbCamera("RicohTheta")
    , context_(NULL)
    , device_(NULL)
    , deviceHandle_(NULL)
    , descriptor_(NULL)
    , cameraIdConfig_(new common::properties::ObservableSetting<uint8_t>)
    , resolutionConfig_(new common::properties::ObservableSettingList<StreamProfile>(
          {StreamProfile({1920, 960}, 29), StreamProfile({3840, 1920}, 29)})) {

    LOGGER.trace("CTor");
    configuration_->insert("cameraId", cameraIdConfig_);
    cameraIdConfig_->setRequestedValue(cameraId);

    configuration_->insert("resolution", resolutionConfig_);
    resolutionConfig_->setRequestedValue(StreamProfile({1920, 960}, 29));
}

RicohTheta::~RicohTheta() {
    LOGGER.trace("DTor");
    shutdown();
}

bool RicohTheta::resetFaultImpl() {
    LOGGER.trace("resetFaultImpl");
    return shutdown();
}

bool RicohTheta::switchOnImpl() {
    LOGGER.trace("switchOnImpl");

    if (uvc_theta_init(&context_, NULL) != UVC_SUCCESS) {
        throw std::runtime_error("Could not initialize uvc_theta_init");
    }

    if (!findDevice()) {
        return false;
    }

    LOGGER.info("Found {} [Serial: {}]", descriptor_->product, descriptor_->serialNumber);

    if (uvc_theta_open(device_, &deviceHandle_) != UVC_SUCCESS) {
        LOGGER.error("Could not open device");
        return false;
    }

    cameraIdConfig_->setValue(cameraIdConfig_->getRequestedValue());

    return true;
}

bool RicohTheta::shutdownImpl() {
    LOGGER.trace("shutdownImpl");

    if (deviceHandle_) {
        LOGGER.trace("uvc_theta_close");
        uvc_theta_close(deviceHandle_);
        deviceHandle_ = nullptr;
    }

    if (device_) {
        LOGGER.trace("uvc_theta_unref_device");
        uvc_theta_unref_device(device_);
        device_ = nullptr;
    }

    if (descriptor_) {
        LOGGER.trace("uvc_theta_free_device_descriptor");
        uvc_theta_free_device_descriptor(descriptor_);
        descriptor_ = nullptr;
    }

    if (context_)
        uvc_theta_exit(context_);

    return true;
}

bool RicohTheta::disableImpl() {
    LOGGER.trace("disableImpl");

    if (deviceHandle_) {
        LOGGER.trace("uvc_theta_stop_streaming");
        uvc_theta_stop_streaming(deviceHandle_);

        if (streamCtx_) {
            auto ctx = reinterpret_cast<streamCtx*>(streamCtx_);
            delete ctx;
            streamCtx_ = nullptr;
        }
    }

    return true;
}

bool RicohTheta::enableImpl() {
    LOGGER.trace("enableImpl");

    LOGGER.debug("Starting stream {}x{}",
                 resolutionConfig_->getRequestedValue().resolution.width,
                 resolutionConfig_->getRequestedValue().resolution.height);
    if (uvc_theta_get_stream_ctrl_format_size(
            deviceHandle_,
            &stream_,
            UVC_FRAME_FORMAT_H264,
            resolutionConfig_->getRequestedValue().resolution.width,
            resolutionConfig_->getRequestedValue().resolution.height,
            resolutionConfig_->getRequestedValue().fps) != UVC_SUCCESS) {
        LOGGER.error("Could not get stream control");
        return false;
    }
    resolutionConfig_->setValue(resolutionConfig_->getRequestedValue());

    streamCtx_ = new streamCtx();

    if (uvc_theta_start_streaming(deviceHandle_, &stream_, cb, streamCtx_, 0) != UVC_SUCCESS) {
        LOGGER.error("Could not start streaming");
        return false;
    }

    getFrame();

    return true;
}

bool RicohTheta::faultImpl() {
    LOGGER.trace("faultImpl");
    disableImpl();

    return true;
}

algorithms::compression::VideoFrame RicohTheta::getFrame() {
    auto ctx = reinterpret_cast<streamCtx*>(streamCtx_);

    auto frameOpt = ctx->framesQueue.pop(std::chrono::seconds(4));

    if (!frameOpt) {
        LOGGER.error("Timeout while waiting for frame");
        fault();
        return algorithms::compression::VideoFrame{};
    }

    if (frameOpt.value().empty()) {
        LOGGER.error("Something went wrong during frame grabber");
        fault();
        return algorithms::compression::VideoFrame{};
    }

    return frameOpt.value();
}

bool RicohTheta::findDevice() {
    uvc_theta_device_t **devlist, *dev;

    std::vector<int> ids;
    if (uvc_theta_find_devices(context_, &devlist, USBVID_RICOH, 0, NULL) != UVC_SUCCESS) {
        LOGGER.warn("No camera found");
        return false;
    }

    for (int idx = 0; (dev = devlist[idx]) != NULL; idx++) {
        uvc_theta_device_descriptor_t* desc;
        if (uvc_theta_get_device_descriptor(dev, &desc) != UVC_SUCCESS)
            continue;

        if ((desc->idProduct != USBPID_THETAV_UVC && desc->idProduct != USBPID_THETAZ1_UVC))
            continue;

        ids.push_back(idx);
        uvc_theta_free_device_descriptor(desc);
    }

    if (cameraIdConfig_->getRequestedValue() >= ids.size()) {
        LOGGER.info("Requested camera not found");
        return false;
    }

    device_ = devlist[ids[cameraIdConfig_->getRequestedValue()]];
    uvc_theta_ref_device(device_);
    uvc_theta_free_device_list(devlist, 1);
    uvc_theta_get_device_descriptor(device_, &descriptor_);

    return true;
}

} // namespace cameras
} // namespace sensors
} // namespace devices
} // namespace urf
