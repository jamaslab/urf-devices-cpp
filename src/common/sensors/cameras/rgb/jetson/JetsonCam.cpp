#define DEFAULT_V4L_BUFFERS 4

#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <vector>
#include <unistd.h>

#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

#include "urf/devices/sensors/cameras/rgb/jetson/JetsonCam.hpp"

#include <urf/common/logger/Logger.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("JetsonCam");
}

using namespace Argus;

namespace urf {
namespace devices {
namespace sensors {
namespace cameras {

JetsonCam::JetsonCam(uint8_t cameraId) :
    RgbCamera("JetsonCam-"+std::to_string(cameraId)),
    cameraId_(cameraId),
    requestedResolution_(1280, 720),
    requestedFps_(60),
    cameraProvider_(),
    captureSession_(),
    stream_(),
    frameConsumer_(),
    sourceSettings_(),
    cameraDevice_() {
        LOGGER.trace("CTor");

        cameraProvider_ = UniqueObj<CameraProvider>(CameraProvider::create(NULL));
        transitionCompleted();
}

JetsonCam::~JetsonCam() {
    LOGGER.trace("DTor");
    if ((state_ == ComponentStates::Enabled) || (state_ == ComponentStates::SwitchedOn))
        shutdown();
}

bool JetsonCam::resetFault() {
    LOGGER.trace("resetFault()");

    if (!startTransition(ComponentStateTransitions::ResetFault)) {
        LOGGER.warn("Can't reset fault in current state");
        return false;
    }

    transitionCompleted();
    return true;
}

bool JetsonCam::switchOn() {
    LOGGER.trace("switchOn()");

    if (!startTransition(ComponentStateTransitions::SwitchOn)) {
        LOGGER.warn("Can't switch on in current state");
        return false;
    }

    // get camera provider
    auto iCameraProvider = interface_cast<ICameraProvider>(cameraProvider_);
    if (!iCameraProvider) {
        LOGGER.error("Couldn't create CameraProvider");
        fault();
        return false;
    }

    // get camera device
    std::vector<CameraDevice*> devices;
    auto status = iCameraProvider->getCameraDevices(&devices);
    if (Argus::STATUS_OK != status) {
        LOGGER.error("Couldn't list available devices");
        fault();
        return false;
    }

    if (cameraId_ >= devices.size()) {
        LOGGER.error("Camera {} not connected", cameraId_);
        fault();
        return false;
    }

    cameraDevice_ = devices[cameraId_];

    transitionCompleted();
    return true;
}

bool JetsonCam::shutdown() {
    LOGGER.trace("shutdown()");
    if ((state_ == ComponentStates::Enabled) && (!disable())) {
        return false;
    }

    if (!startTransition(ComponentStateTransitions::Shutdown)) {
        LOGGER.warn("Can't shutdown in current state");
        return false;
    }

    transitionCompleted();
    return true;
}

bool JetsonCam::disable() {
    LOGGER.trace("disable()");
    if (!startTransition(ComponentStateTransitions::Disable)) {
        LOGGER.warn("Can't disable in current state");
        return false;
    }

    auto iCaptureSession = interface_cast<ICaptureSession>(captureSession_);
    if (iCaptureSession) {
        iCaptureSession->stopRepeat();
        iCaptureSession->waitForIdle();
        iCaptureSession->cancelRequests();
    }

    auto iStream = interface_cast<IEGLOutputStream>(stream_);
    if (iStream) {
        iStream->disconnect();
    }

    captureSession_.reset();

    transitionCompleted();
    return true;
}

bool JetsonCam::enable() {
    LOGGER.trace("enable()");
    if ((state_ == ComponentStates::ReadyToSwitchOn) && (!switchOn())) {
        return false;
    }

    if (!startTransition(ComponentStateTransitions::Enable)) {
        LOGGER.warn("Can't enable in current state");
        return false;
    }

    // create capture session
    auto iCameraProvider = interface_cast<ICameraProvider>(cameraProvider_);
    if (!iCameraProvider) {
        LOGGER.error("Couldn't get ICameraProvider");
        fault();
        return false;
    }

    captureSession_ = UniqueObj<CaptureSession>(iCameraProvider->createCaptureSession(cameraDevice_));
    auto iCaptureSession = interface_cast<ICaptureSession>(captureSession_);
    if (!iCaptureSession) {
        LOGGER.error("Couldn't get ICaptureSession");
        fault();
        return false;
    }

    // create stream settings
    auto streamSettings = UniqueObj<OutputStreamSettings>(iCaptureSession->createOutputStreamSettings(STREAM_TYPE_EGL));
    auto iOutputStreamSettings = interface_cast<IEGLOutputStreamSettings>(streamSettings);
    if (!iOutputStreamSettings) {
        LOGGER.error("Couldn't get OutputStreamSettings");
        fault();
        return false;
    }

    // set stream pixel format and resolution
    iOutputStreamSettings->setPixelFormat(Argus::PIXEL_FMT_YCbCr_420_888);
    auto evenResolution = Argus::Size2D<uint32_t>(
        requestedResolution_.width,
        requestedResolution_.height
    );
    iOutputStreamSettings->setResolution(evenResolution);
    iOutputStreamSettings->setMetadataEnable(false);

    Argus::Status status;
    // create stream
    stream_ = UniqueObj<OutputStream>(iCaptureSession->createOutputStream(streamSettings.get(), &status));
    auto iStream = interface_cast<IEGLOutputStream>(stream_);
    if (!iStream) {
        fault();
        return false;
    }

    // create frame consumer
    frameConsumer_ = UniqueObj<EGLStream::FrameConsumer>(EGLStream::FrameConsumer::create(stream_.get()));
    auto iFrameConsumer = interface_cast<EGLStream::IFrameConsumer>(frameConsumer_);
    if (!iFrameConsumer) {
        fault();
        return false;
    }

    // start repeating capture request
    auto request = UniqueObj<Request>(iCaptureSession->createRequest());
    auto iRequest = interface_cast<IRequest>(request);
    if (!iRequest) {
        fault();
        return false;
    }

    // configure source settings in request
    // 1. set sensor mode
    auto iCameraProperties = interface_cast<ICameraProperties>(cameraDevice_);
    std::vector<SensorMode*> sensorModes;
    status = iCameraProperties->getAllSensorModes(&sensorModes);
    if (Argus::STATUS_OK != status ||
        0 >= sensorModes.size()) {
            fault();
            return false;
    }

    auto iAutoControlSettings = interface_cast<IAutoControlSettings>(iRequest->getAutoControlSettings());
    status = iAutoControlSettings->setAeLock(false);
    status = iAutoControlSettings->setAwbLock(false);

    sourceSettings_ = interface_cast<ISourceSettings>(iRequest->getSourceSettings());
    status = sourceSettings_->setSensorMode(sensorModes[0]);
    if (Argus::STATUS_OK != status) {
        fault();
        return false;
    }

    // 2. set frame duration
    status = sourceSettings_->setFrameDurationRange(Argus::Range<uint64_t>(
        1e9 / requestedFps_,
        1e9 / requestedFps_
    ));
    if (Argus::STATUS_OK != status) {
        fault();
        return false;
    }

    // configure stream settings
    auto iStreamSettings = interface_cast<IStreamSettings>(iRequest->getStreamSettings(stream_.get()));
    if (!iStreamSettings) {
        fault();
        return false;
    }
    // set stream resolution
    status = iStreamSettings->setSourceClipRect(Argus::Rectangle<float>(
        0.0, 0.0, 1.0, 1.0
    ));

    if (Argus::STATUS_OK != status) {
        fault();
        return false;
    }

    // enable output stream
    iRequest->enableOutputStream(stream_.get());

    // start repeating capture request
    status = iCaptureSession->repeat(request.get());
    if (Argus::STATUS_OK != status) {
        fault();
        return false;
    }

    // connect stream
    iStream->waitUntilConnected();

    transitionCompleted();
    return true;
}

bool JetsonCam::reconfigure() {
    LOGGER.trace("reconfigure()");
    if (!disable()) {
        return false;
    }

    if (!enable()) {
        return false;
    }

    return true;
}

bool JetsonCam::fault() {
    LOGGER.trace("fault()");
    failTransition();

    return true;
}

devices::ComponentStates JetsonCam::currentState() const {
    return state_;
}

devices::ComponentStateTransitions JetsonCam::currentTransition() const {
    return transition_;
}


cv::Mat JetsonCam::getFrame() {
    if (state_ != ComponentStates::Enabled) {
        LOGGER.warn("Device is not enabled");
        return cv::Mat();
    }

    auto iStream = interface_cast<IEGLOutputStream>(stream_);
    if (!iStream) {
        LOGGER.error("Failed to create stream interface");
        fault();
        return cv::Mat(); // failed to create stream interface
    }

    auto iFrameConsumer = interface_cast<EGLStream::IFrameConsumer>(frameConsumer_);
    if (!iFrameConsumer) {
        LOGGER.error("Failed to create frame consumer");
        fault();
        return cv::Mat(); // failed to create frame consumer
    }

    auto frame = UniqueObj<EGLStream::Frame>(iFrameConsumer->acquireFrame(5e9));
    auto iFrame = interface_cast<EGLStream::IFrame>(frame);
    if (!iFrame) {
        LOGGER.error("Failed to get frame");
        fault();
        return cv::Mat(); // failed to get frame
    }

    auto image = iFrame->getImage();

    // copy to native buffer converting resolution, color format, and layout
    int fd = -1;
    auto iNativeBuffer = interface_cast<EGLStream::NV::IImageNativeBuffer>(image);
    auto resolution = Argus::Size2D<uint32_t>(requestedResolution_.width, requestedResolution_.height);
    fd = iNativeBuffer->createNvBuffer(resolution,
        NvBufferColorFormat::NvBufferColorFormat_ARGB32,
        NvBufferLayout_Pitch);
    if (fd == -1) {
        LOGGER.error("Failed to create native buffer");
        fault();
        return cv::Mat(); // failed to create native buffer
    }

    NvBufferParams nativeBufferParams;
    NvBufferGetParams(fd, &nativeBufferParams);

    auto m_datamem = (char *)mmap(NULL, requestedResolution_.width*requestedResolution_.height*4, PROT_READ | PROT_WRITE, MAP_SHARED, fd, nativeBufferParams.offset[0]);

    if (m_datamem) {
        LOGGER.error("Failed to mmap");
        fault();
        return cv::Mat();
    }

    cv::Mat rgb(requestedResolution_.height, requestedResolution_.width, CV_8UC4, m_datamem);

    NvBufferDestroy(fd);
    return rgb;
}


bool JetsonCam::setResolution(const cv::Size& resolution) {
    requestedResolution_ = resolution;
    return true;
}

bool JetsonCam::setFramerate(int fps) {
    requestedFps_ = fps;
    return true;
}

bool JetsonCam::setZoom(float zoom) {
    return false;
}

bool JetsonCam::setPosition(float pan, float tilt) {
    return false;
}

bool JetsonCam::setExposure(float exposure) {
    return false;
}

bool JetsonCam::setShutterSpeed(float speed) {
    return false;
}

bool JetsonCam::enableAutofocus(bool autofocus) {
    return false;
}

bool JetsonCam::setFocus(float focus) {
    return false;
}

bool JetsonCam::setISO(int iso) {
    return false;
}


std::optional<cv::Size> JetsonCam::getResolution() {
    return requestedResolution_;
}

std::optional<int> JetsonCam::getFramerate() {
    return requestedFps_;
}

std::optional<float> JetsonCam::getZoom() {
    return std::nullopt;
}

std::optional<float> JetsonCam::getPan() {
    return std::nullopt;
}

std::optional<float> JetsonCam::getTilt() {
    return std::nullopt;
}

std::optional<float> JetsonCam::getExposure() {
    return std::nullopt;
}

std::optional<float> JetsonCam::getShutterSpeed() {
    return std::nullopt;
}

std::optional<bool> JetsonCam::autofocusEnabled() {
    return std::nullopt;
}

std::optional<float> JetsonCam::getFocus() {
    return std::nullopt;
}

std::optional<int> JetsonCam::getISO() {
    return std::nullopt;
}

std::vector<cv::Size> JetsonCam::getAvailableResolutions() {

}

}  // namespace cameras
}  // namespace sensors
}  // namespace devices
}  // namespace urf
