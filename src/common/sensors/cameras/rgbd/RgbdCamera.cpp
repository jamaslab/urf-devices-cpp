#include "urf/devices/sensors/cameras/rgbd/RgbdCamera.hpp"

namespace urf {
namespace devices {
namespace sensors {
namespace cameras {

RgbdCamera::RgbdCamera(const std::string& deviceName) :
    Device(deviceName, {"RgbdCamera"}),
    configuration_(new common::properties::PropertyNode) {
        settings_.insert("configuration", configuration_);
    }

}  // namespace cameras
}  // namespace sensors
}  // namespace devices
}  // namespace urf