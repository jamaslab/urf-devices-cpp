#include "urf/devices/sensors/cameras/rgbd/RemoteRgbdCamera.hpp"

#include <urf/common/logger/Logger.hpp>

#include <common/communication/CommandHandler.hpp>
#include <common/communication/MessageFactory.hpp>
#include <common/communication/SettingsFactory.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("RemoteRgbdCamera");
}

namespace urf {
namespace devices {
namespace sensors {
namespace cameras {

using namespace urf::common::properties;

RemoteRgbdCamera::RemoteRgbdCamera(uint8_t userId, const std::string& client)
    : Device("RemoteRgbdCamera", {"RgbdCamera"})
    , RgbdCamera("RemoteRgbdCamera")
    , client_(new middleware::sockets::Client(client))
    , userId_(userId)
    , timeout_(std::chrono::seconds(5))
    , settings_()
    , frameCv_()
    , frameMtx_()
    , frameSetFlag_(false)
    , latestFrame_() {
        client_->onUpdate("component", [this](auto, auto msg) {
            settings_.find("component")->second.find("deviceState")->second->from_json(msg["deviceState"]);
        });

        client_->onUpdate("camera_settings", [this](auto, auto msg) {
            auto camera_settings = settings_.find("camera_settings")->second;
            for (auto& item : msg.body().data().items()) {
                if (camera_settings.count(item.key()) == 0) {
                    LOGGER.warn("Received unknown setting");
                } else {
                    camera_settings.find(item.key())->second->from_json(item.value());
                }
            }
        });

        common::components::SettingsGroup remoteSettingsGroup("remote_settings");
        settings_.insert({remoteSettingsGroup.name(), remoteSettingsGroup});
}

bool RemoteRgbdCamera::open() {
    if (!client_->open()) {
        return false;
    }

    if (!client_->subscribe("*")) {
        return false;
    }

    auto response = client_->request(communication::MessageFactory::getSetting(userId_, "ALL"), timeout_);
    if (response.empty()) {
        LOGGER.warn("Could not retrieve settings");
        client_->close();
        return false;
    }

    if (!response[0]["retval"].get<bool>()) {
        LOGGER.warn("Failed to get settings: {}", response[0]["error"].get<std::string>());
        client_->close();
        return false;
    }

    for (auto const& group : response[0].body().data().items()) {
        if (group.value().is_null())
            continue;

        if (group.key() == "retval")
            continue;

        auto sg = communication::SettingsFactory::settingsGroup(group.key(), group.value());
        settings_.insert({sg.name(), sg});
    }

    return true;
}

bool RemoteRgbdCamera::close() {
    return client_->close();
}

bool RemoteRgbdCamera::acquire(accesscontrol::Roles role) {
    try {
        communication::CommandHandler::sendCommand(
            communication::MessageFactory::acquire(userId_, role),
            client_,
            timeout_
        );
    } catch (const std::exception& ex) {
        LOGGER.warn("{}", ex.what());
        return false;
    }
    return true;
}

bool RemoteRgbdCamera::release() {
    try {
        communication::CommandHandler::sendCommand(
            communication::MessageFactory::release(userId_),
            client_,
            timeout_
        );
    } catch (const std::exception& ex) {
        LOGGER.warn("{}", ex.what());
        return false;
    }
    return true;
}

void RemoteRgbdCamera::setTimeout(const std::chrono::milliseconds& timeout) {
    timeout_ = timeout;
}

std::chrono::milliseconds RemoteRgbdCamera::getTimeout() {
    return timeout_;
}

RgbdFrame RemoteRgbdCamera::getFrame() {
    std::unique_lock lock(frameMtx_);
    frameSetFlag_ = false;
    if (!frameCv_.wait_for(lock, getTimeout(), [this]() { return frameSetFlag_; })) {
        LOGGER.warn("Timeout while waiting for frames");
        return RgbdFrame();
    }
    return latestFrame_;
}

ComponentStates RemoteRgbdCamera::currentState() const {
    if (!client_->isOpen()) {
        return ComponentStates::Init;
    }

    return std::dynamic_pointer_cast<ObservableProperty<common::components::StatePair>>(
               settings_.find("component")->second.find("state")->second)
        ->getValue()
        .first;
}

ComponentStateTransitions RemoteRgbdCamera::currentTransition() const {
    if (!client_->isOpen()) {
        return ComponentStateTransitions::Init;
    }

    return std::dynamic_pointer_cast<ObservableProperty<common::components::StatePair>>(
               settings_.find("component")->second.find("state")->second)
        ->getValue()
        .second;
}

std::string RemoteRgbdCamera::componentName() const {
    if (!client_->isOpen()) {
        return "";
    }

    return std::dynamic_pointer_cast<ObservableProperty<nlohmann::json>>(
               settings_.find("component")->second.find("name")->second)
        ->getValue()
        .get<std::string>();
}

std::vector<std::string> RemoteRgbdCamera::componentClass() const {
    if (!client_->isOpen()) {
        return {""};
    }

    return std::dynamic_pointer_cast<ObservableProperty<nlohmann::json>>(
               settings_.find("component")->second.find("class")->second)
        ->getValue()
        .get<std::vector<std::string>>();
}

std::unordered_map<std::string, common::components::SettingsGroup> RemoteRgbdCamera::settings() {
    return settings_;
}

bool RemoteRgbdCamera::resetFaultImpl() {
    try {
        communication::CommandHandler::sendCommand(
            communication::MessageFactory::changeState(userId_, common::components::ComponentStateTransitions::ResetFault),
            client_,
            timeout_
        );
    } catch (const std::exception& ex) {
        LOGGER.warn("{}", ex.what());
        return false;
    }
    return true;
}

bool RemoteRgbdCamera::switchOnImpl()  {
    try {
        communication::CommandHandler::sendCommand(
            communication::MessageFactory::changeState(userId_, common::components::ComponentStateTransitions::SwitchOn),
            client_,
            timeout_
        );
    } catch (const std::exception& ex) {
        LOGGER.warn("{}", ex.what());
        return false;
    }
    return updateCameraSettings();
}

bool RemoteRgbdCamera::shutdownImpl()  {
    try {
        communication::CommandHandler::sendCommand(
            communication::MessageFactory::changeState(userId_, common::components::ComponentStateTransitions::Shutdown),
            client_,
            timeout_
        );
    } catch (const std::exception& ex) {
        LOGGER.warn("{}", ex.what());
        return false;
    }
    return true;
}

bool RemoteRgbdCamera::disableImpl()  {
    try {
        communication::CommandHandler::sendCommand(
            communication::MessageFactory::changeState(userId_, common::components::ComponentStateTransitions::Disable),
            client_,
            timeout_
        );
    } catch (const std::exception& ex) {
        LOGGER.warn("{}", ex.what());
        return false;
    }

    return true;
}

bool RemoteRgbdCamera::enableImpl()  {
    try {
        communication::CommandHandler::sendCommand(
            communication::MessageFactory::changeState(userId_, common::components::ComponentStateTransitions::Enable),
            client_,
            timeout_
        );
    } catch (const std::exception& ex) {
        LOGGER.warn("{}", ex.what());
        return false;
    }

    return updateCameraSettings();
}

bool RemoteRgbdCamera::faultImpl() {
    middleware::messages::Message request;
    request.header().writerId(userId_);
    request["cmd"] = "fault";

    try {
        communication::CommandHandler::sendCommand(
            request,
            client_,
            timeout_
        );
    } catch (const std::exception& ex) {
        LOGGER.warn("{}", ex.what());
        return false;
    }
    return true;
}

bool RemoteRgbdCamera::updateCameraSettings() {
    auto response = client_->request(communication::MessageFactory::getSetting(userId_, "camera_settings"), timeout_);
    if (response.empty()) {
        LOGGER.warn("Could not retrieve settings");
        client_->close();
        return false;
    }

    if (!response[0]["retval"].get<bool>()) {
        LOGGER.warn("Failed to get settings: {}", response[0]["error"].get<std::string>());
        client_->close();
        return false;
    }

    auto cameraSg = settings_.find("camera_settings")->second;

    for (auto const& variable : response[0].body().data().items()) {
        if (variable.value().is_null())
            continue;

        if (variable.key() == "retval")
            continue;

        if (cameraSg.count(variable.key()) != 0) {
            cameraSg.find(variable.key())->second->from_json(variable.value());
        } else {
            auto setting = communication::SettingsFactory::setting(variable.value());
            cameraSg.insert({variable.key(), setting});
        }
    }

    return true;
}

}
}
}
}