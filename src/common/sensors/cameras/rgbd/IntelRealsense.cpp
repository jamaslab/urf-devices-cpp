#include <urf/devices/sensors/cameras/rgbd/IntelRealsense.hpp>

#include <urf/common/logger/Logger.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("IntelRealsense");
constexpr unsigned int FRAMESET_TIMEOUT_MS = 3000;

Eigen::MatrixXf rs2ToEigenExtrinsics(const rs2_extrinsics& extrinsics) {
    Eigen::MatrixXf matrix(4,4);
    matrix(0, 0) = extrinsics.rotation[0];
    matrix(1, 0) = extrinsics.rotation[1];
    matrix(2, 0) = extrinsics.rotation[2];
    matrix(3, 0) = 0;

    matrix(0, 1) = extrinsics.rotation[3];
    matrix(1, 1) = extrinsics.rotation[4];
    matrix(2, 1) = extrinsics.rotation[5];
    matrix(3, 1) = 0;

    matrix(0, 2) = extrinsics.rotation[6];
    matrix(1, 2) = extrinsics.rotation[7];
    matrix(2, 2) = extrinsics.rotation[8];
    matrix(3, 2) = 0;

    matrix(0, 3) = extrinsics.translation[0];
    matrix(1, 3) = extrinsics.translation[1];
    matrix(2, 3) = extrinsics.translation[2];
    matrix(3, 3) = 1;

    return matrix;
}

std::tuple<int, int, int> rgbTexture(const rs2::video_frame& texture, const rs2::texture_coordinate& Texture_XY)
{
    // Get Width and Height coordinates of texture
    int width  = texture.get_width();  // Frame width in pixels
    int height = texture.get_height(); // Frame height in pixels

    // Normals to Texture Coordinates conversion
    int x_value = std::min(std::max(int(Texture_XY.u * width  + .5f), 0), width - 1);
    int y_value = std::min(std::max(int(Texture_XY.v * height + .5f), 0), height - 1);

    int bytes = x_value * texture.get_bytes_per_pixel();   // Get # of bytes per pixel
    int strides = y_value * texture.get_stride_in_bytes(); // Get line width in bytes
    int Text_Index =  (bytes + strides);

    const auto New_Texture = reinterpret_cast<const uint8_t*>(texture.get_data());

    // RGB components to save in tuple
    int NT1 = New_Texture[Text_Index];
    int NT2 = New_Texture[Text_Index + 1];
    int NT3 = New_Texture[Text_Index + 2];

    return std::tuple<int, int, int>(NT1, NT2, NT3);
}

} // namespace

namespace urf {
namespace devices {
namespace sensors {
namespace cameras {

using namespace urf::common::properties;

std::unique_ptr<rs2::context> IntelRealsense::rsCtx_;
size_t IntelRealsense::ctxRefCount_ = 0;

IntelRealsense::IntelRealsense(int index)
    : Device("IntelRealsense", {"RgbdCamera"})
    , RgbdCamera("IntelRealsense")
    , cameraIdConfig_(new ObservableSetting<uint8_t>())
    , serialNumber_(new ObservableProperty<std::string>())
    , cameraModel_(new ObservableProperty<std::string>())
    , colorResolution_(new ObservableSettingList<StreamProfile>)
    , depthResolution_(new ObservableSettingList<StreamProfile>)
    , depthScale_(new ObservableProperty<float>())
    , colorActive_(new ObservableSetting<bool>())
    , depthActive_(new ObservableSetting<bool>())
    , pointCloudActive_(new ObservableSetting<bool>())
    , alignDepth_(new ObservableSetting<bool>())
    , extrinsics_(new ObservableProperty<Eigen::MatrixXf>)
    , device_()
    , pipe_()
    , colorSensor_()
    , depthSensor_()
    , alignToColor_(RS2_STREAM_COLOR)
    , pipeMtx_() {
    if (ctxRefCount_ == 0) {
        rsCtx_.reset(new rs2::context);
        ctxRefCount_++;
    }

    cameraIdConfig_->setRequestedValue(index);
    colorActive_->setRequestedValue(true);
    depthActive_->setRequestedValue(false);
    pointCloudActive_->setRequestedValue(false);

    configuration_->insert("camera_id", cameraIdConfig_);
    configuration_->insert("serial_number", serialNumber_);
    configuration_->insert("camera_model", cameraModel_);
    configuration_->insert("color_resolution", colorResolution_);
    configuration_->insert("depth_resolution", depthResolution_);
    configuration_->insert("depth_scale", depthScale_);
    configuration_->insert("extrinsics", extrinsics_);

    configuration_->insert("color_active", colorActive_);
    configuration_->insert("depth_active", depthActive_);
    configuration_->insert("pointcloud_active", pointCloudActive_);
    configuration_->insert("align_depth", alignDepth_);

    extrinsics_->setValue(Eigen::MatrixXf(4,4));
}

IntelRealsense::~IntelRealsense() {
    ctxRefCount_--;
    if (ctxRefCount_ == 0) {
        rsCtx_.reset(nullptr);
    }
}

RgbdFrame IntelRealsense::getFrame() {
    if (currentState() != ComponentStates::Enabled) {
        LOGGER.warn("Device is not in enabled state");
        return RgbdFrame::empty();
    }

    std::scoped_lock lock(pipeMtx_);
    rs2::frameset frameset;
    if (!pipe_.try_wait_for_frames(&frameset, FRAMESET_TIMEOUT_MS)) {
        LOGGER.warn("Timeout waiting for frames");
        fault();
        return RgbdFrame::empty();
    }

    RgbdFrame frame;
    if (colorActive_->getValue() && depthActive_->getValue() && alignDepth_->getValue()) {
        frameset = alignToColor_.process(frameset);
    }

    auto colorFrame = frameset.get_color_frame();
    auto depthFrame = frameset.get_depth_frame();

    if (colorActive_->getValue() && colorFrame) {
        frame.colorFrame = algorithms::compression::VideoFrame(colorFrame.get_width(), colorFrame.get_height(), "bgr24");
        std::memcpy(const_cast<uint8_t*>(frame.colorFrame.value().data(0)), colorFrame.get_data(), colorFrame.get_width()*colorFrame.get_height());
    }

    if (depthActive_->getValue() && depthFrame) {
        frame.depthFrame = algorithms::compression::VideoFrame(depthFrame.get_width(), colorFrame.get_height(), "gray16");
        std::memcpy(const_cast<uint8_t*>(frame.depthFrame.value().data(0)), depthFrame.get_data(), colorFrame.get_width()*colorFrame.get_height()*2);
    }

    if (pointCloudActive_->getValue() && depthFrame && colorFrame) {
        rs2::pointcloud pc;
        rs2::points points;

        pc.map_to(colorFrame);
        points = pc.calculate(depthFrame);

        frame.pointCloud = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
        frame.pointCloud.value()->width = colorFrame.get_width();
        frame.pointCloud.value()->height = colorFrame.get_height();
        frame.pointCloud.value()->is_dense = true;
        frame.pointCloud.value()->points.reserve(points.size());

        auto textureCoords = points.get_texture_coordinates();
        auto vertices = points.get_vertices();
        #pragma omp parallel
        #pragma omp for
        for (size_t i = 0; i < points.size(); i++) {
            if ((vertices[i].x == 0) && (vertices[i].y == 0) && (vertices[i].z == 0)) {
                continue;
            }
            auto rgbColor = rgbTexture(colorFrame, textureCoords[i]);
            frame.pointCloud.value()->emplace_back(pcl::PointXYZRGB(
                vertices[i].x, vertices[i].y, vertices[i].z,
                std::get<2>(rgbColor), std::get<1>(rgbColor), std::get<0>(rgbColor)));
        }
    }

    return frame;
}

bool IntelRealsense::resetFaultImpl() {
    try {
        device_.hardware_reset();
    } catch (...) { }
    return true;
}

bool IntelRealsense::switchOnImpl() {
    auto devices = rsCtx_->query_devices();

    if (devices.size() <= cameraIdConfig_->getRequestedValue()) {
        LOGGER.warn("Could not find camera {}", cameraIdConfig_->getRequestedValue());
        return false;
    }

    device_ = devices[cameraIdConfig_->getRequestedValue()];

    cameraIdConfig_->setValue(cameraIdConfig_->getRequestedValue());
    serialNumber_->setValue(device_.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER));
    cameraModel_->setValue(device_.get_info(RS2_CAMERA_INFO_NAME));

    LOGGER.info("Correclty opened camera {} (Model: {}, SN: {})",
                cameraIdConfig_->getValue(),
                cameraModel_->getValue(),
                serialNumber_->getValue());

    auto sensors = device_.query_sensors();
    for (auto const& sensor : sensors) {
        for (auto const& streamProfile : sensor.get_stream_profiles()) {
            if (sensor.is<rs2::color_sensor>() &&
                (streamProfile.stream_type() == rs2_stream::RS2_STREAM_COLOR)) {
                auto vsp = streamProfile.as<rs2::video_stream_profile>();
                auto settingList = colorResolution_->getList();
                StreamProfile profile({static_cast<uint32_t>(vsp.width()), static_cast<uint32_t>(vsp.height())}, vsp.fps());
                if (std::find(settingList.begin(), settingList.end(), profile) == settingList.end()) {
                    settingList.push_back(profile);
                    colorResolution_->setList(settingList);
                }
            } else if (sensor.is<rs2::depth_sensor>() &&
                       (streamProfile.stream_type() == rs2_stream::RS2_STREAM_DEPTH)) {
                auto vsp = streamProfile.as<rs2::video_stream_profile>();
                auto settingList = depthResolution_->getList();
                StreamProfile profile({static_cast<uint32_t>(vsp.width()), static_cast<uint32_t>(vsp.height())}, vsp.fps());
                if (std::find(settingList.begin(), settingList.end(), profile) == settingList.end()) {
                    settingList.push_back(profile);
                    depthResolution_->setList(settingList);
                }
            }
        }

        if (sensor.is<rs2::depth_sensor>()) {
            depthScale_->setValue(sensor.as<rs2::depth_sensor>().get_depth_scale());
        }
    }

    // Just initializing the resolutions if not already done
    if (colorResolution_->getRequestedValue().resolution.empty()) {
    	colorResolution_->setRequestedValue(colorResolution_->getList()[0]);
    }

    if (depthResolution_->getRequestedValue().resolution.empty()) {
    	depthResolution_->setRequestedValue(depthResolution_->getList()[0]);
    }

    return true;
}

bool IntelRealsense::shutdownImpl() {
    device_ = rs2::device();

    return true;
}

bool IntelRealsense::disableImpl() {
    std::scoped_lock lock(pipeMtx_);
    pipe_.stop();

    return true;
}

bool IntelRealsense::enableImpl() {
    rs2::config pipelineConfig;

    auto colorStream = colorResolution_->getRequestedValue();
    auto depthStream = depthResolution_->getRequestedValue();

    pipelineConfig.enable_device(serialNumber_->getValue());

    if (colorActive_->getRequestedValue() || pointCloudActive_->getRequestedValue()) {
        LOGGER.info("Setting color resolution: {}x{}@{}", colorStream.resolution.width, colorStream.resolution.height, colorStream.fps);
        pipelineConfig.enable_stream(rs2_stream::RS2_STREAM_COLOR,
                                     colorStream.resolution.width,
                                     colorStream.resolution.height,
                                     rs2_format::RS2_FORMAT_BGR8,
                                     colorStream.fps);
    }

    if (depthActive_->getRequestedValue() || pointCloudActive_->getRequestedValue()) {
        LOGGER.info("Setting depth resolution: {}x{}@{}", depthStream.resolution.width, depthStream.resolution.height, depthStream.fps);
        pipelineConfig.enable_stream(rs2_stream::RS2_STREAM_DEPTH,
                                     depthStream.resolution.width,
                                     depthStream.resolution.height,
                                     rs2_format::RS2_FORMAT_Z16,
                                     depthStream.fps);
    }

    std::scoped_lock lock(pipeMtx_);
    auto selection = pipe_.start(pipelineConfig);
    if (!selection) {
        LOGGER.warn("Could not start pipeline");
        return false;
    }

    auto frameset = std::make_unique<rs2::frameset>();
    // Some sensors take quite some time to start.
    // We need to wait a lot of time before receiving the first frame
    if (!pipe_.try_wait_for_frames(frameset.get(), 10*1000)) {
        LOGGER.warn("Timeout waiting for frames");
        pipe_.stop();
        return false;
    }

    colorActive_->setValue(colorActive_->getRequestedValue());
    depthActive_->setValue(depthActive_->getRequestedValue());
    pointCloudActive_->setValue(pointCloudActive_->getRequestedValue());

    if (colorActive_->getValue() || pointCloudActive_->getValue()) {
        auto stream = selection.get_stream(RS2_STREAM_COLOR).as<rs2::video_stream_profile>();
        auto i = stream.get_intrinsics();
        colorStream.principalPoint = std::make_pair(i.ppx, i.ppy);
        colorStream.focalPoint = std::make_pair(i.fx, i.fy);
        colorResolution_->setValue(colorStream);
    }

    if (depthActive_->getValue() || pointCloudActive_->getValue()) {
        auto stream = selection.get_stream(RS2_STREAM_DEPTH).as<rs2::video_stream_profile>();
        auto i = stream.get_intrinsics();
        depthStream.principalPoint = std::make_pair(i.ppx, i.ppy);
        depthStream.focalPoint = std::make_pair(i.fx, i.fy);
        depthResolution_->setValue(depthStream);
    }

    if ((colorActive_->getValue() && depthActive_->getValue()) || pointCloudActive_->getValue()) {
        auto color_stream = selection.get_stream(RS2_STREAM_COLOR).as<rs2::video_stream_profile>();
        auto depth_stream = selection.get_stream(RS2_STREAM_DEPTH).as<rs2::video_stream_profile>();
        auto e = depth_stream.get_extrinsics_to(color_stream);

        extrinsics_->setValue(rs2ToEigenExtrinsics(e));

        alignDepth_->setValue(alignDepth_->getRequestedValue());
    } else {
        alignDepth_->setValue(false);
    }

    return true;
}

bool IntelRealsense::faultImpl() {
    std::scoped_lock lock(pipeMtx_);
    try {
    // stop throws exception if pipe was not started
        pipe_.stop();
    } catch (...) { }
    return true;
}

} // namespace cameras
} // namespace sensors
} // namespace devices
} // namespace urf
