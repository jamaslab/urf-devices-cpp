#include "urf/devices/sensors/imu/RemoteIMU.hpp"

#include <iostream>

namespace urf {
namespace devices {
namespace sensors {
namespace imu {

using namespace urf::common::properties;

RemoteIMU::RemoteIMU(uint8_t userId, const std::string& client)
    : RemoteDevice(userId, client)
    , measurementsUpdateFrequency_(0) {

    client_->onUpdate("measurements", [this](auto, auto msg) {
        if (settings_.has("measurements")) {
            return;
        }

        for (auto const& value : msg.body().data().items()) {
            try {
                settings_["measurements"]->at(value.key())->from_json(value.value());
            } catch (const std::exception&) {
                continue;
            }
        }

        std::dynamic_pointer_cast<ObservableProperty<uint64_t>>(
            settings_["measurements"]["timestamp"])
            ->setValue(msg.header().timestamp());
    });

    client_->onUpdate("configuration", [this](auto, auto msg) {
        if (settings_.has("configuration") == 0) {
            return;
        }

        for (auto const& value : msg.body().data().items()) {
            try {
                settings_["configuration"]->at(value.key())->from_json(value.value());
            } catch (const std::exception&) {
                continue;
            }
        }
    });
}

bool RemoteIMU::subscribeToTopics() {
    if (!client_->subscribe("measurements", measurementsUpdateFrequency_)) {
        return false;
    }

    if (!client_->subscribe("configuration")) {
        return false;
    }

    auto measTimestampProp = std::make_shared<common::properties::ObservableProperty<uint64_t>>();
    settings_["measurements"].insert({"timestamp", measTimestampProp});

    return true;
}

void RemoteIMU::measurementsUpdateFrequency(const std::chrono::milliseconds& freq) {
    measurementsUpdateFrequency_ = freq;

    if (client_->isOpen()) {
        client_->subscribe("measurements", measurementsUpdateFrequency_);
    }
}

std::chrono::milliseconds RemoteIMU::measurementsUpdateFrequency() const {
    return measurementsUpdateFrequency_;
}

} // namespace imu
} // namespace sensors
} // namespace devices
} // namespace urf
