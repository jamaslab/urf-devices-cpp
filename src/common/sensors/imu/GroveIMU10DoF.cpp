#include <limits>
#include <math.h>

#include "urf/devices/sensors/imu/GroveIMU10DoF.hpp"

#include <urf/common/logger/Logger.hpp>

#define ACC_GYRO_BASE_ADDR 0x3B
#define ACC_GYRO_DATA_SIZE 14
#define MAG_BASE_ADDR 0x03
#define MAG_DATA_SIZE 6

#define MPU9250_RA_PWR_MGMT_1 0x6B
#define MPU9250_PWR1_TEMP_DIS_BIT 3

namespace {
auto LOGGER = urf::common::getLoggerInstance("GroveIMU10DoF");
}

namespace urf {
namespace devices {
namespace sensors {
namespace imu {

GroveIMU10DoF::GroveIMU10DoF(std::shared_ptr<communication::I2CBus> i2cBus)
    : Device("GroveIMU10DoF", {"IMU"})
    , IMU("GroveIMU10DoF")
    , i2cBus_(i2cBus)
    , device_(0x68)
    , magnetomerDev_(0x0C)
    , filter_()
    , accConfig_()
    , gyroConfig_()
    , filterBeta_()
    , accelerations_(new common::properties::ObservableProperty<std::vector<float>>)
    , gyroscopes_(new common::properties::ObservableProperty<std::vector<float>>)
    , magnetometers_(new common::properties::ObservableProperty<std::vector<float>>)
    , attitude_(new common::properties::ObservableProperty<std::vector<float>>)
    , temperature_(new common::properties::ObservableProperty<float>)
    , accFactor_(1 / 16384.0f)
    , gyroFactor_(250 * 0.0174533 / 32768.0f)
    , magFactor_(1200 / 4096.0f)
    , stopDataGrabber_(false)
    , dataGrabberThread_() {
    accConfig_ = std::make_shared<common::properties::ObservableSettingList<float>>(
        std::vector<float>{2, 4, 8, 16});
    accConfig_->setRequestedValue(2);
    configuration_->insert("accelerometerRange", accConfig_);

    gyroConfig_ = std::make_shared<common::properties::ObservableSettingList<float>>(
        std::vector<float>{250, 500, 1000, 2000});
    gyroConfig_->setRequestedValue(250);
    configuration_->insert("gyroscopeRange", gyroConfig_);

    filterBeta_ = std::make_shared<common::properties::ObservableSettingRanged<float>>();
    filterBeta_->setRange({0.0f, 1.0f});
    filterBeta_->setRequestedValue(0.1f);
    configuration_->insert("filterBtea", filterBeta_);


    accelerations_->setValue({.0f, .0f, .0f});
    gyroscopes_->setValue({.0f, .0f, .0f});
    magnetometers_->setValue({.0f, .0f, .0f});
    attitude_->setValue({.0f, .0f, .0f});
    temperature_->setValue(.0f);

    measurements_->insert("a", accelerations_);
    measurements_->insert("g", gyroscopes_);
    measurements_->insert("m", magnetometers_);
    measurements_->insert("rpy", attitude_);
    measurements_->insert("T", temperature_);
}

GroveIMU10DoF::~GroveIMU10DoF() {
    shutdown();
}

bool GroveIMU10DoF::resetFaultImpl() {
    stopDataGrabber_ = true;
    if (dataGrabberThread_.joinable())
        dataGrabberThread_.join();

    return true;
}

bool GroveIMU10DoF::switchOnImpl() {
    if (!i2cBus_->isOpen()) {
        LOGGER.warn("I2C bus is not open");
        return false;
    }

    auto buf = i2cBus_->read(device_, ACC_GYRO_BASE_ADDR, ACC_GYRO_DATA_SIZE);
    if (buf.size() != ACC_GYRO_DATA_SIZE) {
        LOGGER.warn("Could not communicate with device");
        return false;
    }

    return true;
}

bool GroveIMU10DoF::shutdownImpl() {
    return true;
}

bool GroveIMU10DoF::disableImpl() {
    stopDataGrabber_ = true;
    dataGrabberThread_.join();

    return true;
}

bool GroveIMU10DoF::enableImpl() {
    // Set Gyro range
    // Set Acc range
    
    filter_.reset(new algorithms::filters::MadgwickFilter(100, 0.5f));

    if (!i2cBus_->write(device_, 0x37, {0x02})) {
        LOGGER.warn("Could not set magnetometer bypass pin");
        fault();
        return false;
    }

    if (!calibrateMagnetometer()) {
        LOGGER.warn("Could not calibrate the magnetometer");
        fault();
        return false;
    }


    stopDataGrabber_ = true;
    dataGrabberThread_ = std::thread(&GroveIMU10DoF::dataGrabber, this);

    // It waits for the thread to actually start
    while (stopDataGrabber_) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    return true;
}

bool GroveIMU10DoF::faultImpl() {
    return true;
}

void GroveIMU10DoF::dataGrabber() {
    stopDataGrabber_ = false;

    auto bufToFloat = [](const std::vector<uint8_t>& buf, int i) {
        return static_cast<float>(static_cast<int16_t>(((((int16_t)buf[i]) << 8) | buf[i + 1])));
    };

    auto magBufToFloat = [](const std::vector<uint8_t>& buf, int i) {
        return static_cast<float>(static_cast<int16_t>(((((int16_t)buf[i + 1]) << 8) | buf[i])));
    };

    auto time = std::chrono::high_resolution_clock::now();
    while (!stopDataGrabber_) {
        if (!i2cBus_->isOpen()) {
            LOGGER.warn("I2C bus is not open");
            fault();
            return;
        }

        auto buf = i2cBus_->read(device_, ACC_GYRO_BASE_ADDR, ACC_GYRO_DATA_SIZE);
        if (buf.size() != ACC_GYRO_DATA_SIZE) {
            LOGGER.warn("Could not get measurement from device");
            fault();
            return;
        }

        auto magBuf = i2cBus_->read(magnetomerDev_, MAG_BASE_ADDR, MAG_DATA_SIZE);
        if (magBuf.size() != MAG_DATA_SIZE) {
            LOGGER.warn("Could not get magnetometer measurement");
            fault();
            return;
        }

        accelerations_->setValue({bufToFloat(buf, 0) * accFactor_,
                                  bufToFloat(buf, 2) * accFactor_,
                                  bufToFloat(buf, 4) * accFactor_});
        gyroscopes_->setValue({bufToFloat(buf, 8) * gyroFactor_,
                               bufToFloat(buf, 10) * gyroFactor_,
                               bufToFloat(buf, 12) * gyroFactor_});
        temperature_->setValue(bufToFloat(buf, 6) / 333.87f + 21.0f);
        magnetometers_->setValue({magBufToFloat(magBuf, 0) * magFactor_ - magCenter_[0],
                                  magBufToFloat(magBuf, 2) * magFactor_ - magCenter_[1],
                                  magBufToFloat(magBuf, 4) * magFactor_ - magCenter_[2]});
        Eigen::Vector3f accEig, gyrEig, magEig;
        for (int i=0; i < 3; i++) {
            accEig(i) = accelerations_->getValue()[i];
            gyrEig(i) = gyroscopes_->getValue()[i];
            magEig(i) = magnetometers_->getValue()[i];
        }
        filter_->update(accEig, gyrEig, magEig);

        attitude_->setValue({ filter_->angles()(0), filter_->angles()(1), filter_->angles()(2) });

        // We pre-request the magnetometer measurement
        if (!i2cBus_->write(magnetomerDev_, 0x0A, {0x01})) {
            LOGGER.warn("Could not enable the magnetometer");
            fault();
            return;
        }

        time += std::chrono::milliseconds(10);
        std::this_thread::sleep_until(time);
    }
}

bool GroveIMU10DoF::calibrateMagnetometer() {
    auto magBufToFloat = [](const std::vector<uint8_t>& buf, int i) {
        return static_cast<float>(static_cast<int16_t>(((((int16_t)buf[i + 1]) << 8) | buf[i])));
    };

    std::array<float, 3> mMax = {std::numeric_limits<float>::min(),
                                 std::numeric_limits<float>::min(),
                                 std::numeric_limits<float>::min()},
                         mMin = {std::numeric_limits<float>::max(),
                                 std::numeric_limits<float>::max(),
                                 std::numeric_limits<float>::max()},
                         m;

    for (int i = 0; i < 100; i++) {
        if (!i2cBus_->write(magnetomerDev_, 0x0A, {0x01})) {
            LOGGER.warn("Could not enable the magnetometer");
            return false;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(5));

        auto magBuf = i2cBus_->read(magnetomerDev_, MAG_BASE_ADDR, MAG_DATA_SIZE);
        if (magBuf.size() != MAG_DATA_SIZE) {
            LOGGER.warn("Could not get magnetometer measurement");
            return false;
        }

        m[0] = magBufToFloat(magBuf, 0) * magFactor_;
        m[1] = magBufToFloat(magBuf, 2) * magFactor_;
        m[2] = magBufToFloat(magBuf, 4) * magFactor_;

        for (int k = 0; k < 3; k++) {
            mMin[k] = std::min(mMin[k], m[k]);
            mMax[k] = std::max(mMax[k], m[k]);
        }
    }

    for (int k = 0; k < 3; k++) {
        magCenter_[k] = (mMax[k] - mMin[k]) / 2;
    }

    return true;
}

} // namespace imu
} // namespace sensors
} // namespace devices
} // namespace urf
