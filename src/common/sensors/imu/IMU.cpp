#include "urf/devices/sensors/imu/IMU.hpp"

namespace urf {
namespace devices {
namespace sensors {
namespace imu {

IMU::IMU(const std::string& deviceName)
    : Device(deviceName, {"IMU"})
    , measurements_(new common::properties::PropertyNode)
    , configuration_(new common::properties::PropertyNode) {
        settings_.insert("measurements", measurements_);
        settings_.insert("configuration", configuration_);
    }

} // namespace cameras
} // namespace sensors
} // namespace devices
} // namespace urf
