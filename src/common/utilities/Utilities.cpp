#include "common/utilities/Utilities.hpp"

namespace urf {
namespace devices {
namespace utilities {

std::string roleToString(accesscontrol::Roles role) {
    switch (role) {
        case accesscontrol::Roles::VIEWER:
            return "viewer";
        case accesscontrol::Roles::USER:
            return "user";
        case accesscontrol::Roles::EXPERT:
            return "expert";
        case accesscontrol::Roles::SUDO:
            return "sudo";
    }

    return "unknown";
}

}  // namespace utilities
}  // namespace devices
}  // namespace urf
