#pragma once

#include <string>

#include "urf/devices/Device.hpp"
#include "urf/devices/accesscontrol/AccessControl.hpp"

namespace urf {
namespace devices {
namespace utilities {

std::string roleToString(accesscontrol::Roles role);

}  // namespace utilities
}  // namespace devices
}  // namespace urf
