#include <algorithm>
#include <random>

#include "urf/devices/accesscontrol/AccessControl.hpp"

namespace {

std::random_device dev;
std::mt19937 rng(dev());
std::uniform_int_distribution<std::mt19937::result_type> dist(1, 255);

uint8_t getRandomId() {
    return static_cast<uint8_t>(dist(rng));
}

} // namespace

namespace urf {
namespace devices {
namespace accesscontrol {

AccessControl::AccessControl(size_t maxSlots)
    : slotsMtx_()
    , currentSlots_()
    , currentRoles_()
    , currentlyWriting_(std::nullopt)
    , maxSlots_(maxSlots) { }

std::optional<uint8_t> AccessControl::acquireSlot(const std::string& username, Roles role) {
    std::lock_guard<std::mutex> lock(slotsMtx_);
    if (currentSlots_.find(username) != currentSlots_.end()) {
        // The requested username is already in the acquire list
        auto userId = currentSlots_[username];
        if (currentRoles_[userId] < role) {
            currentRoles_[userId] = role;
        }

        if (role != Roles::VIEWER) {
            if (!currentlyWriting_) {
                // Nobody is writing so the current requestor gets it
                currentlyWriting_ = userId;
            } else if (currentlyWriting_.value() != userId) {
                // If there is already a writer but his/her role is lower, then it gets the writing role
                if (currentRoles_[currentlyWriting_.value()] < role) {
                    currentlyWriting_ = userId;
                }
            }
        } else {
            // If the current writer and is downgrading to viewer, remove the current writer
            if (currentlyWriting_ && (currentlyWriting_.value() == userId)) {
                currentlyWriting_ = std::nullopt;
            }
        }

        return userId;
    }

    // This is wrong. If the maxSlots_ you need to take the palce of someone that has a lower role than you
    if (currentSlots_.size() == maxSlots_) {
        // There's no more space
        return std::nullopt;
    }

    uint8_t newId;
    do {
        newId = getRandomId();
    } while (currentRoles_.find(newId) != currentRoles_.end());

    if (role != Roles::VIEWER) {
        if (!currentlyWriting_) {
            // Nobody is writing so the current requestor gets it
            currentlyWriting_ = newId;
        } else if ((currentlyWriting_.value() != newId) && (currentRoles_[currentlyWriting_.value()] < role)){
            // If there is already a writer but his/her role is lower, then it gets the writing role
            currentlyWriting_ = newId;
        }
    }

    currentSlots_[username] = newId;
    currentRoles_[newId] = role;

    return newId;
}

bool AccessControl::releaseSlot(uint8_t userId) {
    std::lock_guard<std::mutex> lock(slotsMtx_);
    auto it = currentRoles_.find(userId);
    if (it == currentRoles_.end()) {
        return false;
    }

    currentRoles_.erase(it);
    if (currentRoles_.size() == 0) {
        currentlyWriting_ = std::nullopt;
    }

    if (currentlyWriting_ && (currentlyWriting_.value() == userId)) {
        currentlyWriting_ = std::nullopt;
    }

    for (auto it = currentSlots_.begin(); it != currentSlots_.end(); it++) {
        if (it->second == userId) {
            currentSlots_.erase(it);
            break;
        }
    }

    return true;
}

bool AccessControl::hasReadAccess(uint8_t userId) const {
    std::lock_guard<std::mutex> lock(slotsMtx_);
    return currentRoles_.find(userId) != currentRoles_.end();
}

bool AccessControl::hasWriteAccess(uint8_t userId) const {
    std::lock_guard<std::mutex> lock(slotsMtx_);
    return (currentlyWriting_ && (currentlyWriting_.value() == userId));
}

bool AccessControl::hasReadAccess(const std::string& user) const {
    std::lock_guard<std::mutex> lock(slotsMtx_);
    return (currentSlots_.find(user) != currentSlots_.end());
}

bool AccessControl::hasWriteAccess(const std::string& user) const {
    std::lock_guard<std::mutex> lock(slotsMtx_);
    return (currentSlots_.find(user) != currentSlots_.end()) &&
           (currentlyWriting_ && (currentlyWriting_.value() == currentSlots_.at(user)));
}

std::unordered_map<std::string, uint8_t> AccessControl::currentSlots() const {
    std::lock_guard<std::mutex> lock(slotsMtx_);
    return currentSlots_;
}

} // namespace accesscontrol
} // namespace devices
} // namespace urf
