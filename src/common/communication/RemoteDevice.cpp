#include <urf/middleware/messages/Message.hpp>

#include "urf/devices/communication/RemoteDevice.hpp"

#include <urf/common/logger/Logger.hpp>
#include <urf/common/properties/ObservablePropertyFactory.hpp>

#include "common/utilities/Utilities.hpp"

#include <memory>

namespace {
auto LOGGER = urf::common::getLoggerInstance("RemoteDevice");
}

using urf::middleware::messages::Message;
using namespace urf::common::properties;

namespace urf {
namespace devices {
namespace communication {

RemoteDevice::RemoteDevice(std::shared_ptr<middleware::sockets::Client> client)
    : client_(client)
    , userId_()
    , timeout_(std::chrono::seconds(5)) {

    client_->onUpdate("component", [this](auto, auto msg) {
        settings_["component"]->at("state")->from_json(msg["state"]);
    });
}

RemoteDevice::RemoteDevice(const std::string& client)
    : RemoteDevice(std::make_shared<middleware::sockets::Client>(client)) {
}

bool RemoteDevice::open() {
    if (!client_->open()) {
        return false;
    }

    Message msg;
    msg["cmd"] = "get";
    msg["node"] = "/";

    auto response = client_->request(msg, timeout_);
    if (response.empty()) {
        LOGGER.warn("Remote device didn't respond on time");
        client_->close();
        return false;
    }

    if (!response[0]["retval"].get<bool>()) {
        LOGGER.warn("Failed to get settings: {}", response[0]["error"].get<std::string>());
        client_->close();
        return false;
    }

    try {
        settings_ = response[0]["data"].get<common::properties::PropertyNode>();
    } catch (const std::exception& ex) {
        LOGGER.info("Invalid parsing: {}, {} ", ex.what(), response[0].body().data().dump());
        client_->close();
        return false;
    }

    if (!client_->subscribe("component")) {
        LOGGER.info("Could not subscribe to topic");
        client_->close();
        return false;
    }

    return true;
}

bool RemoteDevice::close() {
    release();
    return client_->close();
}

bool RemoteDevice::resetFault() noexcept {
    if (!client_->isOpen()) {
        LOGGER.warn("Device not connected");
        return false;
    }

    return stateChangeRequest("reset");
}

bool RemoteDevice::switchOn() noexcept {
    if (!client_->isOpen()) {
        LOGGER.warn("Could not open connection to device");
        return false;
    }

    return stateChangeRequest("switchon");
}

bool RemoteDevice::shutdown() noexcept {
    if (!client_->isOpen()) {
        LOGGER.warn("Device not connected");
        return false;
    }

    return stateChangeRequest("shutdown");
}

bool RemoteDevice::disable() noexcept {
    if (!client_->isOpen()) {
        LOGGER.warn("Device not connected");
        return false;
    }

    return stateChangeRequest("disable");
}

bool RemoteDevice::enable() noexcept {
    if ((currentState() == ComponentStates::ReadyToSwitchOn) && !switchOn()) {
        return false;
    }

    if (!client_->isOpen()) {
        LOGGER.warn("Device not connected");
        return false;
    }

    return stateChangeRequest("enable");
}

bool RemoteDevice::reconfigure() noexcept {
    if (!client_->isOpen()) {
        LOGGER.warn("Device not connected");
        return false;
    }

    return stateChangeRequest("reconfigure");
}

bool RemoteDevice::fault() noexcept {
    if (!client_->isOpen()) {
        LOGGER.warn("Device not connected");
        return false;
    }

    return stateChangeRequest("fault");
}

bool RemoteDevice::acquire(const std::string& username, accesscontrol::Roles role) {
    if (!client_->isOpen()) {
        LOGGER.warn("Device not connected");
        return false;
    }

    Message request;
    request["cmd"] = "acquire";
    request["role"] = utilities::roleToString(role);
    request["user"] = username;

    auto response = client_->request(request, timeout_);
    if (response.empty()) {
        LOGGER.warn("Device did not respond");
        return false;
    }

    try {
        if (response[0]["retval"].get<bool>()) {
            userId_ = response[0]["id"].get<uint8_t>();
            return true;
        } else {
            LOGGER.warn(response[0]["error"].get<std::string>());
            return false;
        }
    } catch (const std::exception&) {
        LOGGER.warn("Something went wrong with the response");
        return false;
    }
}

bool RemoteDevice::release() {
    if (!client_->isOpen()) {
        LOGGER.warn("Device not connected");
        return false;
    }

    return stateChangeRequest("release");
}

ComponentStates RemoteDevice::currentState() const {
    if (!client_->isOpen()) {
        return ComponentStates::Init;
    }

    return get<common::components::StatePair>("component", "state").value().first;
}

ComponentStateTransitions RemoteDevice::currentTransition() const {
    if (!client_->isOpen()) {
        return ComponentStateTransitions::Init;
    }

    return get<common::components::StatePair>("component", "state").value().second;
}

std::string RemoteDevice::componentName() const {
    if (!client_->isOpen()) {
        return "";
    }

    return get<std::string>("component", "name").value();
}

std::vector<std::string> RemoteDevice::componentClass() const {
    if (!client_->isOpen()) {
        return {""};
    }

    return get<std::vector<std::string>>("component", "class").value();
}

common::properties::PropertyNode RemoteDevice::settings() const {
    return settings_;
}

bool RemoteDevice::stateChangeRequest(const std::string& cmd) {
    Message request;
    request.header().writerId(userId_);
    request["cmd"] = cmd;

    auto response = client_->request(request, timeout_);
    if (response.empty()) {
        LOGGER.warn("Device did not respond");
        return false;
    }

    try {
        if (response[0]["retval"].get<bool>()) {
            return true;
        } else {
            if (response[0].body().data().count("error") != 0) {
                LOGGER.warn("Failed to change state: {}", response[0]["error"]);
            } else {
                LOGGER.warn("Failed to change state");
            }
            return false;
        }
    } catch (const std::exception&) {
        LOGGER.warn("Something went wrong with the response: {}", response[0].body().data().dump());
        return false;
    }
}

} // namespace communication
} // namespace devices
} // namespace urf
