#include <arpa/inet.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <unistd.h>

#include "urf/devices/communication/i2c/I2CBus.hpp"

#define INT_ADDR_MAX_BYTES 4
#define PAGE_MAX_BYTES 4096

#define GET_I2C_DELAY(delay) ((delay) == 0 ? I2C_DEFAULT_DELAY : (delay))
#define GET_I2C_FLAGS(tenbit, flags) ((tenbit) ? ((flags) | I2C_M_TEN) : (flags))
#define GET_WRITE_SIZE(addr, remain, page_bytes)                                                   \
    ((addr) + (remain) > (page_bytes) ? (page_bytes) - (addr) : remain)

#include <urf/common/logger/Logger.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("I2CBus");
}

namespace urf {
namespace devices {
namespace communication {

I2CBus::I2CBus(const std::string& name)
    : name_(name)
    , fd_(-1) { }

I2CBus::~I2CBus() {
    close();
}

bool I2CBus::open() {
    std::scoped_lock lock(busMtx_);
    if (fd_ != -1) {
        return true;
    }

    if ((fd_ = ::open(name_.c_str(), O_RDWR)) == -1) {
        LOGGER.warn("Could not open");
        return false;
    }

    return true;
}

bool I2CBus::close() {
    std::scoped_lock lock(busMtx_);
    if (fd_ == -1) {
        return true;
    }

    ::close(fd_);
    fd_ = -1;
    return true;
}

bool I2CBus::isOpen() {
    std::scoped_lock lock(busMtx_);
    return fd_ != -1;
}

bool I2CBus::select(uint32_t devAddr, uint32_t tenbit) {
    if (fd_ == -1) {
        return false;
    }

    if (ioctl(fd_, I2C_TENBIT, tenbit)) {
        LOGGER.warn("Could not select");
        return false;
    }

    if (ioctl(fd_, I2C_SLAVE, devAddr)) {
        LOGGER.warn("Could not select");
        return false;
    }

    return true;
}

void I2CBus::addressConvert(uint32_t iaddr, uint32_t len, unsigned char* addr) {
    union {
        unsigned int iaddr;
        unsigned char caddr[INT_ADDR_MAX_BYTES];
    } convert;

    /* I2C internal address order is big-endian, same with network order */
    convert.iaddr = htonl(iaddr);

    /* Copy address to addr buffer */
    int i = len - 1;
    int j = INT_ADDR_MAX_BYTES - 1;

    while (i >= 0 && j >= 0) {
        addr[i--] = convert.caddr[j--];
    }
}

bool I2CBus::write(const I2CDevice& device,
                   unsigned int iaddr,
                   const std::vector<uint8_t>& buffer) {
    std::scoped_lock lock(busMtx_);
    if (fd_ == -1) {
        return false;
    }

    ssize_t remain = buffer.size();
    ssize_t ret;
    size_t cnt = 0, size = 0;
    unsigned char tmp_buf[PAGE_MAX_BYTES + INT_ADDR_MAX_BYTES];

    /* Set i2c slave address */
    if (!select(device.address(), device.tenbit())) {
        return false;
    }

    /* Once only can write less than 4 byte */
    while (remain > 0) {
        size = GET_WRITE_SIZE(iaddr % device.pageBytes(), remain, device.pageBytes());
        memset(tmp_buf, 0, sizeof(tmp_buf));
        addressConvert(iaddr, device.iaddrBytes(), tmp_buf);

        /* Copy data to tmp_buf */
        memcpy(tmp_buf + device.iaddrBytes(), buffer.data() + cnt, size);

        /* Write to buf content to i2c device length  is address length and
                write buffer length */
        ret = ::write(fd_, tmp_buf, device.iaddrBytes() + size);
        if (ret == -1 || (size_t)ret != device.iaddrBytes() + size) {
            LOGGER.warn("Could not write");
            return false;
        }

        usleep(device.delay() * 1e3);

        /* Move to next #size bytes */
        cnt += size;
        iaddr += size;
        remain -= size;
    }

    return true;
}

bool I2CBus::writeBits(
    const I2CDevice& device, unsigned int iaddr, uint8_t data, uint8_t startBit, uint8_t len) {
    if (fd_ == -1) {
        return false;
    }

    uint8_t b = readBits(device, iaddr, startBit, len);

    uint8_t mask = ((1 << len) - 1) << (startBit - len + 1);
    data <<= (startBit - len + 1); // shift data into correct position
    data &= mask; // zero all non-important bits in data
    b &= ~(mask); // zero all important bits in existing byte
    b |= data; // combine data with existing byte

    std::scoped_lock lock(busMtx_);
    return write(device, iaddr, {b});
}

std::vector<uint8_t> I2CBus::read(const I2CDevice& device, uint32_t iaddr, size_t len) {
    std::scoped_lock lock(busMtx_);
    if (fd_ == -1) {
        return std::vector<uint8_t>();
    }

    ssize_t cnt;
    unsigned char addr[INT_ADDR_MAX_BYTES];

    /* Set i2c slave address */
    if (!select(device.address(), device.tenbit())) {
        return std::vector<uint8_t>();
    }

    /* Convert i2c internal address */
    memset(addr, 0, sizeof(addr));
    addressConvert(iaddr, device.iaddrBytes(), addr);

    /* Write internal address to devide  */
    if (::write(fd_, addr, device.iaddrBytes()) != device.iaddrBytes()) {
        LOGGER.info("Could not write address: {}", strerror(errno));
        return std::vector<uint8_t>();
    }

    /* Wait a while */
    usleep(device.delay() * 1e3);
    std::vector<uint8_t> buf;
    buf.resize(len);

    /* Read count bytes data from int_addr specify address */
    if ((cnt = ::read(fd_, buf.data(), len)) == -1) {
        LOGGER.warn("Could not read");
        return std::vector<uint8_t>();
    }

    if (static_cast<size_t>(cnt) != len) {
        buf.resize(cnt);
    }

    return buf;
}

uint8_t I2CBus::readBits(const I2CDevice& device, uint32_t iaddr, uint8_t startBit, uint8_t len) {
    if (fd_ == -1) {
        return 0;
    }

    uint8_t b = 0;
    auto bytes = read(device, iaddr, len);
    if (bytes.size() == 0) {
        return 0;
    }

    b = bytes[0];

    uint8_t mask = ((1 << len) - 1) << (startBit - len + 1);
    b &= mask;
    b >>= (startBit - len + 1);
    return b;
}

} // namespace communication
} // namespace devices
} // namespace urf
