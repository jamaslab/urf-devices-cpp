#include "urf/devices/communication/i2c/I2CDevice.hpp"

namespace urf {
namespace devices {
namespace communication {
    
I2CDevice::I2CDevice(uint16_t addr,
                     uint8_t tenbit,
                     uint8_t delay,
                     uint16_t flags,
                     uint32_t page_bytes,
                     uint32_t iaddr_bytes)
    : addr_(addr)
    , tenbit_(tenbit)
    , delay_(delay)
    , flags_(flags)
    , page_bytes_(page_bytes)
    , iaddr_bytes_(iaddr_bytes) { }

} // namespace communication
} // namespace devices
} // namespace urf
