#pragma once

#include <chrono>

#include <urf/middleware/sockets/Client.hpp>
#include <urf/middleware/messages/Message.hpp>

namespace urf {
namespace devices {
namespace communication {

class CommandHandler {
 public:
    static bool sendCommand(const urf::middleware::messages::Message& request, std::shared_ptr<urf::middleware::sockets::Client> client, const std::chrono::milliseconds& timeout);
};

} // namespace communication
} // namespace devices
} // namespace urf
