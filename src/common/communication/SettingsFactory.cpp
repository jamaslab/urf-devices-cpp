#include "common/communication/SettingsFactory.hpp"

#include <urf/common/properties/ObservablePropertyFactory.hpp>

namespace urf {
namespace devices {
namespace communication {

std::shared_ptr<common::properties::IObservableProperty> SettingsFactory::setting(const nlohmann::json& json) {
    try {
        auto property = common::properties::ObservablePropertyFactory::create(
            json["dtype"], json["type"].get<common::properties::PropertyType>());
        property->from_json(json);
        return property;
    } catch (const std::exception&) {
        auto property = common::properties::ObservablePropertyFactory::create(
            "json", json["type"].get<common::properties::PropertyType>());
        property->from_json(json);
        return property;
    }
}

common::components::SettingsGroup SettingsFactory::settingsGroup(const std::string groupName, const nlohmann::json& json) {
    common::components::SettingsGroup sg(groupName);
    for (auto const& variable : json.items()) {
        sg.insert({variable.key(), setting(variable.value())});
    }

    return sg;
}

} // namespace communication
} // namespace devices
} // namespace urf
