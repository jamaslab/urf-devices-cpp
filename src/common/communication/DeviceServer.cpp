#include "urf/devices/communication/DeviceServer.hpp"
#include "urf/devices/communication/handlers/ComponentHandlers.hpp"
#include "urf/devices/communication/handlers/UpdateAllHandler.hpp"

#include <urf/common/logger/Logger.hpp>

#include "common/utilities/Utilities.hpp"

namespace {
auto LOGGER = urf::common::getLoggerInstance("DeviceServer");

} // namespace

namespace urf {
namespace devices {
namespace communication {

DeviceServer::DeviceServer(std::shared_ptr<middleware::sockets::Server> server,
                           std::shared_ptr<common::components::IComponent> device,
                           size_t maxSlots)
    : isOpen_(false)
    , server_(server)
    , device_(device)
    , accessControl_(new accesscontrol::AccessControl(maxSlots))
    , stopThreads_(false)
    , requestHandlers_()
    , pullHandlers_()
    , updateHandlers_()
    , requestHandlerThread_() {
    LOGGER.trace("CTor");

    requestHandlers_["acquire"] = std::make_unique<AcquireSlotHandler>(accessControl_);
    requestHandlers_["release"] = std::make_unique<ReleaseSlotHandler>(accessControl_);
    requestHandlers_["reset"] = std::make_unique<ResetFaultHandler>(device_, accessControl_);
    requestHandlers_["switchon"] = std::make_unique<SwitchOnHandler>(device_, accessControl_);
    requestHandlers_["shutdown"] = std::make_unique<ShutdownHandler>(device_, accessControl_);
    requestHandlers_["disable"] = std::make_unique<DisableHandler>(device_, accessControl_);
    requestHandlers_["enable"] = std::make_unique<EnableHandler>(device_, accessControl_);
    requestHandlers_["reconfigure"] = std::make_unique<ReconfigureHandler>(device_, accessControl_);
    requestHandlers_["fault"] = std::make_unique<FaultHandler>(device_, accessControl_);
    requestHandlers_["set"] = std::make_unique<SetHandler>(device_, accessControl_);
    requestHandlers_["get"] = std::make_unique<GetHandler>(device_, accessControl_);

    addUpdateHandler(std::make_unique<UpdateAllHandler>(server, "component", device_->settings()["component"]));
}

bool DeviceServer::open() {
    LOGGER.trace("open()");
    if (isOpen_) {
        LOGGER.warn("Already opened");
        return false;
    }

    stopThreads_ = false;
    if (!server_->open()) {
        LOGGER.warn("Could not open server");
        return false;
    }

    requestHandlerThread_ = std::thread(&DeviceServer::requestHandler, this);

    isOpen_ = true;
    return true;
}

bool DeviceServer::close() {
    LOGGER.trace("close()");
    if (!isOpen_) {
        LOGGER.warn("Not opened");
        return false;
    }

    stopThreads_ = true;
    server_->close();

    requestHandlerThread_.join();
    isOpen_ = false;
    return true;
}

std::shared_ptr<accesscontrol::AccessControl> DeviceServer::accessControl() const noexcept {
    return accessControl_;
}

void DeviceServer::addRequestHandler(const std::string& request, std::unique_ptr<RequestHandlerBase> handler) noexcept {
    LOGGER.trace("addRequestHandler({})", request);
    requestHandlers_[request] = std::move(handler);
}

void DeviceServer::addPullHandler(std::unique_ptr<PullHandlerBase> handler) noexcept {
    LOGGER.trace("addPullHandler()");
    server_->onPush([this, &handler](auto message) {
        handler->operator()(message);
    });

    pullHandlers_.push_back(std::move(handler));
}

void DeviceServer::addUpdateHandler(std::unique_ptr<UpdateHandlerBase> handler) noexcept {
    LOGGER.trace("addUpdateHandler()");

    server_->addTopic(handler->topic());
    updateHandlers_.push_back(std::move(handler));
}

void DeviceServer::requestHandler() {
    middleware::messages::Message response;
    while (!stopThreads_) {
        if (!server_->isOpen()) {
            return;
        }

        auto requestOpt = server_->receiveRequest();
        if (!requestOpt) {
            LOGGER.warn("Invalid request");
            continue;
        }

        auto request = requestOpt.value();

        if (request.body().data().find("cmd") != request.body().data().end()) {
            std::string cmd = request["cmd"];
            auto it = requestHandlers_.find(cmd);
            if (it != requestHandlers_.end()) {
                LOGGER.trace("Handling request: {}", cmd);
                response = it->second->operator()(request);
            } else {
                LOGGER.warn("Invalid request: {}", cmd);

                response["retval"] = false;
                response["error"] = "Invalid request";
            }
        } else {
            LOGGER.warn("Missing cmd field in request");

            response["retval"] = false;
            response["error"] = "Missing cmd field in request";
        }

        server_->respond(response);
    }
}

} // namespace communication
} // namespace devices
} // namespace urf
