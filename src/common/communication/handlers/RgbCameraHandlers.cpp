#include "urf/devices/communication/handlers/RgbCameraHandlers.hpp"

#include <urf/algorithms/compression/VideoCompressionFactory.hpp>

#include <urf/common/logger/Logger.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("RgbCameraHandlers");
}

namespace urf {
namespace devices {
namespace communication {

using namespace sensors::cameras;

using urf::algorithms::compression::CompressionQuality;
using urf::algorithms::compression::VideoCompressionFactory;
using urf::algorithms::compression::VideoFrame;

StartRgbStreamHandler::StartRgbStreamHandler(
    std::shared_ptr<middleware::sockets::ISocket> socket,
    std::shared_ptr<sensors::cameras::RgbCamera> device,
    std::shared_ptr<accesscontrol::AccessControl> accessControl)
    : RequestHandlerBase("startstream")
    , _socket(socket)
    , _device(device)
    , _accessControl(accessControl)
    , _frameGrabberThread()
    , _frameGrabberStarted(false)
    , _frameMtx()
    , _latestFrame()
    , _videoServersMtx()
    , _videoServers() { }

StartRgbStreamHandler::~StartRgbStreamHandler() {
    if (_frameGrabberStarted || _frameGrabberThread.joinable()) {
        _frameGrabberThread.join();
    }

    for (auto& videoServer : _videoServers) {
        videoServer->ended = true;
        if (videoServer->videoServerThread.joinable()) {
            videoServer->videoServerThread.join();
        }
    }
}

middleware::messages::Message
StartRgbStreamHandler::operator()(middleware::messages::Message& request) noexcept {
    middleware::messages::Message response;
    if (!_accessControl->hasReadAccess(request.header().writerId())) {
        response["retval"] = false;
        response["error"] = "No read access";
        return response;
    }

    if (_device->currentState() != ComponentStates::Enabled) {
        response["retval"] = false;
        response["error"] = "Camera is not in enabled state";
        return response;
    }

    auto streamProfile = _device->get<StreamProfile>("configuration", "resolution").value();

    auto config = std::make_shared<RgbVideoServerConfig>();
    auto quality = CompressionQuality::Normal;
    config->fps = streamProfile.fps;

    try {
        // Here there must be a bit of input validation
        // and also a default one if data doesn't arrive
        if ((request["rgb"].count("width") != 0) && (request["rgb"].count("height") != 0)) {
            config->resolution = algorithms::compression::VideoResolution(request["rgb"]["width"],
                                                                          request["rgb"]["height"]);
        }

        if (request["rgb"].count("fps") != 0) {
            config->fps = request["rgb"]["fps"];
        }

        if (request["rgb"].count("quality") != 0) {
            quality = static_cast<CompressionQuality>(request["rgb"]["quality"].get<int>());
        }

        config->encoder = VideoCompressionFactory::getEncoder(
            request["rgb"]["encoding"],
            config->resolution.empty() ? streamProfile.resolution : config->resolution,
            config->fps,
            quality,
            true);
    } catch (const std::exception& ex) {
        response["retval"] = false;
        response["error"] = ex.what();
        return response;
    }

    config->topic = "s_" + request["rgb"]["encoding"].get<std::string>();

    if (!config->resolution.empty()) {
        config->topic += "_" + std::to_string(config->resolution.width) + "x" +
                         std::to_string(config->resolution.height);
    }

    if (config->fps != 0) {
        config->topic += "@" + std::to_string(config->fps);
    }

    if (quality != CompressionQuality::Normal) {
        config->topic += "_" + std::to_string(static_cast<int>(quality));
    }

    config->ended = false;

    if (!_frameGrabberStarted) {
        if (_frameGrabberThread.joinable())
            _frameGrabberThread.join();

        _frameGrabberThread = std::thread(&StartRgbStreamHandler::frameGrabber, this);
        while (!_frameGrabberStarted)
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    if (_socket->addTopic(config->topic)) {
        config->videoServerThread = std::thread(&StartRgbStreamHandler::videoServer, this, config);
        std::unique_lock lock(_videoServersMtx);
        _videoServers.push_back(config);
    }

    response["retval"] = true;
    response["topicname"] = config->topic;

    return response;
}

void StartRgbStreamHandler::frameGrabber() {
    _frameGrabberStarted = true;
    while ((_device->currentState() == ComponentStates::Enabled) && (_socket->isOpen())) {
        // Here check if higher or lower resolution can be applied to the camera and then call reconfigure
        auto frame = _device->getFrame();
        if (!frame.empty()) {
            std::unique_lock lock(_frameMtx);
            _latestFrame = frame;
        }

        {
            // I move here the video servers cleanup
            std::unique_lock lock(_videoServersMtx);
            _videoServers.erase(std::remove_if(_videoServers.begin(),
                                               _videoServers.end(),
                                               [](std::shared_ptr<RgbVideoServerConfig> p) {
                                                   if (p->ended.load())
                                                       p->videoServerThread.join();
                                                   return p->ended.load();
                                               }),
                                _videoServers.end());
        }
    }

    _frameGrabberStarted = false;
}

void StartRgbStreamHandler::videoServer(std::shared_ptr<RgbVideoServerConfig> config) {
    std::chrono::time_point<std::chrono::high_resolution_clock> noSubscribersTime_;
    bool noSubscribers_ = false;

    while ((_device->currentState() == ComponentStates::Enabled) && (_socket->isOpen())) {
        auto start = std::chrono::high_resolution_clock::now();
        std::optional<VideoFrame> frame = std::nullopt;
        {
            std::shared_lock lock(_frameMtx);
            if (_latestFrame.empty())
                continue;
            frame = _latestFrame;
        }

        config->encoder->addFrame(frame.value());

        middleware::messages::Message frameMsg;
        frameMsg["rgb"] = nlohmann::json::binary(config->encoder->getBytes());
        frameMsg["rgbenc"] = config->encoder->getName();
        _socket->publish(config->topic, frameMsg);

        LOGGER.trace("Duration: {}",
                     std::chrono::duration_cast<std::chrono::milliseconds>(
                         std::chrono::high_resolution_clock::now() - start)
                         .count());

        if (_socket->subscriptionsCount(config->topic) == 0) {
            if (!noSubscribers_) {
                noSubscribers_ = true;
                noSubscribersTime_ = start;
            }
        } else {
            noSubscribers_ = false;
        }

        if (noSubscribers_ &&
            (std::chrono::duration_cast<std::chrono::seconds>(start - noSubscribersTime_).count() >
             5)) {
            LOGGER.info("Closing the video server as nobody is subscribed anymore");
            break;
        }

        if (config->fps != 0) {
            std::this_thread::sleep_until(start + std::chrono::milliseconds(1000 / config->fps));
        }
    }

    _socket->removeTopic(config->topic);
    config->ended = true;
}

} // namespace communication
} // namespace devices
} // namespace urf