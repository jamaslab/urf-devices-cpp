#include <urf/devices/communication/handlers/RequestHandlerBase.hpp>

namespace urf {
namespace devices {
namespace communication {

RequestHandlerBase::RequestHandlerBase(const std::string& command) : _command(command) {}

const std::string& RequestHandlerBase::command() const {
    return _command;
}

}  // namespace communication
}  // namespace devices
}  // namespace urf