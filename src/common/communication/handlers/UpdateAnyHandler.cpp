#include "urf/devices/communication/handlers/UpdateAnyHandler.hpp"

#include <iostream>

namespace urf {
namespace devices {
namespace communication {

UpdateAnyHandler::UpdateAnyHandler(
    std::shared_ptr<middleware::sockets::ISocket> socket,
    const std::string& topic,
    std::shared_ptr<common::properties::IObservableProperty> property,
    std::chrono::duration<double> updateDelay)
    : UpdateHandlerBase(socket, topic, property)
    , _updateDelay(updateDelay)
    , _updateFuture()
    , _msgMutex()
    , _updateMessage() {
    subscribeToValueChanges(topic, property);
}

void UpdateAnyHandler::subscribeToValueChanges(
    const std::string& propertyName,
    std::shared_ptr<common::properties::IObservableProperty> property) {
    auto node = std::dynamic_pointer_cast<common::properties::PropertyNode>(property);
    if (node != nullptr) {
        for (auto& [name, prop] : node->getValue()) {
            subscribeToValueChanges(name, prop);
        }
    } else {
        property->onAnyValueChange([this, propertyName, property](auto, auto) {
            if (_updateDelay.count() == 0) {
                // We publish immediately the change
                middleware::messages::Message msg;
                property->to_json(msg[propertyName], true);
                _socket->publish(_topic, std::move(msg));
            } else {
                std::lock_guard<std::mutex> lock(_msgMutex);
                property->to_json(_updateMessage[propertyName], true);

                // This is a delayed publish
                if (!_updateFuture.valid() || (_updateFuture.wait_for(std::chrono::seconds(0)) ==
                                               std::future_status::ready)) {
                    _updateFuture = std::async(std::launch::async, [this]() {
                        std::this_thread::sleep_for(_updateDelay);
                        std::lock_guard<std::mutex> lock(_msgMutex);
                        _socket->publish(_topic, _updateMessage);
                        _updateMessage = middleware::messages::Message();
                    });
                }
            }
        });
    }
}

} // namespace communication
} // namespace devices
} // namespace urf