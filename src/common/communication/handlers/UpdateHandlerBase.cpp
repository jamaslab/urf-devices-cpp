#include "urf/devices/communication/handlers/UpdateHandlerBase.hpp"

namespace urf {
namespace devices {
namespace communication {

UpdateHandlerBase::UpdateHandlerBase(
    std::shared_ptr<middleware::sockets::ISocket> socket,
    const std::string& topic,
    std::shared_ptr<common::properties::IObservableProperty> property)
    : _socket(std::move(socket))
    , _topic(topic)
    , _property(std::move(property)) { }

std::string UpdateHandlerBase::topic() const noexcept {
    return _topic;
}

} // namespace communication
} // namespace devices
} // namespace urf