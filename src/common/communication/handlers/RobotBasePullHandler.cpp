#include "urf/devices/communication/handlers/RobotBasePullHandler.hpp"

namespace urf {
namespace devices {
namespace communication {

RobotBasePullHandler::RobotBasePullHandler(
    std::shared_ptr<robots::robotbase::RobotBase> device,
    std::shared_ptr<accesscontrol::AccessControl> accessControl)
    : PullHandlerBase()
    , _device(std::move(device))
    , _accessControl(std::move(accessControl)) { }

void RobotBasePullHandler::operator()(const middleware::messages::Message& command) noexcept {
    if (!_accessControl->hasWriteAccess(command.header().writerId())) {
            return;
    } else if (command.body().data().find("cart_vel") != command.body().data().end()) {
        try {
            auto vel = command["cart_vel"].get<std::array<float, 3>>();
            _device->setCartesianVelocity(vel);
        } catch (const std::exception&) {}
    }
}

} // namespace communication
} // namespace devices
} // namespace urf