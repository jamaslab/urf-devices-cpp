#include "urf/devices/communication/handlers/UpdateAllHandler.hpp"

namespace urf {
namespace devices {
namespace communication {

UpdateAllHandler::UpdateAllHandler(
    std::shared_ptr<middleware::sockets::ISocket> socket,
    const std::string& topic,
    std::shared_ptr<common::properties::IObservableProperty> property)
    : UpdateHandlerBase(socket, topic, property)
    , _updateTotal()
    , _updateMessageMtx()
    , _updateCount(0)
    , _updateMessage() {
    subscribeToValueChanges(topic, property);
}

void UpdateAllHandler::subscribeToValueChanges(
    const std::string& propertyName,
    std::shared_ptr<common::properties::IObservableProperty> property) {
    auto node = std::dynamic_pointer_cast<common::properties::PropertyNode>(property);
    if (node != nullptr) {
        for (auto& [name, prop] : node->getValue()) {
            subscribeToValueChanges(name, prop);
        }
    } else {
        _updateTotal++;
        property->onAnyValueChange([this, propertyName, property](auto, auto) {
            std::lock_guard<std::mutex> lock(_updateMessageMtx);
            _updateCount++;
            property->to_json(_updateMessage[propertyName], true);

            if (_updateCount == _updateTotal) {
                _socket->publish(_topic, _updateMessage);
                _updateCount = 0;
                _updateMessage = middleware::messages::Message();
            }
        });
    }
}

} // namespace communication
} // namespace devices
} // namespace urf