#include "urf/devices/communication/handlers/RgbdCameraHandlers.hpp"

#include <urf/algorithms/compression/DataCompressionFactory.hpp>
#include <urf/algorithms/compression/VideoCompressionFactory.hpp>

#include <urf/common/logger/Logger.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("RgbdCameraHandlers");
}

namespace urf {
namespace devices {
namespace communication {

using namespace sensors::cameras;

using urf::algorithms::compression::CompressionQuality;
using urf::algorithms::compression::VideoCompressionFactory;
using urf::algorithms::compression::DataCompressionFactory;
using urf::algorithms::compression::PointCloudEncoder;

StartRgbdStreamHandler::StartRgbdStreamHandler(
    std::shared_ptr<middleware::sockets::ISocket> socket,
    std::shared_ptr<sensors::cameras::RgbdCamera> device,
    std::shared_ptr<accesscontrol::AccessControl> accessControl)
    : RequestHandlerBase("startstream")
    , _socket(socket)
    , _device(device)
    , _accessControl(accessControl)
    , _frameGrabberThread()
    , _frameGrabberStarted(false)
    , _frameMtx()
    , _latestFrame()
    , _videoServersMtx()
    , _videoServers() { }

StartRgbdStreamHandler::~StartRgbdStreamHandler() {
    if (_frameGrabberStarted || _frameGrabberThread.joinable()) {
        _frameGrabberThread.join();
    }

    for (auto& videoServer : _videoServers) {
        videoServer->ended = true;
        if (videoServer->videoServerThread.joinable()) {
            videoServer->videoServerThread.join();
        }
    }
}

middleware::messages::Message
StartRgbdStreamHandler::operator()(middleware::messages::Message& request) noexcept {
    middleware::messages::Message response;
    if (!_accessControl->hasReadAccess(request.header().writerId())) {
        response["retval"] = false;
        response["error"] = "No read access";
        return response;
    }

    if (_device->currentState() != ComponentStates::Enabled) {
        response["retval"] = false;
        response["error"] = "Camera is not in enabled state";
        return response;
    }

    auto config = std::make_shared<RgbdServerConfig>();
    int quality = 0;
    try {
        config->fps = request["fps"];

        // Here there must be a bit of input validation
        // and also a default one if data doesn't arrive
        config->includeColor = request["rgb"]["active"];
        if (config->includeColor) {
            config->colorResolution = {request["rgb"]["width"], request["rgb"]["height"]};
            quality = request["rgb"]["quality"];
            config->rgbEncoder =
                VideoCompressionFactory::getEncoder(request["rgb"]["encoding"],
                                                    config->colorResolution,
                                                    config->fps,
                                                    static_cast<CompressionQuality>(quality),
                                                    true);
        }

        config->includeDepth = request["depth"]["active"];
        if (config->includeDepth) {
            config->depthResolution =
                {request["depth"]["width"], request["depth"]["height"]};
            config->depthEncoder =
                DataCompressionFactory::getEncoder(request["depth"]["encoding"], false);
        }
    } catch (const std::exception& ex) {
        response["retval"] = false;
        response["error"] = ex.what();
        return response;
    }

    auto colorActive = _device->get<bool>("configuration", "color_active").value();
    if (config->includeColor && !colorActive) {
        response["retval"] = false;
        response["error"] = "Color frame not active on device";
        return response;
    }

    auto depthActive =  _device->get<bool>("configuration", "depth_active").value();
    if (config->includeDepth && !depthActive) {
        response["retval"] = false;
        response["error"] = "Depth frame not active on device";
        return response;
    }

    config->topic = "s";
    if (config->includeColor) {
        config->topic += "_c_" + std::to_string(config->colorResolution.width) + "x" +
                         std::to_string(config->colorResolution.height) + "_" +
                         std::to_string(quality);
    }

    if (config->includeDepth) {
        config->topic += "_d_" + std::to_string(config->depthResolution.width) + "x" +
                         std::to_string(config->depthResolution.height);
    }

    config->topic += "@" + std::to_string(config->fps);
    config->ended = false;

    if (!_frameGrabberStarted) {
        if (_frameGrabberThread.joinable())
            _frameGrabberThread.join();

        auto pcActive = _device->get<bool>("configuration", "pointcloud_active").value();
        if (pcActive) {
            _cloudEncoder.reset(new PointCloudEncoder(false, DataCompressionFactory::getEncoder("lz4", false)));
            _socket->addTopic("cloud");
        } else {
            _socket->removeTopic("cloud");
        }

        _frameGrabberThread = std::thread(&StartRgbdStreamHandler::frameGrabber, this);
        while (!_frameGrabberStarted)
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    if (_socket->addTopic(config->topic)) {
        config->videoServerThread = std::thread(&StartRgbdStreamHandler::videoServer, this, config);
        std::unique_lock lock(_videoServersMtx);
        _videoServers.push_back(config);
    }

    response["retval"] = true;
    response["topicname"] = config->topic;

    return response;
}

void StartRgbdStreamHandler::frameGrabber() {
    _frameGrabberStarted = true;
    while ((_device->currentState() == ComponentStates::Enabled) && (_socket->isOpen())) {
        // Here check if higher or lower resolution can be applied to the camera and then call reconfigure
        auto frame = _device->getFrame();
        {
            std::unique_lock lock(_frameMtx);
            _latestFrame = frame;
        }

        {
            // I move here the video servers cleanup
            std::unique_lock lock(_videoServersMtx);
            _videoServers.erase(std::remove_if(_videoServers.begin(),
                                               _videoServers.end(),
                                               [](std::shared_ptr<RgbdServerConfig> p) {
                                                   if (p->ended.load())
                                                       p->videoServerThread.join();
                                                   return p->ended.load();
                                               }),
                                _videoServers.end());
        }

        if (frame.pointCloud && _socket->subscriptionsCount("cloud")) {
            middleware::messages::Message frameMsg;
            frameMsg["pclenc"] = _cloudEncoder->getName();
            frameMsg["pcl"] = nlohmann::json::binary(
                _cloudEncoder->encode<pcl::PointXYZRGB>(frame.pointCloud.value()));
            _socket->publish("cloud", frameMsg);
        }
    }

    _frameGrabberStarted = false;
}

void StartRgbdStreamHandler::videoServer(std::shared_ptr<RgbdServerConfig> config) {
    std::chrono::time_point<std::chrono::high_resolution_clock> noSubscribersTime_;
    bool noSubscribers_ = false;

    while ((_device->currentState() == ComponentStates::Enabled) && (_socket->isOpen())) {
        auto start = std::chrono::high_resolution_clock::now();
        middleware::messages::Message frameMsg;

        if (config->includeColor) {
            std::optional<algorithms::compression::VideoFrame> frame = std::nullopt;
            {
                std::shared_lock lock(_frameMtx);
                if (_latestFrame.colorFrame == std::nullopt)
                    continue;
                frame = _latestFrame.colorFrame.value();
            }

            config->rgbEncoder->addFrame(frame.value());

            frameMsg["rgb"] = nlohmann::json::binary(config->rgbEncoder->getBytes());
            frameMsg["rgbenc"] = config->rgbEncoder->getName();
        }

        if (config->includeDepth) {
            std::optional<algorithms::compression::VideoFrame> frame = std::nullopt;
            {
                std::shared_lock lock(_frameMtx);
                if (_latestFrame.depthFrame == std::nullopt)
                    continue;
                frame = _latestFrame.depthFrame.value();
            }

            frameMsg["dth"] = nlohmann::json::binary(
                config->depthEncoder->encode(frame.value().data(0), frame.value().stride(0)*frame.value().height()));
            frameMsg["dthenc"] = config->depthEncoder->getName();
        }

        _socket->publish(config->topic, frameMsg);
        LOGGER.trace("Duration: {}",
                      std::chrono::duration_cast<std::chrono::milliseconds>(
                          std::chrono::high_resolution_clock::now() - start)
                          .count());

        if (_socket->subscriptionsCount(config->topic) == 0) {
            if (!noSubscribers_) {
                noSubscribers_ = true;
                noSubscribersTime_ = start;
            }
        } else {
            noSubscribers_ = false;
        }

        if (noSubscribers_ &&
            (std::chrono::duration_cast<std::chrono::seconds>(start - noSubscribersTime_).count() >
             5)) {
            LOGGER.info("Closing the video server as nobody is subscribed anymore");
            break;
        }

        std::this_thread::sleep_until(start + std::chrono::milliseconds(1000 / config->fps));
    }

    _socket->removeTopic(config->topic);
    config->ended = true;
}

} // namespace communication
} // namespace devices
} // namespace urf