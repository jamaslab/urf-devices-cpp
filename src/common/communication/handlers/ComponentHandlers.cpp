#include "urf/devices/communication/handlers/ComponentHandlers.hpp"

namespace {
std::vector<std::string> split(std::string s, std::string delimiter) {
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    std::string token;
    std::vector<std::string> res;

    while ((pos_end = s.find(delimiter, pos_start)) != std::string::npos) {
        token = s.substr(pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back(token);
    }

    res.push_back(s.substr(pos_start));
    return res;
}

}

namespace urf {
namespace devices {
namespace communication {

AcquireSlotHandler::AcquireSlotHandler(std::shared_ptr<accesscontrol::AccessControl> accessControl)
    : RequestHandlerBase("acquire")
    , _accessControl(accessControl) { }

middleware::messages::Message
AcquireSlotHandler::operator()(middleware::messages::Message& request) noexcept {
    middleware::messages::Message response;
    accesscontrol::Roles role;

    std::string username;

    try {
        username = request["user"];
    } catch (const std::exception&) {
        response["retval"] = false;
        response["error"] = "Missing user in acquire request";
        return response;
    }

    try {
        std::string roleStr = request["role"];
        if (roleStr == "viewer") {
            role = accesscontrol::Roles::VIEWER;
        } else if (roleStr == "user") {
            role = accesscontrol::Roles::USER;
        } else if (roleStr == "expert") {
            role = accesscontrol::Roles::EXPERT;
        } else if (roleStr == "sudo") {
            role = accesscontrol::Roles::SUDO;
        } else {
            response["retval"] = false;
            response["error"] = "Invalid role " + roleStr;
            return response;
        }
    } catch (const std::exception&) {
        response["retval"] = false;
        response["error"] = "Missing role in acquire request";
        return response;
    }

    auto userId = _accessControl->acquireSlot(username, role);

    if (userId) {
        response["retval"] = true;
        response["id"] = userId.value();
    } else {
        response["retval"] = false;
        response["error"] = "Could not acquire a slot";
    }

    return response;
}

ReleaseSlotHandler::ReleaseSlotHandler(std::shared_ptr<accesscontrol::AccessControl> accessControl)
    : RequestHandlerBase("release")
    , _accessControl(accessControl) { }

middleware::messages::Message
ReleaseSlotHandler::operator()(middleware::messages::Message& request) noexcept {
    middleware::messages::Message response;
    if (_accessControl->releaseSlot(request.header().writerId())) {
        response["retval"] = true;
    } else {
        response["retval"] = false;
        response["error"] = "Could not release slot";
    }

    return response;
}

ResetFaultHandler::ResetFaultHandler(std::shared_ptr<common::components::IComponent> device,
                                     std::shared_ptr<accesscontrol::AccessControl> accessControl)
    : RequestHandlerBase("reset")
    , _device(device)
    , _accessControl(accessControl) { }

middleware::messages::Message
ResetFaultHandler::operator()(middleware::messages::Message& request) noexcept {
    middleware::messages::Message response;
    if (!_accessControl->hasWriteAccess(request.header().writerId())) {
        response["retval"] = false;
        response["error"] = "No write access";
    } else {
        auto retval = _device->resetFault();
        response["retval"] = retval;

        if (!retval) {
            response["error"] = "Device could not be reset";
        }
    }

    return response;
}

SwitchOnHandler::SwitchOnHandler(std::shared_ptr<common::components::IComponent> device,
                                 std::shared_ptr<accesscontrol::AccessControl> accessControl)
    : RequestHandlerBase("switchon")
    , _device(device)
    , _accessControl(accessControl) { }

middleware::messages::Message SwitchOnHandler::operator()(middleware::messages::Message& request) noexcept {
    middleware::messages::Message response;
    if (!_accessControl->hasWriteAccess(request.header().writerId())) {
        response["retval"] = false;
        response["error"] = "No write access";
    } else {
        auto retval = _device->switchOn();
        response["retval"] = retval;

        if (!retval) {
            response["error"] = "Device could not be switched on";
        }
    }

    return response;
}

ShutdownHandler::ShutdownHandler(std::shared_ptr<common::components::IComponent> device,
                                 std::shared_ptr<accesscontrol::AccessControl> accessControl)
    : RequestHandlerBase("shutdown")
    , _device(device)
    , _accessControl(accessControl) { }

middleware::messages::Message ShutdownHandler::operator()(middleware::messages::Message& request) noexcept {
    middleware::messages::Message response;
    if (!_accessControl->hasWriteAccess(request.header().writerId())) {
        response["retval"] = false;
        response["error"] = "No write access";
    } else {
        auto retval = _device->shutdown();
        response["retval"] = retval;

        if (!retval) {
            response["error"] = "Device could not be shutdown";
        }
    }

    return response;
}

DisableHandler::DisableHandler(std::shared_ptr<common::components::IComponent> device,
                               std::shared_ptr<accesscontrol::AccessControl> accessControl)
    : RequestHandlerBase("disable")
    , _device(device)
    , _accessControl(accessControl) { }

middleware::messages::Message DisableHandler::operator()(middleware::messages::Message& request) noexcept {
    middleware::messages::Message response;
    if (!_accessControl->hasWriteAccess(request.header().writerId())) {
        response["retval"] = false;
        response["error"] = "No write access";
    } else {
        auto retval = _device->disable();
        response["retval"] = retval;

        if (!retval) {
            response["error"] = "Device could not be disabled";
        }
    }

    return response;
}

EnableHandler::EnableHandler(std::shared_ptr<common::components::IComponent> device,
                             std::shared_ptr<accesscontrol::AccessControl> accessControl)
    : RequestHandlerBase("enable")
    , _device(device)
    , _accessControl(accessControl) { }

middleware::messages::Message EnableHandler::operator()(middleware::messages::Message& request) noexcept {
    middleware::messages::Message response;
    if (!_accessControl->hasWriteAccess(request.header().writerId())) {
        response["retval"] = false;
        response["error"] = "No write access";
    } else {
        auto retval = _device->enable();
        response["retval"] = retval;

        if (!retval) {
            response["error"] = "Device could not be enabled";
        }
    }

    return response;
}

ReconfigureHandler::ReconfigureHandler(std::shared_ptr<common::components::IComponent> device,
                                       std::shared_ptr<accesscontrol::AccessControl> accessControl)
    : RequestHandlerBase("reconfigure")
    , _device(device)
    , _accessControl(accessControl) { }

middleware::messages::Message ReconfigureHandler::operator()(middleware::messages::Message& request) noexcept{
    middleware::messages::Message response;
    if (!_accessControl->hasWriteAccess(request.header().writerId())) {
        response["retval"] = false;
        response["error"] = "No write access";
    } else {
        auto retval = _device->reconfigure();
        response["retval"] = retval;

        if (!retval) {
            response["error"] = "Device could not be reconfigured";
        }
    }

    return response;
}

FaultHandler::FaultHandler(std::shared_ptr<common::components::IComponent> device,
                           std::shared_ptr<accesscontrol::AccessControl> accessControl)
    : RequestHandlerBase("fault")
    , _device(device)
    , _accessControl(accessControl) { }

middleware::messages::Message FaultHandler::operator()(middleware::messages::Message& request) noexcept {
    middleware::messages::Message response;
    if (!_accessControl->hasWriteAccess(request.header().writerId())) {
        response["retval"] = false;
        response["error"] = "No write access";
    } else {
        auto retval = _device->fault();
        response["retval"] = retval;

        if (!retval) {
            response["error"] = "Device could not be faulted";
        }
    }

    return response;
}

GetHandler::GetHandler(std::shared_ptr<common::components::IComponent> device,
                       std::shared_ptr<accesscontrol::AccessControl> accessControl)
    : RequestHandlerBase("get")
    , _device(device)
    , _accessControl(accessControl) { }

middleware::messages::Message GetHandler::operator()(middleware::messages::Message& request) noexcept {
    middleware::messages::Message response;
    if (!_accessControl->hasReadAccess(request.header().writerId())) {
        response["retval"] = false;
        response["error"] = "No read access";
        return response;
    }

    if (request.body().data().count("node") == 0) {
        response["retval"] = false;
        response["error"] = "Missing node name";
        return response;
    }

    auto nodeStr = request["node"].get<std::string>();
    if (nodeStr == "/") {
        response["retval"] = true;
        response["data"] = _device->settings();
        return response;
    }

    auto splitted = split(nodeStr, "/");
    if (splitted.size() == 0) {
        response["retval"] = false;
        response["error"] = "Invalid node string format: " + nodeStr;
        return response;
    }

    if (splitted[0] == "server") {
        response["retval"] = true;
        response["data"]["access_control"] = _accessControl->currentSlots();
        return response;
    }

    if (!_device->settings().has(splitted[0])) {
        response["retval"] = false;
        response["error"] = "Invalid node name: " + splitted[0];
        return response;
    }

    auto node = _device->settings()[splitted[0]];

    for (size_t i = 1; i < splitted.size(); i++) {
        try {
            node = node->at(splitted[i]);
        } catch (const std::runtime_error&) {
            response["retval"] = false;
            response["error"] = "Invalid node name: " + splitted[i];
            return response;
        }
    }

    response["retval"] = true;
    response["data"] = nlohmann::json();
    node->to_json(response["data"]);

    return response;
}

SetHandler::SetHandler(std::shared_ptr<common::components::IComponent> device,
                       std::shared_ptr<accesscontrol::AccessControl> accessControl)
    : RequestHandlerBase("set")
    , _device(device)
    , _accessControl(accessControl) { }

middleware::messages::Message SetHandler::operator()(middleware::messages::Message& request) noexcept {
    middleware::messages::Message response;
    if (!_accessControl->hasWriteAccess(request.header().writerId())) {
        response["retval"] = false;
        response["error"] = "No write access";

        return response;
    }

    if (request.body().data().count("node") == 0) {
        response["retval"] = false;
        response["error"] = "Missing node name";
        return response;
    }

    auto nodeStr = request["node"].get<std::string>();
    auto splitted = split(nodeStr, "/");
    if (splitted.size() == 0) {
        response["retval"] = false;
        response["error"] = "Invalid node string format: " + nodeStr;
        return response;
    }

    auto node = _device->settings()[splitted[0]];
    for (size_t i = 1; i < splitted.size(); i++) {
        try {
            node = node->at(splitted[i]);
        } catch (const std::runtime_error&) {
            response["retval"] = false;
            response["error"] = "Invalid node name: " + splitted[i];
            return response;
        }
    }

    if (dynamic_cast<common::properties::PropertyNode*>(node.get()) != nullptr) {
        response["retval"] = false;
        response["error"] = "It's not possible to assign a value to a node";
        return response;
    }

    if (node->readonly()) {
        response["retval"] = false;
        response["error"] = "Requested setting is readonly";
    } else {
        try {
            request.body().data().erase("value");
            node->from_json(request.body().data());
            response["retval"] = true;
        } catch (const std::exception& ex) {
            response["retval"] = false;
            response["error"] = ex.what();
        }
    }

    return response;
}

} // namespace communication
} // namespace devices
} // namespace urf