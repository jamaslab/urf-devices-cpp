#pragma once

#include <memory>

#include <urf/common/components/IComponent.hpp>
#include <urf/middleware/messages/Message.hpp>

namespace urf {
namespace devices {
namespace communication {

class SettingsFactory {
 public:
    static std::shared_ptr<common::properties::IObservableProperty> setting(const nlohmann::json& json);
    static common::components::SettingsGroup settingsGroup(const std::string groupName, const nlohmann::json& json);
};

} // namespace communication
} // namespace devices
} // namespace urf
