#include "common/communication/MessageFactory.hpp"
#include "common/utilities/Utilities.hpp"

namespace urf {
namespace devices {
namespace communication {

middleware::messages::Message MessageFactory::acquire(uint8_t userId, accesscontrol::Roles role) {
    middleware::messages::Message request;
    request.header().writerId(userId);
    request["cmd"] = "acquire";
    request["role"] = utilities::roleToString(role);

    return request;
}

middleware::messages::Message MessageFactory::release(uint8_t userId) {
    middleware::messages::Message request;
    request.header().writerId(userId);
    request["cmd"] = "release";

    return request;
}

middleware::messages::Message MessageFactory::changeState(uint8_t userId, common::components::ComponentStateTransitions state) {
    middleware::messages::Message request;
    request.header().writerId(userId);

    switch (state) {
        case common::components::ComponentStateTransitions::ResetFault:
            request["cmd"] = "reset";
            break;
        case common::components::ComponentStateTransitions::Shutdown:
            request["cmd"] = "shutdown";
            break;
        case common::components::ComponentStateTransitions::SwitchOn:
            request["cmd"] = "switchon";
            break;
        case common::components::ComponentStateTransitions::Disable:
            request["cmd"] = "disable";
            break;
        case common::components::ComponentStateTransitions::Enable:
            request["cmd"] = "enable";
            break;
        case common::components::ComponentStateTransitions::Init:
            request["cmd"] = "init";
            break;
        case common::components::ComponentStateTransitions::NoTransition:
            request["cmd"] = "none";
            break;
    }

    return request;
}

middleware::messages::Message MessageFactory::getSetting(uint8_t userId, const std::string& groupName, const std::string& settingName) {
    middleware::messages::Message request;
    request.header().writerId(userId);
    request["cmd"] = "get";
    request["group"] = groupName;
    request["name"] = settingName;

    return request;
}

} // namespace communication
} // namespace devices
} // namespace urf
