#pragma once

#include <urf/common/components/IComponent.hpp>
#include <urf/middleware/messages/Message.hpp>

#include "urf/devices/accesscontrol/AccessControl.hpp"

namespace urf {
namespace devices {
namespace communication {

class MessageFactory {
 public:
    static middleware::messages::Message acquire(uint8_t userId, accesscontrol::Roles role);
    static middleware::messages::Message release(uint8_t userId);

    static middleware::messages::Message changeState(uint8_t userId, common::components::ComponentStateTransitions state);

    static middleware::messages::Message getSetting(uint8_t userId, const std::string& groupName, const std::string& settingName = "ALL");
};

} // namespace communication
} // namespace devices
} // namespace urf
