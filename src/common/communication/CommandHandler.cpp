#include "common/communication/CommandHandler.hpp"

namespace urf {
namespace devices {
namespace communication {

bool CommandHandler::sendCommand(const urf::middleware::messages::Message& request, std::shared_ptr<urf::middleware::sockets::Client> client, const std::chrono::milliseconds& timeout) {
    auto response = client->request(request, timeout);
    if (response.empty()) {
        throw std::runtime_error("Timeout while waiting for response");
    }

     try {
        if (response[0]["retval"].get<bool>()) {
            return true;
        } else {
            if (response[0].body().data().count("error") != 0) {
                throw std::runtime_error("Command rejected: " + response[0]["error"].get<std::string>());
            }
            throw std::runtime_error("Command rejected");
        }
    } catch (const nlohmann::json::exception&) {
        throw std::runtime_error("Something went wrong with the response: " + response[0].body().data().dump());
    }
}

} // namespace communication
} // namespace devices
} // namespace urf
