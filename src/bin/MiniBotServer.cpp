#include <urf/devices/communication/DeviceServer.hpp>
#include <urf/devices/communication/handlers/RobotBasePullHandler.hpp>
#include <urf/devices/robots/robotbase/MiniBot.hpp>
#include <urf/middleware/sockets/Server.hpp>

#include <urf/common/args.hpp>

using namespace urf::devices::robots::robotbase;
using namespace urf::middleware::sockets;
using namespace urf::devices::communication;

int main(int argc, char* argv[]) {
    args::ArgumentParser parser("MiniBot server.");
    args::HelpFlag help(parser, "help", "Display this help menu", {'h', "help"});
    args::ValueFlag<std::string> deviceName(
        parser, "device", "Minibot device port (default: /dev/ttyACM0)", {'d', "device"}, "/dev/ttyACM0");
    args::ValueFlag<std::string> serviceName(
        parser, "service name", "Service name", {'n', "service-name"}, "miniBot");
    args::ValueFlagList<std::string> servers(
        parser, "servers", "List of servers open", {'s', "servers"}, {}, args::Options::Required);
    args::CompletionFlag completion(parser, {"complete"});

    try {
        parser.ParseCLI(argc, argv);
    } catch (const args::Help&) {
        std::cout << parser;
        return 1;
    } catch (const args::Error& e) {
        std::cerr << e.what() << std::endl;
        return -1;
    }

    auto robot = std::make_shared<MiniBot>(args::get(deviceName));
    auto server = std::make_shared<Server>(args::get(serviceName), args::get(servers));

    DeviceServer devServer(server, robot);
    devServer.addPullHandler(std::make_unique<RobotBasePullHandler>(robot, devServer.accessControl()));

    if (!devServer.open()) {
         return -1;
    }

    while(true) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    return 0;
}