#include <urf/devices/communication/DeviceServer.hpp>
#include <urf/devices/communication/handlers/UpdateAllHandler.hpp>
#include <urf/devices/communication/handlers/UpdateAnyHandler.hpp>
#include <urf/devices/communication/i2c/I2CBus.hpp>
#include <urf/devices/sensors/imu/GroveIMU10DoF.hpp>

#include <urf/common/args.hpp>
#include <urf/middleware/sockets/Server.hpp>

#include <iostream>

const std::string BUS_NAME("/dev/i2c-1");

using namespace urf::middleware::sockets;
using namespace urf::devices::communication;

using urf::devices::sensors::imu::GroveIMU10DoF;

int main(int argc, char* argv[]) {
    args::ArgumentParser parser("Grove IMU 10 DOF server.");
    args::HelpFlag help(parser, "help", "Display this help menu", {'h', "help"});
    args::ValueFlag<std::string> busName(
        parser, "i2c", "i2c bus port", {'i', "i2c-bus"}, "", args::Options::Required);
    args::ValueFlag<std::string> serviceName(
        parser, "service name", "Service name", {'n', "service-name"}, "groveImu10DoF");
    args::ValueFlagList<std::string> servers(
        parser, "servers", "List of servers open", {'s', "servers"}, {}, args::Options::Required);
    args::CompletionFlag completion(parser, {"complete"});

    try {
        parser.ParseCLI(argc, argv);
    } catch (const args::Help&) {
        std::cout << parser;
        return 1;
    } catch (const args::Error& e) {
        std::cerr << e.what() << std::endl;
        return -1;
    }

    auto i2cbus = std::make_shared<I2CBus>(args::get(busName));
    if (!i2cbus->open()) {
        std::cout << "Could not open i2c bus" << std::endl;
        return -1;
    }

    auto imu = std::make_shared<GroveIMU10DoF>(i2cbus);
    auto server = std::make_shared<Server>(args::get(serviceName), args::get(servers));

    DeviceServer devServer(server, imu);
    devServer.addUpdateHandler(std::make_unique<UpdateAllHandler>(
        server, "measurements", imu->settings()["measurements"]));
    devServer.addUpdateHandler(std::make_unique<UpdateAnyHandler>(
        server, "configuration", imu->settings()["configuration"], std::chrono::milliseconds(250)));

    if (!devServer.open()) {
        return -1;
    }

    while (true) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    return 0;
}