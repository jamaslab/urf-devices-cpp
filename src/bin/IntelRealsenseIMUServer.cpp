#include <urf/devices/communication/DeviceServer.hpp>
#include <urf/devices/communication/handlers/RgbdCameraHandlers.hpp>
#include <urf/devices/communication/handlers/UpdateAllHandler.hpp>
#include <urf/devices/communication/handlers/UpdateAnyHandler.hpp>
#include <urf/devices/sensors/cameras/rgbd/IntelRealsenseIMU.hpp>
#include <urf/middleware/sockets/Server.hpp>

#include <urf/common/args.hpp>

using namespace urf::devices::sensors::cameras;
using namespace urf::devices::communication;
using namespace urf::middleware::sockets;

int main(int argc, char* argv[]) {
    args::ArgumentParser parser("Intel Realsense IMU server.");
    args::HelpFlag help(parser, "help", "Display this help menu", {'h', "help"});
    args::ValueFlag<int> deviceName(
        parser, "device", "Realsense device number", {'d', "device"}, 0, args::Options::Required);
    args::ValueFlag<std::string> serviceName(
        parser, "service name", "Service name", {'n', "service-name"}, "intelRealsenseIMU");
    args::ValueFlagList<std::string> servers(
        parser, "servers", "List of servers open", {'s', "servers"}, {}, args::Options::Required);
    args::CompletionFlag completion(parser, {"complete"});

    try {
        parser.ParseCLI(argc, argv);
    } catch (const args::Help&) {
        std::cout << parser;
        return 1;
    } catch (const args::Error& e) {
        std::cerr << e.what() << std::endl;
        return -1;
    }

    auto camera = std::make_shared<IntelRealsenseIMU>(args::get(deviceName));
    auto server = std::make_shared<Server>(args::get(serviceName), args::get(servers));

    DeviceServer devServer(server, camera);
    devServer.addRequestHandler(
        "startstream",
        std::make_unique<StartRgbdStreamHandler>(server, camera, devServer.accessControl()));
    devServer.addUpdateHandler(std::make_unique<UpdateAllHandler>(
        server, "measurements", camera->settings()["measurements"]));
    devServer.addUpdateHandler(
        std::make_unique<UpdateAnyHandler>(server,
                                           "configuration",
                                           camera->settings()["configuration"],
                                           std::chrono::milliseconds(250)));

    if (!devServer.open()) {
        return -1;
    }

    while (true) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    return 0;
}