set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(urf_middleware_cpp REQUIRED)
find_package(urf_algorithms_cpp REQUIRED)
find_package(urf_common_cpp REQUIRED)

include_directories()

set(sources
    common/Device.cpp
    common/accesscontrol/AccessControl.cpp
    common/communication/DeviceServer.cpp
    common/communication/handlers/RequestHandlerBase.cpp
    common/communication/handlers/ComponentHandlers.cpp
    common/communication/handlers/UpdateHandlerBase.cpp
    common/communication/handlers/UpdateAllHandler.cpp
    common/communication/handlers/UpdateAnyHandler.cpp
    common/communication/CommandHandler.cpp
    common/communication/MessageFactory.cpp
    common/communication/RemoteDevice.cpp
    common/utilities/Utilities.cpp
)

set(libs
    ${urf_middleware_cpp_LIBRARIES}
    ${urf_algorithms_cpp_LIBRARIES}
    ${urf_common_cpp_LIBRARIES})


set(include_dirs
    ${urf_middleware_cpp_INCLUDE_DIRS}
    ${urf_algorithms_cpp_INCLUDE_DIRS}
    ${urf_common_cpp_INCLUDE_DIRS})

set(headers
    urf/devices/accesscontrol/AccessControl.hpp
    urf/devices/communication/DeviceServer.hpp
    urf/devices/communication/handlers/RequestHandlerBase.hpp
    urf/devices/communication/handlers/ComponentHandlers.hpp
    urf/devices/communication/handlers/UpdateHandlerBase.hpp
    urf/devices/communication/handlers/UpdateAllHandler.hpp
    urf/devices/communication/handlers/UpdateAnyHandler.hpp
    urf/devices/communication/handlers/PullHandlerBase.hpp
    urf/devices/Device.hpp
    urf/devices/sensors/imu/IMU.hpp
)

if (UNIX)
set(sources
    ${sources}
    common/communication/i2c/I2CBus.cpp
    common/communication/i2c/I2CDevice.cpp
    common/interaction/Joypad.cpp
)

set(headers
    ${headers}
    urf/devices/communication/i2c/I2CDevice.hpp
    urf/devices/communication/i2c/I2CBus.hpp
    urf/devices/interaction/Joypad.hpp
)

set(libs
    ${libs}
    stdc++fs)
endif(UNIX)

if (BUILD_IMUS)
    set(sources
        ${sources}
        common/sensors/imu/IMU.cpp
    )

    set(headers
        ${headers}
        urf/devices/sensors/imu/IMU.hpp
    )

    if(UNIX)
        set(sources
            ${sources}
            common/sensors/imu/GroveIMU10DoF.cpp
        )

        set(headers
            ${headers}
            urf/devices/sensors/imu/GroveIMU10DoF.hpp
        )
    endif(UNIX)
endif(BUILD_IMUS)

if (BUILD_CAMERAS)
    set(sources
        ${sources}
        common/communication/handlers/RgbCameraHandlers.cpp
        common/sensors/cameras/rgb/RgbCamera.cpp
    )

    set(headers
        ${headers}
        urf/devices/communication/handlers/RgbCameraHandlers.hpp
        urf/devices/sensors/cameras/rgb/RgbCamera.hpp
    )

    if (UNIX)
        find_package(libuvc_theta REQUIRED)
        set(sources
            ${sources}
            common/sensors/cameras/rgb/RicohTheta.cpp
        )

        set(headers
            ${headers}
            urf/devices/sensors/cameras/rgb/RicohTheta.hpp
        )

        set(libs
            ${libs}
            ${libuvc_theta_LIBRARIES})

        set(include_dirs
            ${include_dirs}
            ${libuvc_theta_INCLUDE_DIRS})
    endif(UNIX)

endif(BUILD_CAMERAS)

if (BUILD_RGBDCAMERAS)
    set(sources
        ${sources}
        common/sensors/cameras/rgbd/RgbdCamera.cpp
        common/communication/handlers/RgbdCameraHandlers.cpp
    )

    set(headers
        ${headers}
        urf/devices/communication/handlers/RgbdCameraHandlers.hpp
        urf/devices/sensors/cameras/rgbd/RgbdCamera.hpp
        urf/devices/sensors/cameras/rgbd/RgbdFrame.hpp
    )

    if (BUILD_REALSENSE)
        find_package(realsense2)
        find_package(libusb REQUIRED)

        set(sources
            ${sources}
            common/sensors/cameras/rgbd/IntelRealsense.cpp
        )

        set(headers
            ${headers}
	        urf/devices/sensors/cameras/rgbd/IntelRealsense.hpp
        )

        if (BUILD_IMUS)
            set(sources
                ${sources}
                common/sensors/cameras/rgbd/IntelRealsenseIMU.cpp
            )

            set(headers
                ${headers}
                urf/devices/sensors/cameras/rgbd/IntelRealsenseIMU.hpp
            )
        endif(BUILD_IMUS)

        set(libs
            ${libs}
            ${realsense2_LIBRARIES}
            ${libusb_LIBRARIES})

        set(include_dirs
            ${include_dirs}
            ${realsense2_INCLUDE_DIRS}
            ${libusb_INCLUDE_DIRS})
    endif()


endif(BUILD_RGBDCAMERAS)

if (BUILD_ROBOTBASES)
find_package(libmodbus REQUIRED)

set(sources
    ${sources}
    common/communication/handlers/RobotBasePullHandler.cpp
    common/robots/robotbase/RobotBase.cpp
    common/robots/robotbase/MiniBot.cpp
)

set(headers
    ${headers}
    urf/devices/communication/handlers/RobotBasePullHandler.hpp
    urf/devices/robots/robotbase/RobotBase.hpp
    urf/devices/robots/robotbase/MiniBot.hpp
)

set(libs
    ${libs}
    ${libmodbus_LIBRARIES})

set(include_dirs
    ${include_dirs}
    ${libmodbus_INCLUDE_DIRS})
endif(BUILD_ROBOTBASES)

if(${JETSON})
    find_package(TegraMM REQUIRED)

    if(${TegraMM_FOUND})
        message(STATUS "Found Tegra Multimedia API")
    else()
        message(STATUS "Could not find Tegra Multimedia API")
    endif()

    if (BUILD_CAMERAS)
        set(sources
            ${sources}
            # common/sensors/cameras/rgb/jetson/JetsonCam.cpp
        )

        set(headers
            ${headers}
            # urf/devices/sensors/cameras/rgb/jetson/JetsonCam.hpp
        )

        set(libs
            ${libs}
            ${TegraMM_LIBRARIES}
        )
        ## This is to avoid the collision with nvidia jpeg
        list(REMOVE_ITEM libs libjpeg-turbo::libjpeg-turbo)
        list(REMOVE_ITEM libs JPEG::JPEG)

        set(include_dirs
            ${include_dirs}
            ${TegraMM_INCLUDE_DIRS}
        )
    endif(BUILD_CAMERAS)
endif(${JETSON})


if (UNIX)
endif(UNIX)

if (WIN32)
endif(WIN32)

add_library(urf_devices ${sources})
target_link_libraries(urf_devices ${libs})
target_include_directories(urf_devices SYSTEM PUBLIC ${include_dirs})
target_include_directories(urf_devices PUBLIC ${PROJECT_SOURCE_DIR}/src/)

if (WIN32)
    include(GenerateExportHeader)
    set(EXPORT_HEADER_PATH ${CMAKE_CURRENT_BINARY_DIR}/urf/devices/urf_devices_export.h)
    generate_export_header(urf_devices EXPORT_FILE_NAME ${EXPORT_HEADER_PATH})
    install(FILES "${EXPORT_HEADER_PATH}" DESTINATION include/urf/devices)
endif(WIN32)

install(TARGETS urf_devices EXPORT urf_devices DESTINATION lib/)

foreach (file ${headers})
    get_filename_component(dir ${file} DIRECTORY)
    install(FILES ${file} DESTINATION include/${dir})
endforeach()

add_subdirectory(bin)
