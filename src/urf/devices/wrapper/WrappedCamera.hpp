#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/devices/urf_devices_export.h"
#else
#    define URF_DEVICES_EXPORT
#endif

#include "urf/devices/wrapper/WrappedDevice.hpp"
#include "urf/devices/sensors/cameras/rgb/RgbCamera.hpp"

namespace urf {
namespace devices {
namespace wrapper {

class URF_DEVICES_EXPORT WrappedCamera : public WrappedDevice, public sensors::cameras::RgbCamera {
 public:
    WrappedCamera() = delete;
    WrappedCamera(const std::string& deviceName,
                  std::shared_ptr<middleware::sockets::Client> client);
    ~WrappedCamera() override = default;

    cv::Mat getFrame() override;

 protected:
    bool resetFaultImpl() override;
    bool switchOnImpl() override;
    bool shutdownImpl() override;
    bool disableImpl() override;
    bool enableImpl() override;
    bool faultImpl() override;
};

} // namespace wrapper
} // namespace devices
} // namespace urf
