#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/devices/urf_devices_export.h"
#else
#    define URF_DEVICES_EXPORT
#endif

#include <atomic>
#include <future>
#include <memory>
#include <string>
#include <unordered_map>

#include "urf/devices/Device.hpp"

#include <urf/middleware/sockets/Client.hpp>

namespace urf {
namespace devices {
namespace wrapper {

class URF_DEVICES_EXPORT WrappedDevice : public virtual Device {
 public:
    WrappedDevice() = delete;
    WrappedDevice(const std::string& deviceName,
                  const std::vector<std::string>& deviceClass,
                  std::shared_ptr<middleware::sockets::Client> client);
    ~WrappedDevice() override;

 protected:
    bool resetFaultImpl() override;
    bool switchOnImpl() override;
    bool shutdownImpl() override;
    bool disableImpl() override;
    bool enableImpl() override;
    bool faultImpl() override;

 protected:
    std::shared_ptr<middleware::sockets::Client> client_;

 private:
    bool stateChangeRequest(const std::string& cmd);
    void subscribeToRequestedValuesChanges();
    void requestWrapperProperties();

 private:
    // std::future<void> requestHandlerFut_;
    // std::atomic<bool> requestHandlerStop_;
};

} // namespace wrapper
} // namespace devices
} // namespace urf
