#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/devices/urf_devices_export.h"
#else
#    define URF_DEVICES_EXPORT
#endif

#include "urf/devices/sensors/imu/IMU.hpp"
#include "urf/devices/communication/i2c/I2CBus.hpp"

#include <atomic>
#include <thread>

#include <urf/algorithms/filters/imu/MadgwickFilter.hpp>

namespace urf {
namespace devices {
namespace sensors {
namespace imu {

class URF_DEVICES_EXPORT GroveIMU10DoF : public IMU {
 public:
    GroveIMU10DoF() = delete;
    explicit GroveIMU10DoF(std::shared_ptr<communication::I2CBus> i2cBus);
    GroveIMU10DoF(const GroveIMU10DoF&) = delete;
    GroveIMU10DoF(GroveIMU10DoF&&) = delete;
    ~GroveIMU10DoF() override;

 private:
    bool resetFaultImpl() override final;
    bool switchOnImpl() override final;
    bool shutdownImpl() override final;
    bool disableImpl() override final;
    bool enableImpl() override final;
    bool faultImpl() override final;

    void dataGrabber();

    bool calibrateMagnetometer();
    bool setClockSource(uint8_t source);
    bool setGyroRange(uint8_t range);
    bool setAccRange(uint8_t range);
    bool setSleepEnabled(bool enabled);
    bool setDHPFMode(uint8_t mode);

    uint8_t getDHPFMode();

 private:
    std::shared_ptr<communication::I2CBus> i2cBus_;
    communication::I2CDevice device_, magnetomerDev_;
    std::unique_ptr<algorithms::filters::MadgwickFilter> filter_;

    std::shared_ptr<common::properties::ObservableSettingList<float>> accConfig_;
    std::shared_ptr<common::properties::ObservableSettingList<float>> gyroConfig_;
    std::shared_ptr<common::properties::ObservableSettingRanged<float>> filterBeta_;

    std::shared_ptr<common::properties::ObservableProperty<std::vector<float>>> accelerations_;
    std::shared_ptr<common::properties::ObservableProperty<std::vector<float>>> gyroscopes_;
    std::shared_ptr<common::properties::ObservableProperty<std::vector<float>>> magnetometers_;
    std::shared_ptr<common::properties::ObservableProperty<std::vector<float>>> attitude_;
    std::shared_ptr<common::properties::ObservableProperty<float>> temperature_;

    float accFactor_;
    float gyroFactor_;
    float magFactor_;
    std::array<float, 3> magCenter_;

    std::atomic<bool> stopDataGrabber_;
    std::thread dataGrabberThread_;
};

} // namespace imu
} // namespace sensors
} // namespace devices
} // namespace urf
