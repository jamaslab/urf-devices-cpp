#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/devices/urf_devices_export.h"
#else
#    define URF_DEVICES_EXPORT
#endif

#include <atomic>
#include <functional>
#include <memory>
#include <string>
#include <thread>

#include <urf/middleware/sockets/Client.hpp>

#include "urf/devices/Device.hpp"
#include "urf/devices/communication/RemoteDevice.hpp"

namespace urf {
namespace devices {
namespace sensors {
namespace imu {

class URF_DEVICES_EXPORT RemoteIMU : public communication::RemoteDevice {
 public:
    RemoteIMU() = delete;
    RemoteIMU(uint8_t userId, const std::string& client);
    ~RemoteIMU() = default;

    void measurementsUpdateFrequency(const std::chrono::milliseconds&);
    std::chrono::milliseconds measurementsUpdateFrequency() const;
 private:
    bool subscribeToTopics() override;

 private:
    std::chrono::milliseconds measurementsUpdateFrequency_;
};

} // namespace imu
} // namespace sensors
} // namespace devices
} // namespace urf
