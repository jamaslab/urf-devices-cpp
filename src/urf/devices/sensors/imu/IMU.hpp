#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/devices/urf_devices_export.h"
#else
#    define URF_DEVICES_EXPORT
#endif

#include "urf/devices/Device.hpp"

namespace urf {
namespace devices {
namespace sensors {
namespace imu {

class URF_DEVICES_EXPORT IMU : public virtual Device {
 public:
    IMU() = delete;
    explicit IMU(const std::string& deviceName);
    virtual ~IMU() = default;

 protected:
    bool resetFaultImpl() override = 0;
    bool switchOnImpl() override = 0;
    bool shutdownImpl() override = 0;
    bool disableImpl() override = 0;
    bool enableImpl() override = 0;
    bool faultImpl() override = 0;

 protected:
    std::shared_ptr<common::properties::PropertyNode> measurements_;
    std::shared_ptr<common::properties::PropertyNode> configuration_;
};

} // namespace imu
} // namespace sensors
} // namespace devices
} // namespace urf
