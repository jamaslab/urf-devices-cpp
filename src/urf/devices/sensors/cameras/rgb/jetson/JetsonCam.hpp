#pragma once

#include <Argus/Argus.h>
#include <EGLStream/NV/ImageNativeBuffer.h>
#include <nvbuf_utils.h>
#include <NvBuffer.h>
#include <EGLStream/EGLStream.h>
#include <NvVideoConverter.h>

#include "urf/devices/sensors/cameras/rgb/RgbCamera.hpp"

namespace urf {
namespace devices {
namespace sensors {
namespace cameras {

class JetsonCam : public RgbCamera {
 public:
    JetsonCam() = delete;
    explicit JetsonCam(uint8_t cameraId);
    JetsonCam(const JetsonCam&) = delete;
    JetsonCam(JetsonCam&&) = delete;
    ~JetsonCam();

    /** State machine related method **/
    bool resetFault() override;
    bool switchOn() override;
    bool shutdown() override;
    bool disable() override;
    bool enable() override;
    bool reconfigure() override;
    bool fault() override;
    devices::ComponentStates currentState() const override;
    devices::ComponentStateTransitions currentTransition() const override;

    cv::Mat getFrame() override;

    bool setResolution(const cv::Size&) override;
    bool setFramerate(int fps) override;
    bool setZoom(float zoom) override;
    bool setPosition(float pan, float tilt) override;
    bool setExposure(float exposure) override;
    bool setShutterSpeed(float speed) override;
    bool enableAutofocus(bool autofocus) override;
    bool setFocus(float focus) override;
    bool setISO(int iso) override;

    std::optional<cv::Size> getResolution() override;
    std::optional<int> getFramerate() override;
    std::optional<float> getZoom() override;
    std::optional<float> getPan() override;
    std::optional<float> getTilt() override;
    std::optional<float> getExposure() override;
    std::optional<float> getShutterSpeed() override;
    std::optional<bool> autofocusEnabled() override;
    std::optional<float> getFocus() override;
    std::optional<int> getISO() override;

    std::vector<cv::Size> getAvailableResolutions() override;

private:
    uint8_t cameraId_;

    cv::Size requestedResolution_;
    int requestedFps_;

    Argus::UniqueObj<Argus::CameraProvider> cameraProvider_;
    Argus::UniqueObj<Argus::CaptureSession> captureSession_;
    Argus::UniqueObj<Argus::OutputStream> stream_;
    Argus::UniqueObj<EGLStream::FrameConsumer> frameConsumer_;
    Argus::ISourceSettings* sourceSettings_;
    Argus::CameraDevice* cameraDevice_;
};

}  // namespace cameras
}  // namespace sensors
}  // namespace devices
}  // namespace urf
