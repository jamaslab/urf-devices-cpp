#pragma once

#include <memory>
#include <libuvc-theta/libuvc.h>

#include "urf/devices/sensors/cameras/rgb/RgbCamera.hpp"

namespace urf {
namespace devices {
namespace sensors {
namespace cameras {

class RicohTheta : public RgbCamera {
 public:
    RicohTheta() = delete;
    explicit RicohTheta(uint8_t cameraId);
    RicohTheta(const RicohTheta&) = delete;
    RicohTheta(RicohTheta&&) = delete;
    ~RicohTheta() override;

    algorithms::compression::VideoFrame getFrame() override;

private:
    bool resetFaultImpl() override final;
    bool switchOnImpl() override final;
    bool shutdownImpl() override final;
    bool disableImpl() override final;
    bool enableImpl() override final;
    bool faultImpl() override final;

    bool findDevice();

private:
    uvc_theta_context_t* context_;
    uvc_theta_device_t* device_;
    uvc_theta_stream_ctrl_t stream_;
    uvc_theta_device_handle_t* deviceHandle_;
    uvc_theta_device_descriptor_t* descriptor_;

    std::shared_ptr<common::properties::ObservableSetting<uint8_t>> cameraIdConfig_;
    std::shared_ptr<common::properties::ObservableSettingList<StreamProfile>> resolutionConfig_;

    void* streamCtx_;
};

}  // namespace cameras
}  // namespace sensors
}  // namespace devices
}  // namespace urf
