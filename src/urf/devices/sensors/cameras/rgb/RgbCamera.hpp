#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/devices/urf_devices_export.h"
#else
    #define URF_DEVICES_EXPORT
#endif

#include <optional>
#include <vector>

#include "urf/devices/Device.hpp"

#include <urf/algorithms/compression/VideoFrame.hpp>

namespace urf {
namespace devices {
namespace sensors {
namespace cameras {

struct StreamProfile {
    StreamProfile() = default;
    StreamProfile(const algorithms::compression::VideoResolution& res, uint32_t f = 0);
    StreamProfile(const StreamProfile&) = default;

    algorithms::compression::VideoResolution resolution;
    uint32_t fps;

    std::optional<std::pair<float, float>> focalPoint;
    std::optional<std::pair<float, float>> principalPoint;

    bool operator==(const StreamProfile &b) const;
    bool operator!=(const StreamProfile &b) const;

    bool operator<(const StreamProfile &b) const;
    bool operator>(const StreamProfile &b) const;

    friend void to_json(nlohmann::json& j, const StreamProfile& p);
    friend void from_json(const nlohmann::json& j, StreamProfile& p);
};

class URF_DEVICES_EXPORT RgbCamera : public virtual Device {
 public:
    RgbCamera() = delete;
    explicit RgbCamera(const std::string& deviceName);
    virtual ~RgbCamera() = default;

    virtual algorithms::compression::VideoFrame getFrame() = 0;

 protected:
    bool resetFaultImpl() override = 0;
    bool switchOnImpl() override = 0;
    bool shutdownImpl() override = 0;
    bool disableImpl() override = 0;
    bool enableImpl() override = 0;
    bool faultImpl() override = 0;

 protected:
    std::shared_ptr<common::properties::PropertyNode> configuration_;
};

}  // namespace cameras
}  // namespace sensors
}  // namespace devices
}  // namespace urf
