#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/devices/urf_devices_export.h"
#else
    #define URF_DEVICES_EXPORT
#endif

#include <optional>
#include <vector>


#include "urf/devices/Device.hpp"
#include "urf/devices/sensors/cameras/rgbd/RgbdFrame.hpp"

namespace urf {
namespace devices {
namespace sensors {
namespace cameras {

class URF_DEVICES_EXPORT RgbdCamera : public virtual Device {
 public:
    RgbdCamera(const std::string& deviceName);
    virtual ~RgbdCamera() = default;

    virtual RgbdFrame getFrame() = 0;

 protected:
    bool resetFaultImpl() override = 0;
    bool switchOnImpl() override = 0;
    bool shutdownImpl() override = 0;
    bool disableImpl() override = 0;
    bool enableImpl() override = 0;
    bool faultImpl() override = 0;

 protected:
    std::shared_ptr<common::properties::PropertyNode> configuration_;
};

}  // namespace cameras
}  // namespace sensors
}  // namespace devices
}  // namespace urf
