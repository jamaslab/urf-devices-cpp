#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/devices/urf_devices_export.h"
#else
#    define URF_DEVICES_EXPORT
#endif

#include <atomic>
#include <functional>
#include <memory>
#include <string>
#include <thread>

#include <urf/middleware/sockets/Client.hpp>

#include "urf/devices/Device.hpp"
#include "urf/devices/communication/RemoteDevice.hpp"
#include "urf/devices/sensors/cameras/rgbd/RgbdCamera.hpp"

namespace urf {
namespace devices {
namespace sensors {
namespace cameras {

struct RemoteStreamProfile {
    std::optional<cv::Size> resolution;
    float quality;
    std::string compression;
};

class URF_DEVICES_EXPORT RemoteRgbdCamera : public RgbdCamera {
 public:
    RemoteRgbdCamera() = delete;
    RemoteRgbdCamera(uint8_t userId, const std::string& client);
    ~RemoteRgbdCamera() = default;

    bool open();
    bool close();

    bool acquire(accesscontrol::Roles role);
    bool release();

    void setTimeout(const std::chrono::milliseconds& timeout);
    std::chrono::milliseconds getTimeout();

    RgbdFrame getFrame() override;

    ComponentStates currentState() const override;
    ComponentStateTransitions currentTransition() const override;

    std::string componentName() const override;
    std::vector<std::string> componentClass() const override;

    std::unordered_map<std::string, common::components::SettingsGroup> settings() override;

    bool startStream();
    bool stopStream();

 private:
    bool resetFaultImpl() override;
    bool switchOnImpl() override;
    bool shutdownImpl() override;
    bool disableImpl() override;
    bool enableImpl() override;
    bool faultImpl() override;

    bool updateCameraSettings();

 private:
    std::shared_ptr<middleware::sockets::Client> client_;
    uint8_t userId_;

    std::chrono::milliseconds timeout_;
    std::unordered_map<std::string, common::components::SettingsGroup> settings_;

    std::condition_variable frameCv_;
    std::mutex frameMtx_;
    bool frameSetFlag_;
    RgbdFrame latestFrame_;
};

} // namespace imu
} // namespace sensors
} // namespace devices
} // namespace urf
