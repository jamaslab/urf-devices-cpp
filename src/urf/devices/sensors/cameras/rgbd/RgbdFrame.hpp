#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/devices/urf_devices_export.h"
#else
#    define URF_DEVICES_EXPORT
#endif

#include <optional>

#include <pcl/pcl_base.h>
#include <pcl/point_types.h>

#include <urf/algorithms/compression/VideoFrame.hpp>

namespace urf {
namespace devices {
namespace sensors {
namespace cameras {

struct URF_DEVICES_EXPORT RgbdFrame {
    RgbdFrame()
        : colorFrame(std::nullopt)
        , depthFrame(std::nullopt)
        , pointCloud(std::nullopt) { }

    RgbdFrame(std::optional<algorithms::compression::VideoFrame> rgb,
              std::optional<algorithms::compression::VideoFrame> depth,
              std::optional<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> cloud)
        : colorFrame(rgb)
        , depthFrame(depth)
        , pointCloud(cloud) { }

    std::optional<algorithms::compression::VideoFrame> colorFrame;
    std::optional<algorithms::compression::VideoFrame> depthFrame;
    std::optional<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> pointCloud;

    static RgbdFrame empty() {
        return RgbdFrame();
    }
};

} // namespace cameras
} // namespace sensors
} // namespace devices
} // namespace urf
