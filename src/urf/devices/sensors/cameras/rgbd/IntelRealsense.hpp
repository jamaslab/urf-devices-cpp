#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/devices/urf_devices_export.h"
#else
    #define URF_DEVICES_EXPORT
#endif

#define _SILENCE_CXX17_ITERATOR_BASE_CLASS_DEPRECATION_WARNING
#define _SILENCE_CXX17_ADAPTOR_TYPEDEFS_DEPRECATION_WARNING

#include "urf/devices/sensors/cameras/rgbd/RgbdCamera.hpp"
#include "urf/devices/sensors/cameras/rgb/RgbCamera.hpp"

#include <Eigen/Core>
#include <librealsense2/rs.hpp>

namespace urf {
namespace devices {
namespace sensors {
namespace cameras {

class URF_DEVICES_EXPORT IntelRealsense : public RgbdCamera {
 public:
    IntelRealsense() = delete;
    explicit IntelRealsense(int index);
    // explicit IntelRealsense(const std::string& serialNumber);
    ~IntelRealsense();

    RgbdFrame getFrame() override final;

 protected:
    bool resetFaultImpl() override final;
    bool switchOnImpl() override final;
    bool shutdownImpl() override final;
    bool disableImpl() override final;
    bool enableImpl() override final;
    bool faultImpl() override final;

 private:
    std::shared_ptr<common::properties::ObservableSetting<uint8_t>> cameraIdConfig_;
    std::shared_ptr<common::properties::ObservableProperty<std::string>> serialNumber_;
    std::shared_ptr<common::properties::ObservableProperty<std::string>> cameraModel_;

    std::shared_ptr<common::properties::ObservableSettingList<StreamProfile>> colorResolution_;
    std::shared_ptr<common::properties::ObservableSettingList<StreamProfile>> depthResolution_;
    std::shared_ptr<common::properties::ObservableProperty<float>> depthScale_;

    std::shared_ptr<common::properties::ObservableSetting<bool>> colorActive_;
    std::shared_ptr<common::properties::ObservableSetting<bool>> depthActive_;
    std::shared_ptr<common::properties::ObservableSetting<bool>> pointCloudActive_;
    std::shared_ptr<common::properties::ObservableSetting<bool>> alignDepth_;

    std::shared_ptr<common::properties::ObservableProperty<Eigen::MatrixXf>> extrinsics_;

    rs2::device device_;
    rs2::pipeline pipe_;
    rs2::sensor colorSensor_;
    rs2::sensor depthSensor_;
    rs2::align alignToColor_;

    std::mutex pipeMtx_;

    static std::unique_ptr<rs2::context> rsCtx_;
    static size_t ctxRefCount_;
};

}  // namespace cameras
}  // namespace sensors
}  // namespace devices
}  // namespace urf
