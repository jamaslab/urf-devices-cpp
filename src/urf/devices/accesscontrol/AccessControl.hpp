#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/devices/urf_devices_export.h"
#else
    #define URF_DEVICES_EXPORT
#endif

#include <cstdint>
#include <mutex>
#include <unordered_map>
#include <optional>
#include <string>
#include <utility>

namespace urf {
namespace devices {
namespace accesscontrol {

enum class Roles {
    VIEWER = 0,
    USER = 1,
    EXPERT = 2,
    SUDO = 3
};

class URF_DEVICES_EXPORT AccessControl {
 public:
    AccessControl() = delete;
    explicit AccessControl(size_t maxSlots);
    AccessControl(const AccessControl&) = delete;
    AccessControl(AccessControl&&) = delete;

    std::optional<uint8_t> acquireSlot(const std::string& username, Roles role);
    bool releaseSlot(uint8_t userId);

    bool hasReadAccess(uint8_t userId) const;
    bool hasWriteAccess(uint8_t userId) const;

    bool hasReadAccess(const std::string& user) const;
    bool hasWriteAccess(const std::string& user) const;

    std::unordered_map<std::string, uint8_t> currentSlots() const;

 private:
    mutable std::mutex slotsMtx_;
    std::unordered_map<std::string, uint8_t> currentSlots_;
    std::unordered_map<uint8_t, Roles> currentRoles_;

    std::optional<uint8_t> currentlyWriting_;

    size_t maxSlots_;
};

}  // namespace accesscontrol
}  // namespace devices
}  // namespace urf
