#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/devices/urf_devices_export.h"
#else
#    define URF_DEVICES_EXPORT
#endif

#include <atomic>
#include <map>
#include <thread>
#include <vector>

#include "urf/devices/Device.hpp"

namespace urf {
namespace devices {
namespace interaction {

class URF_DEVICES_EXPORT Joypad : public Device {
 public:
    Joypad() = delete;
    explicit Joypad(int joypadNumber);
    virtual ~Joypad() = default;

    static std::vector<int> availableJoypads();

 private:
    bool resetFaultImpl() override;
    bool switchOnImpl() override;
    bool shutdownImpl() override;
    bool disableImpl() override;
    bool enableImpl() override;
    bool faultImpl() override;

    void loop();

    size_t getButtonsCount();
    size_t getAxesCount();
    std::string getJoypadName();

 private:
    typedef struct JoypadEvent {
        /**
         * The timestamp of the event, in milliseconds.
         */
        unsigned int time;

        /**
         * The value associated with this joystick event.
         * For buttons this will be either 1 (down) or 0 (up).
         * For axes, this will range between MIN_AXES_VALUE and MAX_AXES_VALUE.
         */
        short value;

        /**
         * The event type.
         */
        unsigned char type;

        /**
         * The axis/button number.
         */
        unsigned char number;
    } JoypadEvent;

 private:
    int fd_;

    std::atomic<bool> stopThread_;
    std::thread joypadEventThread_;

    std::shared_ptr<common::properties::ObservableSetting<int>> buttonsCount_;
    std::shared_ptr<common::properties::ObservableSetting<int>> axesCount_;
    std::shared_ptr<common::properties::ObservableSetting<std::string>> joypadName_;
    std::shared_ptr<common::properties::ObservableSetting<int>> joypadId_;

    std::shared_ptr<common::properties::ObservableProperty<nlohmann::json>> axesPosition_;
    std::shared_ptr<common::properties::ObservableProperty<nlohmann::json>> buttonsPressed_;

    std::shared_ptr<common::properties::PropertyNode> configurations_;
    std::shared_ptr<common::properties::PropertyNode> measurements_;
};

} // namespace interaction
} // namespace devices
} // namespace urf
