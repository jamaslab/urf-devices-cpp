#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/devices/urf_devices_export.h"
#else
#    define URF_DEVICES_EXPORT
#endif

#include <functional>
#include <iostream>
#include <memory>
#include <optional>
#include <string>
#include <unordered_map>

#include <urf/common/components/IComponent.hpp>

namespace urf {
namespace devices {

typedef common::components::ComponentStates ComponentStates;
typedef common::components::ComponentStateTransitions ComponentStateTransitions;

class URF_DEVICES_EXPORT Device : public common::components::IComponent {
 public:
    Device() = delete;
    Device(const std::string& deviceName, const std::vector<std::string>& deviceClass);
    Device(const Device&) = delete;
    Device(Device&&) = delete;
    virtual ~Device() = default;

    /** Goes to ready to switch on **/
    bool resetFault() noexcept override final;

    /** Goes to switched on **/
    bool switchOn() noexcept override final;

    /** Goes to ready to Switch On **/
    bool shutdown() noexcept override final;

    /** Goes to switched on **/
    bool disable() noexcept override final;

    /** Goes to enabled **/
    bool enable() noexcept override final;

    /** Goes to switched on and back to enabled without changing currentState unless it can't re-enable**/
    bool reconfigure() noexcept override final;

    /** A fault can be manually triggered due to external conditions **/
    bool fault() noexcept override final;

    ComponentStates currentState() const override;
    ComponentStateTransitions currentTransition() const override;

    std::string componentName() const override;
    std::vector<std::string> componentClass() const override;

    common::properties::PropertyNode settings() const override;

 protected:
    virtual bool resetFaultImpl() = 0;
    virtual bool switchOnImpl() = 0;
    virtual bool shutdownImpl() = 0;
    virtual bool disableImpl() = 0;
    virtual bool enableImpl() = 0;
    virtual bool faultImpl() = 0;

 protected:
    std::shared_ptr<common::properties::ObservableProperty<common::components::StatePair>> state_;

    common::properties::PropertyNode settings_;

 private:
    bool startTransition(ComponentStateTransitions transition);
    bool failTransition();
    bool transitionCompleted();

 private:
    std::shared_ptr<common::properties::ObservableProperty<std::string>> componentName_;
    std::shared_ptr<common::properties::ObservableProperty<std::vector<std::string>>>
        componentClass_;
};

} // namespace devices
} // namespace urf
