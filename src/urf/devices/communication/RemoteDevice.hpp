#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/devices/urf_devices_export.h"
#else
    #define URF_DEVICES_EXPORT
#endif

#include <atomic>
#include <functional>
#include <memory>
#include <string>
#include <thread>

#include <urf/middleware/sockets/Client.hpp>

#include "urf/devices/Device.hpp"
#include "urf/devices/accesscontrol/AccessControl.hpp"

namespace urf {
namespace devices {
namespace communication {

class URF_DEVICES_EXPORT RemoteDevice : public common::components::IComponent {
 public:
    RemoteDevice() = delete;
    RemoteDevice(std::shared_ptr<middleware::sockets::Client> client);
    RemoteDevice(const std::string& client);
    ~RemoteDevice() = default;

    bool open();
    bool close();

    bool resetFault() noexcept override;
    bool switchOn() noexcept override;
    bool shutdown() noexcept override;
    bool disable() noexcept override;
    bool enable() noexcept override;
    bool reconfigure() noexcept override;
    bool fault() noexcept override;

    bool acquire(const std::string& username, accesscontrol::Roles role);
    bool release();

    inline void setTimeout(const std::chrono::milliseconds& timeout) { timeout_ = timeout; }
    inline std::chrono::milliseconds getTimeout() { return timeout_; }

    ComponentStates currentState() const override;
    ComponentStateTransitions currentTransition() const override;

    std::string componentName() const override;
    std::vector<std::string> componentClass() const override;

    common::properties::PropertyNode settings() const override;

 protected:
    bool stateChangeRequest(const std::string& cmd);

 protected:
    std::shared_ptr<middleware::sockets::Client> client_;
    uint8_t userId_;

    std::chrono::milliseconds timeout_;
    common::properties::PropertyNode settings_;
};

}  // namespace communication
}  // namespace devices
}  // namespace urf
