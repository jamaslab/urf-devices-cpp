#pragma once

#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <sys/types.h>

#include <mutex>
#include <string>
#include <vector>

#include "urf/devices/communication/i2c/I2CDevice.hpp"

namespace urf {
namespace devices {
namespace communication {

class I2CBus {
 public:
    I2CBus() = delete;
    explicit I2CBus(const std::string& name);
    I2CBus(const I2CBus&) = delete;
    I2CBus(I2CBus&&) = delete;
    ~I2CBus();

    bool open();
    bool close();

    bool isOpen();

    bool write(const I2CDevice& device, unsigned int iaddr, const std::vector<uint8_t>& buffer);
    bool writeBits(const I2CDevice& device, unsigned int iaddr, uint8_t data, uint8_t startBit, uint8_t len);

    std::vector<uint8_t> read(const I2CDevice& device, uint32_t iaddr, size_t len);
    uint8_t readBits(const I2CDevice& device, uint32_t iaddr, uint8_t startBit, uint8_t len);

 private:
    void addressConvert(uint32_t iaddr, uint32_t len, unsigned char* addr);
    bool select(uint32_t devAddr, uint32_t tenbit);

 private:
    std::string name_;
    int fd_;
    std::mutex busMtx_;
};

} // namespace communication
} // namespace devices
} // namespace urf
