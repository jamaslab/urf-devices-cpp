#pragma once

#include <cstdint>

namespace urf {
namespace devices {
namespace communication {

class I2CDevice {
 public:
    I2CDevice() = delete;
    I2CDevice(uint16_t addr,
              uint8_t tenbit = 0,
              uint8_t delay = 1,
              uint16_t flags = 0,
              uint32_t page_bytes = 8,
              uint32_t iaddr_bytes = 1);

    uint16_t address() const {
        return addr_;
    }
    uint8_t tenbit() const {
        return tenbit_;
    }
    uint8_t delay() const {
        return delay_;
    }
    uint16_t flags() const {
        return flags_;
    }
    uint32_t pageBytes() const {
        return page_bytes_;
    }

    uint32_t iaddrBytes() const {
        return iaddr_bytes_;
    }

 private:
    uint16_t addr_;
    uint8_t tenbit_;
    uint8_t delay_;
    uint16_t flags_;
    uint32_t page_bytes_;
    uint32_t iaddr_bytes_;
};

} // namespace communication
} // namespace devices
} // namespace urf
