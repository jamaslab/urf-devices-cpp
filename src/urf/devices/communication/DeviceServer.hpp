#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/devices/urf_devices_export.h"
#else
    #define URF_DEVICES_EXPORT
#endif

#include <atomic>
#include <functional>
#include <unordered_map>
#include <memory>
#include <thread>

#include <urf/middleware/sockets/Server.hpp>

#include "urf/devices/accesscontrol/AccessControl.hpp"
#include "urf/devices/communication/handlers/RequestHandlerBase.hpp"
#include "urf/devices/communication/handlers/PullHandlerBase.hpp"
#include "urf/devices/communication/handlers/UpdateHandlerBase.hpp"
#include "urf/devices/Device.hpp"

#define DEFAULT_MAX_SLOTS 5

namespace urf {
namespace devices {
namespace communication {

class URF_DEVICES_EXPORT DeviceServer {
 public:
    DeviceServer() = delete;
    DeviceServer(std::shared_ptr<middleware::sockets::Server> server,
        std::shared_ptr<common::components::IComponent> device,
        size_t maxSlots = DEFAULT_MAX_SLOTS);
    virtual ~DeviceServer() = default;

    virtual bool open();
    virtual bool close();

    std::shared_ptr<accesscontrol::AccessControl> accessControl() const noexcept;

    void addRequestHandler(const std::string& request, std::unique_ptr<RequestHandlerBase> requestHandler) noexcept;
    void addPullHandler(std::unique_ptr<PullHandlerBase> pullHandler) noexcept;
    void addUpdateHandler(std::unique_ptr<UpdateHandlerBase> updateHandler) noexcept;

 protected:
    bool isOpen_;
    std::shared_ptr<middleware::sockets::Server> server_;
    std::shared_ptr<common::components::IComponent> device_;
    std::shared_ptr<accesscontrol::AccessControl> accessControl_;
    
    std::atomic<bool> stopThreads_;
    std::unordered_map<std::string, std::unique_ptr<RequestHandlerBase>> requestHandlers_;
    std::vector<std::unique_ptr<PullHandlerBase>> pullHandlers_;
    std::vector<std::unique_ptr<UpdateHandlerBase>> updateHandlers_;

 private:
    void requestHandler();
    void sendStateUpdate();

 private:
    std::thread requestHandlerThread_;
};

}  // namespace communication
}  // namespace devices
}  // namespace urf
