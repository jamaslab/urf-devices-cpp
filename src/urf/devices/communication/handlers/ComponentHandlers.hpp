#pragma once

#include <memory>

#include <urf/common/components/IComponent.hpp>
#include <urf/middleware/messages/Message.hpp>

#include <urf/devices/accesscontrol/AccessControl.hpp>
#include <urf/devices/communication/handlers/RequestHandlerBase.hpp>

namespace urf {
namespace devices {
namespace communication {

class AcquireSlotHandler : public RequestHandlerBase {
 public:
    AcquireSlotHandler(std::shared_ptr<accesscontrol::AccessControl> accessControl);
    ~AcquireSlotHandler() override = default;

    middleware::messages::Message operator()(middleware::messages::Message& request) noexcept override final;

 private:
    std::shared_ptr<accesscontrol::AccessControl> _accessControl;
};

class ReleaseSlotHandler : public RequestHandlerBase {
 public:
    ReleaseSlotHandler(std::shared_ptr<accesscontrol::AccessControl> accessControl);
    ~ReleaseSlotHandler() override = default;

    middleware::messages::Message operator()(middleware::messages::Message& request) noexcept override final;

 private:
    std::shared_ptr<accesscontrol::AccessControl> _accessControl;
};

class ResetFaultHandler : public RequestHandlerBase {
 public:
    ResetFaultHandler(std::shared_ptr<common::components::IComponent> device, std::shared_ptr<accesscontrol::AccessControl> accessControl);
    ~ResetFaultHandler() override = default;

    middleware::messages::Message operator()(middleware::messages::Message& request) noexcept override final;

 private:
    std::shared_ptr<common::components::IComponent> _device;
    std::shared_ptr<accesscontrol::AccessControl> _accessControl;
};

class SwitchOnHandler : public RequestHandlerBase {
 public:
    SwitchOnHandler(std::shared_ptr<common::components::IComponent> device, std::shared_ptr<accesscontrol::AccessControl> accessControl);
    ~SwitchOnHandler() override = default;

    middleware::messages::Message operator()(middleware::messages::Message& request) noexcept override final;

 private:
    std::shared_ptr<common::components::IComponent> _device;
    std::shared_ptr<accesscontrol::AccessControl> _accessControl;
};

class ShutdownHandler : public RequestHandlerBase {
 public:
    ShutdownHandler(std::shared_ptr<common::components::IComponent> device, std::shared_ptr<accesscontrol::AccessControl> accessControl);
    ~ShutdownHandler() override = default;

    middleware::messages::Message operator()(middleware::messages::Message& request) noexcept override final;

 private:
    std::shared_ptr<common::components::IComponent> _device;
    std::shared_ptr<accesscontrol::AccessControl> _accessControl;
};

class DisableHandler : public RequestHandlerBase {
 public:
    DisableHandler(std::shared_ptr<common::components::IComponent> device, std::shared_ptr<accesscontrol::AccessControl> accessControl);
    ~DisableHandler() override = default;

    middleware::messages::Message operator()(middleware::messages::Message& request) noexcept override final;

 private:
    std::shared_ptr<common::components::IComponent> _device;
    std::shared_ptr<accesscontrol::AccessControl> _accessControl;
};

class EnableHandler : public RequestHandlerBase {
 public:
    EnableHandler(std::shared_ptr<common::components::IComponent> device, std::shared_ptr<accesscontrol::AccessControl> accessControl);
    ~EnableHandler() override = default;

    middleware::messages::Message operator()(middleware::messages::Message& request) noexcept override final;

 private:
    std::shared_ptr<common::components::IComponent> _device;
    std::shared_ptr<accesscontrol::AccessControl> _accessControl;
};

class ReconfigureHandler : public RequestHandlerBase {
 public:
    ReconfigureHandler(std::shared_ptr<common::components::IComponent> device, std::shared_ptr<accesscontrol::AccessControl> accessControl);
    ~ReconfigureHandler() override = default;

    middleware::messages::Message operator()(middleware::messages::Message& request) noexcept override final;

 private:
    std::shared_ptr<common::components::IComponent> _device;
    std::shared_ptr<accesscontrol::AccessControl> _accessControl;
};

class FaultHandler : public RequestHandlerBase {
 public:
    FaultHandler(std::shared_ptr<common::components::IComponent> device, std::shared_ptr<accesscontrol::AccessControl> accessControl);
    ~FaultHandler() override = default;

    middleware::messages::Message operator()(middleware::messages::Message& request) noexcept override final;

 private:
    std::shared_ptr<common::components::IComponent> _device;
    std::shared_ptr<accesscontrol::AccessControl> _accessControl;
};

class GetHandler : public RequestHandlerBase {
 public:
    GetHandler(std::shared_ptr<common::components::IComponent> device, std::shared_ptr<accesscontrol::AccessControl> accessControl);
    ~GetHandler() override = default;

    middleware::messages::Message operator()(middleware::messages::Message& request) noexcept override final;

 private:
    std::shared_ptr<common::components::IComponent> _device;
    std::shared_ptr<accesscontrol::AccessControl> _accessControl;
};

class SetHandler : public RequestHandlerBase {
 public:
    SetHandler(std::shared_ptr<common::components::IComponent> device, std::shared_ptr<accesscontrol::AccessControl> accessControl);
    ~SetHandler() override = default;

    middleware::messages::Message operator()(middleware::messages::Message& request) noexcept override final;

 private:
    std::shared_ptr<common::components::IComponent> _device;
    std::shared_ptr<accesscontrol::AccessControl> _accessControl;
};

}  // namespace communication
}  // namespace devices
}  // namespace urf