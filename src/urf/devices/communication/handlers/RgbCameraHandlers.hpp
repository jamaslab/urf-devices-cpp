#pragma once

#include <atomic>
#include <memory>
#include <mutex>
#include <thread>

#include <urf/algorithms/compression/IVideoEncoder.hpp>

#include <urf/common/components/IComponent.hpp>

#include <urf/middleware/messages/Message.hpp>
#include <urf/middleware/sockets/ISocket.hpp>

#include <urf/devices/accesscontrol/AccessControl.hpp>
#include <urf/devices/communication/handlers/RequestHandlerBase.hpp>
#include <urf/devices/sensors/cameras/rgb/RgbCamera.hpp>

namespace urf {
namespace devices {
namespace communication {

struct RgbVideoServerConfig {
    std::thread videoServerThread;
    std::unique_ptr<algorithms::compression::IVideoEncoder> encoder;
    std::string topic;
    algorithms::compression::VideoResolution resolution;
    uint32_t fps;
    std::atomic<bool> ended;
};

class GetRgbFrameHandler : public RequestHandlerBase {
 public:
    GetRgbFrameHandler(std::shared_ptr<common::components::IComponent> device,
                       std::shared_ptr<accesscontrol::AccessControl> accessControl);
    ~GetRgbFrameHandler() override = default;

    middleware::messages::Message
    operator()(middleware::messages::Message& request) noexcept override final;

 private:
    std::shared_ptr<common::components::IComponent> _device;
    std::shared_ptr<accesscontrol::AccessControl> _accessControl;
};

class StartRgbStreamHandler : public RequestHandlerBase {
 public:
    StartRgbStreamHandler(std::shared_ptr<middleware::sockets::ISocket> socket,
                          std::shared_ptr<sensors::cameras::RgbCamera> device,
                          std::shared_ptr<accesscontrol::AccessControl> accessControl);
    ~StartRgbStreamHandler() override;

    middleware::messages::Message
    operator()(middleware::messages::Message& request) noexcept override final;
 
 private:
   void videoServer(std::shared_ptr<RgbVideoServerConfig> config);
   void frameGrabber();

 private:
    std::shared_ptr<middleware::sockets::ISocket> _socket;
    std::shared_ptr<sensors::cameras::RgbCamera> _device;
    std::shared_ptr<accesscontrol::AccessControl> _accessControl;

    std::thread _frameGrabberThread;
    std::atomic<bool> _frameGrabberStarted;
    std::shared_mutex _frameMtx;
    algorithms::compression::VideoFrame _latestFrame; // Maybe here it can become a triple buffer

    std::mutex _videoServersMtx;
    std::vector<std::shared_ptr<RgbVideoServerConfig>> _videoServers;
};

} // namespace communication
} // namespace devices
} // namespace urf