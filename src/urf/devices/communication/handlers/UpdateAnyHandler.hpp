#pragma once

#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/devices/urf_devices_export.h"
#else
#    define URF_DEVICES_EXPORT
#endif

#include "urf/devices/communication/handlers/UpdateHandlerBase.hpp"

#include <urf/common/containers/ThreadSafeQueue.hpp>

namespace urf {
namespace devices {
namespace communication {

class URF_DEVICES_EXPORT UpdateAnyHandler : public UpdateHandlerBase {
 public:
    UpdateAnyHandler(std::shared_ptr<middleware::sockets::ISocket> socket,
                     const std::string& topic,
                     std::shared_ptr<common::properties::IObservableProperty> property,
                     std::chrono::duration<double> updateDelay = std::chrono::milliseconds(0));
    UpdateAnyHandler(const UpdateAnyHandler&) = default;
    UpdateAnyHandler(UpdateAnyHandler&&) = default;
    ~UpdateAnyHandler() override = default;

    UpdateAnyHandler& operator=(const UpdateAnyHandler&) = default;
    UpdateAnyHandler& operator=(UpdateAnyHandler&&) = default;

 private:
    void subscribeToValueChanges(const std::string& propertyName,
                                 std::shared_ptr<common::properties::IObservableProperty> property);

 private:
    std::chrono::duration<double> _updateDelay;
    std::future<void> _updateFuture;

    std::mutex _msgMutex;
    middleware::messages::Message _updateMessage;
};

} // namespace communication
} // namespace devices
} // namespace urf
