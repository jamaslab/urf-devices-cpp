#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/devices/urf_devices_export.h"
#else
#    define URF_DEVICES_EXPORT
#endif

#include <urf/middleware/sockets/ISocket.hpp>
#include <urf/common/properties/ObservableProperty.hpp>

namespace urf {
namespace devices {
namespace communication {

class URF_DEVICES_EXPORT UpdateHandlerBase {
 public:
    UpdateHandlerBase(std::shared_ptr<middleware::sockets::ISocket> socket,
                        const std::string& topic,
                        std::shared_ptr<common::properties::IObservableProperty> property);
    UpdateHandlerBase(const UpdateHandlerBase&) = default;
    UpdateHandlerBase(UpdateHandlerBase&&) = default;
    virtual ~UpdateHandlerBase() = default;

    std::string topic() const noexcept;

    UpdateHandlerBase& operator=(const UpdateHandlerBase&) = default;
    UpdateHandlerBase& operator=(UpdateHandlerBase&&) = default;

 protected:
    std::shared_ptr<middleware::sockets::ISocket> _socket;
    std::string _topic;
    std::shared_ptr<common::properties::IObservableProperty> _property;
};

} // namespace communication
} // namespace devices
} // namespace urf
