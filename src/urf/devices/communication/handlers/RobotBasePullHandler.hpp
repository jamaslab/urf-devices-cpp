#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/devices/urf_devices_export.h"
#else
    #define URF_DEVICES_EXPORT
#endif

#include "urf/devices/accesscontrol/AccessControl.hpp"
#include "urf/devices/communication/handlers/PullHandlerBase.hpp"
#include "urf/devices/robots/robotbase/RobotBase.hpp"

namespace urf {
namespace devices {
namespace communication {

class URF_DEVICES_EXPORT RobotBasePullHandler : public PullHandlerBase {
 public:
    RobotBasePullHandler(std::shared_ptr<robots::robotbase::RobotBase> device, std::shared_ptr<accesscontrol::AccessControl> accessControl);
    RobotBasePullHandler(const RobotBasePullHandler&) = default;
    RobotBasePullHandler(RobotBasePullHandler&&) = default;
    ~RobotBasePullHandler() override = default;

    void operator()(const middleware::messages::Message& command) noexcept override;

    RobotBasePullHandler& operator=(const RobotBasePullHandler&) = default;
    RobotBasePullHandler& operator=(RobotBasePullHandler&&) = default;

 private:
    std::shared_ptr<robots::robotbase::RobotBase> _device;
    std::shared_ptr<accesscontrol::AccessControl> _accessControl;
};

}  // namespace communication
}  // namespace devices
}  // namespace urf
