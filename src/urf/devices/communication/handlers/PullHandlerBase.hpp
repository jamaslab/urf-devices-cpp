#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/devices/urf_devices_export.h"
#else
    #define URF_DEVICES_EXPORT
#endif

#include <urf/middleware/messages/Message.hpp>

namespace urf {
namespace devices {
namespace communication {

class URF_DEVICES_EXPORT PullHandlerBase {
 public:
    PullHandlerBase() = default;
    PullHandlerBase(const PullHandlerBase&) = default;
    PullHandlerBase(PullHandlerBase&&) = default;
    virtual ~PullHandlerBase() = default;

    virtual void operator()(const middleware::messages::Message& command) noexcept = 0;

    PullHandlerBase& operator=(const PullHandlerBase&) = default;
    PullHandlerBase& operator=(PullHandlerBase&&) = default;
};

}  // namespace communication
}  // namespace devices
}  // namespace urf
