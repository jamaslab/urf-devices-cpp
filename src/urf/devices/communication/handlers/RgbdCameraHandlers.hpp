#pragma once

#include <atomic>
#include <memory>
#include <mutex>
#include <thread>

#include <urf/algorithms/compression/IVideoEncoder.hpp>
#include <urf/algorithms/compression/IDataEncoder.hpp>
#include <urf/algorithms/compression/pcl/PointCloudEncoder.hpp>

#include <urf/common/components/IComponent.hpp>

#include <urf/middleware/messages/Message.hpp>
#include <urf/middleware/sockets/ISocket.hpp>

#include <urf/devices/accesscontrol/AccessControl.hpp>
#include <urf/devices/communication/handlers/RequestHandlerBase.hpp>
#include <urf/devices/sensors/cameras/rgbd/RgbdCamera.hpp>
#include <urf/devices/sensors/cameras/rgbd/RgbdFrame.hpp>

namespace urf {
namespace devices {
namespace communication {

struct RgbdServerConfig {
    std::thread videoServerThread;
    std::unique_ptr<algorithms::compression::IVideoEncoder> rgbEncoder;
    std::unique_ptr<algorithms::compression::IDataEncoder> depthEncoder;
    std::string topic;
    bool includeColor;
    algorithms::compression::VideoResolution colorResolution;
    bool includeDepth;
    algorithms::compression::VideoResolution depthResolution;
    uint32_t fps;
    std::atomic<bool> ended;
};

class StartRgbdStreamHandler : public RequestHandlerBase {
 public:
    StartRgbdStreamHandler(std::shared_ptr<middleware::sockets::ISocket> socket,
                          std::shared_ptr<sensors::cameras::RgbdCamera> device,
                          std::shared_ptr<accesscontrol::AccessControl> accessControl);
    ~StartRgbdStreamHandler() override;

    middleware::messages::Message
    operator()(middleware::messages::Message& request) noexcept override final;

 private:
    void videoServer(std::shared_ptr<RgbdServerConfig> config);
    void frameGrabber();

 private:
    std::shared_ptr<middleware::sockets::ISocket> _socket;
    std::shared_ptr<sensors::cameras::RgbdCamera> _device;
    std::shared_ptr<accesscontrol::AccessControl> _accessControl;

    std::thread _frameGrabberThread;
    std::atomic<bool> _frameGrabberStarted;
    std::shared_mutex _frameMtx;
    sensors::cameras::RgbdFrame _latestFrame; // Maybe here it can become a triple buffer

    std::unique_ptr<algorithms::compression::PointCloudEncoder> _cloudEncoder;

    std::mutex _videoServersMtx;
    std::vector<std::shared_ptr<RgbdServerConfig>> _videoServers;
      
};

} // namespace communication
} // namespace devices
} // namespace urf