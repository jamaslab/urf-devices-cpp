#pragma once

#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/devices/urf_devices_export.h"
#else
#    define URF_DEVICES_EXPORT
#endif

#include "urf/devices/communication/handlers/UpdateHandlerBase.hpp"

namespace urf {
namespace devices {
namespace communication {

class URF_DEVICES_EXPORT UpdateAllHandler : public UpdateHandlerBase {
 public:
    UpdateAllHandler(std::shared_ptr<middleware::sockets::ISocket> socket,
                     const std::string& topic,
                     std::shared_ptr<common::properties::IObservableProperty> property);
    UpdateAllHandler(const UpdateAllHandler&) = default;
    UpdateAllHandler(UpdateAllHandler&&) = default;
    ~UpdateAllHandler() override = default;

    UpdateAllHandler& operator=(const UpdateAllHandler&) = default;
    UpdateAllHandler& operator=(UpdateAllHandler&&) = default;

 private:
    void subscribeToValueChanges(const std::string& propertyName,
                                 std::shared_ptr<common::properties::IObservableProperty> property);

 private:
    size_t _updateTotal;

    std::mutex _updateMessageMtx;
    size_t _updateCount;
    middleware::messages::Message _updateMessage;
};

} // namespace communication
} // namespace devices
} // namespace urf
