#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/devices/urf_devices_export.h"
#else
    #define URF_DEVICES_EXPORT
#endif

#include <string>

#include <urf/middleware/messages/Message.hpp>

namespace urf {
namespace devices {
namespace communication {

class URF_DEVICES_EXPORT RequestHandlerBase {
 public:
    RequestHandlerBase(const std::string& command);
    RequestHandlerBase(const RequestHandlerBase&) = default;
    RequestHandlerBase(RequestHandlerBase&&) = default;
    virtual ~RequestHandlerBase() = default;

    const std::string& command() const;

    virtual middleware::messages::Message operator()(middleware::messages::Message& request) noexcept = 0;

    RequestHandlerBase& operator=(const RequestHandlerBase&) = default;
    RequestHandlerBase& operator=(RequestHandlerBase&&) = default;

 private:
    std::string _command;
};

}  // namespace communication
}  // namespace devices
}  // namespace urf
