#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/devices/urf_devices_export.h"
#else
#    define URF_DEVICES_EXPORT
#endif

#include "urf/devices/robots/robotbase/RobotBase.hpp"

#include <atomic>       // std::atomic
#include <chrono>       // std::chrono::time_point
#include <memory>       // std::shared_ptr
#include <string>       // std::string
#include <thread>       // std::thread

#include <modbus.h>     // modbus_t

#include <urf/algorithms/control/robotbase/IRobotBaseKinematics.hpp>

namespace urf {
namespace devices {
namespace robots {
namespace robotbase {

class URF_DEVICES_EXPORT MiniBot : public RobotBase {
 public:
    MiniBot() = delete;
    MiniBot(const std::string& modbusPort);
    MiniBot(const MiniBot&) = delete;
    MiniBot(MiniBot&&) = delete;
    ~MiniBot() override;

    bool setCartesianVelocity(const std::array<float, 3>& velocities) override;

 private:
    bool resetFaultImpl() override final;
    bool switchOnImpl() override final;
    bool shutdownImpl() override final;
    bool disableImpl() override final;
    bool enableImpl() override final;
    bool faultImpl() override final;

    void controlLoop();
    void updateStatus();

 private:
    typedef struct MotorStatus { 
        uint16_t id;
        uint16_t position;
        uint16_t speed;
        uint16_t load;
        uint16_t voltage;
        uint16_t temperature;
        uint16_t moving;
    } MotorStatus;


    typedef struct MotorSetpoint {
        int16_t minPosition;
        int16_t maxPosition;
        uint16_t enable;
        uint16_t speed;
        int16_t position;
    } MotorSetpoint;

 private:
    std::string modbusPort_;
    modbus_t* device_;

    std::shared_ptr<common::properties::ObservableSetting<std::vector<float>>> cartesianVelocitySetpoint_;
    std::shared_ptr<common::properties::ObservableSettingRanged<float>> movementSafetyTimeout_;

    std::shared_ptr<common::properties::ObservableProperty<std::vector<float>>> cartesianVelocity_;
    std::shared_ptr<common::properties::ObservableProperty<float>> wheelRadius_;
    std::shared_ptr<common::properties::ObservableProperty<std::vector<float>>> wheelsPosition_;

    std::atomic<std::chrono::time_point<std::chrono::high_resolution_clock>> lastSetpointChange_;

    std::unique_ptr<algorithms::control::IRobotBaseKinematics> kinematics_;

    std::atomic<bool> stopControlThread_;
    std::thread controlThread_;

    std::array<MotorStatus, 4> motorStatus_;
    std::array<MotorSetpoint, 4> motorSetpoint_;
    std::array<float, 4> motorsDirection_;
};

} // namespace robotbase
} // namespace robots
} // namespace devices
} // namespace urf
