#pragma once

#if defined(_WIN32) || defined(_WIN64)
#    include "urf/devices/urf_devices_export.h"
#else
#    define URF_DEVICES_EXPORT
#endif

#include "urf/devices/Device.hpp"

namespace urf {
namespace devices {
namespace robots {
namespace robotbase {

class URF_DEVICES_EXPORT RobotBase : public Device {
 public:
    RobotBase() = delete;
    explicit RobotBase(const std::string& deviceName);
    virtual ~RobotBase() = default;

    virtual bool setCartesianVelocity(const std::array<float, 3>& velocities) = 0;

 protected:
    bool resetFaultImpl() override = 0;
    bool switchOnImpl() override = 0;
    bool shutdownImpl() override = 0;
    bool disableImpl() override = 0;
    bool enableImpl() override = 0;
    bool faultImpl() override = 0;

 protected:
    std::shared_ptr<common::properties::PropertyNode> setpoints_;
    std::shared_ptr<common::properties::PropertyNode> measurements_;
    std::shared_ptr<common::properties::PropertyNode> configuration_;
};

} // namespace robotbase
} // namespace robots
} // namespace devices
} // namespace urf
